'use strict';
$(document).ready(function (e) {

    var baseurl = window.location.href;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#name').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('select').on('input', function (e) {
        var value = $(this).val();

        if(!value){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('input[type="number"]').on('input', function (e) {
        var regex = /^([0-9\.]){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#weight, #height').on('input', function (e) {

        var weight = ($('#weight').val())? parseFloat($('#weight').val()): parseFloat(0);
        var height = ($('#height').val())? parseFloat($('#height').val()): parseFloat(0);
        var bmi = (parseFloat(weight) / parseFloat(height) / parseFloat(height)) * parseFloat(10000);
        $('#bmi').val(Math.round((bmi).toFixed(2)));

    });

    $('#standing-long-jump-one, #standing-long-jump-two').on('input', function (e) {

        var standingLongJumpOne = ($('#standing-long-jump-one').val())? parseFloat($('#standing-long-jump-one').val()): parseFloat(0);
        var standingLongJumpTwo = ($('#standing-long-jump-two').val())? parseFloat($('#standing-long-jump-two').val()): parseFloat(0);
        var result = (parseFloat(standingLongJumpOne) + parseFloat(standingLongJumpTwo)) / parseFloat(2);
        $('#standing-long-jump-total').val(parseFloat(result).toFixed(2));

    });

    $('#hexagon-agility-clockwise, #hexagon-agility-counter-clockwise').on('input', function (e) {


        var hexagonAgilityClockwise = ($('#hexagon-agility-clockwise').val())? parseFloat($('#hexagon-agility-clockwise').val()): parseFloat(0);
        var hexagonAgilityCounterClockwise = ($('#hexagon-agility-counter-clockwise').val())? parseFloat($('#hexagon-agility-counter-clockwise').val()): parseFloat(0);
        var result = (parseFloat(hexagonAgilityClockwise) + parseFloat(hexagonAgilityCounterClockwise)) / parseFloat(2);
        $('#hexagon-agility-result').val(parseFloat(result).toFixed(2));

    });

    $('#sit-and-reach-one, #sit-and-reach-two').on('input', function (e) {


        var sitAndReachOne = ($('#sit-and-reach-one').val())? parseFloat($('#sit-and-reach-one').val()): parseFloat(0);
        var sitAndReachTwo = ($('#sit-and-reach-two').val())? parseFloat($('#sit-and-reach-two').val()): parseFloat(0);
        var result = (parseFloat(sitAndReachOne) + parseFloat(sitAndReachTwo)) / parseFloat(2);
        $('#sit-and-reach-result').val(parseFloat(result).toFixed(2));

    });

    $('#stick-drop-test-one, #stick-drop-test-two, #stick-drop-test-three, #stick-drop-test-four').on('input', function (e) {


        var stickDropTestOne = ($('#stick-drop-test-one').val())? parseFloat($('#stick-drop-test-one').val()): parseFloat(0);
        var stickDropTestTwo = ($('#stick-drop-test-two').val())? parseFloat($('#stick-drop-test-two').val()): parseFloat(0);
        var stickDropTestThree = ($('#stick-drop-test-three').val())? parseFloat($('#stick-drop-test-three').val()): parseFloat(0);
        var stickDropTestFour = ($('#stick-drop-test-four').val())? parseFloat($('#stick-drop-test-four').val()): parseFloat(0);
        var result = (parseFloat(stickDropTestOne) + parseFloat(stickDropTestTwo) + parseFloat(stickDropTestThree) + parseFloat(stickDropTestFour)) / parseFloat(4);
        $('#stick-drop-test-result').val(parseFloat(result).toFixed(2));

    });

    $('#zipper-test-one, #zipper-test-two').on('input', function (e) {


        var zipperTestOne = ($('#zipper-test-one').val())? parseFloat($('#zipper-test-one').val()): parseFloat(0);
        var zipperTestTwo = ($('#zipper-test-two').val())? parseFloat($('#zipper-test-two').val()): parseFloat(0);
        var result = (parseFloat(zipperTestOne) + parseFloat(zipperTestTwo)) / parseFloat(4);
        $('#zipper-test-result').val(parseFloat(result).toFixed(2));

    });

    $('#remarks').on('input', function (e) {
        var regex = /^(.){1,}$/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Fill Out This Field');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();

    });

    $('#level-id').change(function (e) {
        var value = $(this).val();
        if(value){
            getGradesList(value);
        }
    });

    function getGradesList(levelId) {
        $.ajax({
            url:mainurl+'grades/getGradesList/'+(levelId),
            type:'GET',
            method:'GET',
            dataType:'JSON',
            beforeSend:function (e) {
                $('#grade-id').prop('disabled', true);
            }
        }).done(function (data,status, xhr) {
            $('#grade-id').empty().append('<option value="">Select Grade</option>');
            $.map(data, function (data, key) {
                $('#grade-id').append('<option value="'+(key)+'">'+(data)+'</option>');
            });
            $('#grade-id').prop('disabled', false);
        }).fail(function (data, status, xhr) {

        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});