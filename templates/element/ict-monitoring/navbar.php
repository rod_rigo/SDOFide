<?php
/**
 * @var \App\View\AppView $this
 */
?>

<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        <a href="javascript:void (0);" class="navbar-brand">
            <span class="brand-text font-weight-light">ICT</span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <?php if(boolval($auth['profile_is_admin'])):?>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Evaluations', 'action' => 'index'])?>" turbolink class="nav-link">Evaluations</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Indicators', 'action' => 'index'])?>" turbolink class="nav-link">Indicators</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Heads', 'action' => 'index'])?>" turbolink class="nav-link">Heads</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'MonitoringTypes', 'action' => 'index'])?>" turbolink class="nav-link">Monitoring Types</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Conditions', 'action' => 'index'])?>" turbolink class="nav-link">Conditions</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Users', 'action' => 'account', intval($auth['id'])])?>" turbolink class="nav-link">Account</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">Logout</a>
                    </li>
                <?php else:?>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Evaluations', 'action' => 'index'])?>" turbolink class="nav-link">Evaluations</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Users', 'action' => 'account', intval($auth['id'])])?>" turbolink class="nav-link">Account</a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">Logout</a>
                    </li>
                <?php endif;?>
            </ul>
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="javascript:void(0);" role="button">
                    <i class="fas fa-th-large"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>
