'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'evaluations/';
    var url = '';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getEvaluations',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {

        },
        buttons: [
            {
                text: 'PDF',
                action: function (e) {
                    Swal.fire({
                        title:'Export To PDF',
                        text: null,
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            // window.open(baseurl+'pdf');

                            var data = table.rows( {search: 'applied'} ).data().toArray();
                            var content = [
                                [
                                    { text: 'Department', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Head', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Monitoring Type', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Analysis', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Recommendations', bold: true, fontSize: 10, alignment: 'center' },
                                ]
                            ];
                            $.map(data, function (value, key) {
                                content.push([
                                    {
                                        text : value.department.department,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.head.head,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.monitoring_type.monitoring_type,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.analysis,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.recommendations,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                ]);
                            });


                            var docDefinition = {
                                info: {
                                    title: 'Evaluation List',
                                    author: null,
                                    subject: null,
                                    keywords: null,
                                    creator: null,
                                    creationDate: moment().format('Y-m-d H:m:s A'),
                                    modDate: moment().format('Y-m-d H:m:s A'),
                                },
                                pageOrientation: 'landscape',
                                pageSize: 'A4',
                                pageMargins: [ 10, 210, 10, 65 ],
                                header: function (currentPage, pageCount) {
                                    return {
                                        image:header,
                                        width: 800,
                                        height: 200,
                                        margin: [ 1, 1, 1, 1 ],
                                        alignment: 'center'
                                    };
                                },
                                footer: function(currentPage, pageCount) {
                                    return {
                                        image:footer,
                                        width: 800,
                                        height: 65,
                                        margin: [ 1, 1, 1, 1 ],
                                        alignment: 'center'
                                    };
                                },
                                content: [
                                    {
                                        layout: 'lightHorizontalLines', // optional
                                        table: {
                                            headerRows: 1,
                                            widths: ['*', '*', '*', '*', '*',],
                                            body: content,
                                        }
                                    },
                                    {
                                        width: '100%',
                                        text: (officer).toUpperCase(),
                                        alignment:'left',
                                        bold: true,
                                        margin: [ 0, 50, 0, 3 ]
                                    },
                                    {
                                        width: '100%',
                                        text: position,
                                        alignment:'left',
                                        italics: true,
                                        margin: [ 0, 0, 0, 5 ]
                                    },
                                ],
                            };

                            pdfMake.createPdf(docDefinition).open();
                        }
                    })
                },
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [10],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [11],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'department.department'},
            { data: 'head.head'},
            { data: 'monitoring_type.monitoring_type'},
            { data: 'progression.progression'},
            { data: 'status.status'},
            { data: 'analysis'},
            { data: 'recommendations'},
            { data: 'date'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'view/'+(dataId);
        Swal.fire({
            title: 'Open In New Window',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            denyButtonColor: '#808080',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            denyButtonText: 'Cancel',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }else if(result.isDenied){
                Swal.close();
            }else{
                Turbolinks.visit(href, {action: 'advance'});
            }
        });
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    $('#year').change(function (e) {
        e.preventDefault();
        monitoring_type_chart.destroy();
        month_chart.destroy();
        department_chart.destroy();
        getEvaluationsYearChart();
        getEvaluationsMonitorTypeChart();
        getEvaluationsDepartmentChart();
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', true);
        var startDate = moment($('#start-date').val()).format('Y-MM-DD');
        var endDate = moment($('#end-date').val()).format('Y-MM-DD');
        var records = $('#records').val();
        table.ajax.url(baseurl+'getEvaluations?start_date='+(startDate)+'&end_date='+(endDate)+'&records='+(records)).load(function () {
            $('#start-date, #end-date, #records, button[type="submit"]').prop('disabled', false);
        }, true);
    });

    var month_canvas = document.querySelector('#month-chart').getContext('2d');
    var month_chart;

    function getEvaluationsYear() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getEvaluationsYear?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.month).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getEvaluationsYearChart() {
        month_chart =  new Chart(month_canvas, {
            type: 'polarArea',
            data: {
                labels: getEvaluationsYear().label,
                datasets: [
                    {
                        label:'Total',
                        data: getEvaluationsYear().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 1
                    },
                    title: {
                        display: true,
                        text: 'Evaluations By Month',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var monitoring_type_canvas = document.querySelector('#monitoring-type-chart').getContext('2d');
    var monitoring_type_chart;

    function getEvaluationsMonitorType() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getEvaluationsMonitorType?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.monitoring_type).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getEvaluationsMonitorTypeChart() {
        monitoring_type_chart =  new Chart(monitoring_type_canvas, {
            type: 'doughnut',
            data: {
                labels: getEvaluationsMonitorType().label,
                datasets: [
                    {
                        label:'Total',
                        data: getEvaluationsMonitorType().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 2
                    },
                    title: {
                        display: true,
                        text: 'Evaluations By Monitoring Type',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var department_canvas = document.querySelector('#department-chart').getContext('2d');
    var department_chart;

    function getEvaluationsDepartment() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getEvaluationsDepartment?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.department).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getEvaluationsDepartmentChart() {
        department_chart =  new Chart(department_canvas, {
            type: 'bar',
            data: {
                labels: getEvaluationsDepartment().label,
                datasets: [
                    {
                        label:'Total',
                        data: getEvaluationsDepartment().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 3
                    },
                    title: {
                        display: true,
                        text: 'Evaluations By Department',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

    getEvaluationsYearChart();
    getEvaluationsMonitorTypeChart();
    getEvaluationsDepartmentChart();

});