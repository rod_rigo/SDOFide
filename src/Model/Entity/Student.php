<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Student Entity
 *
 * @property int $id
 * @property int $department_id
 * @property int $user_id
 * @property int $establishment_id
 * @property string $name
 * @property int $level_id
 * @property int $grade_id
 * @property int $gender_id
 * @property float $age
 * @property float $height
 * @property float $weight
 * @property float $bmi
 * @property float $resting_heart_rest
 * @property float $pulse_rate
 * @property float $partial_curl_up
 * @property float $push_up
 * @property float $stork_stand_left
 * @property float $stork_stand_right
 * @property float $meter_run
 * @property float $shuttle_run
 * @property float $tive_hand_wall
 * @property float $ruler_drop_test
 * @property float $vertical_jump
 * @property float $standing_long_jump_one
 * @property float $standing_long_jump_two
 * @property float $standing_long_jump_total
 * @property float $hexagon_agility_clockwise
 * @property float $hexagon_agility_counter_clockwise
 * @property float $hexagon_agility_result
 * @property float $kick_test
 * @property float $sit_and_reach_one
 * @property float $sit_and_reach_two
 * @property float $sit_and_reach_result
 * @property float $stick_drop_test_one
 * @property float $stick_drop_test_two
 * @property float $stick_drop_test_three
 * @property float $stick_drop_test_four
 * @property float $stick_drop_test_result
 * @property float $zipper_test_one
 * @property float $zipper_test_two
 * @property float $zipper_test_result
 * @property int $status_id
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Establishment $establishment
 * @property \App\Model\Entity\Level $level
 * @property \App\Model\Entity\Grade $grade
 * @property \App\Model\Entity\Gender $gender
 * @property \App\Model\Entity\Status $status
 */
class Student extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'department_id' => true,
        'user_id' => true,
        'establishment_id' => true,
        'name' => true,
        'level_id' => true,
        'grade_id' => true,
        'gender_id' => true,
        'age' => true,
        'height' => true,
        'weight' => true,
        'bmi' => true,
        'resting_heart_rest' => true,
        'pulse_rate' => true,
        'partial_curl_up' => true,
        'push_up' => true,
        'stork_stand_left' => true,
        'stork_stand_right' => true,
        'meter_run' => true,
        'shuttle_run' => true,
        'tive_hand_wall' => true,
        'ruler_drop_test' => true,
        'vertical_jump' => true,
        'standing_long_jump_one' => true,
        'standing_long_jump_two' => true,
        'standing_long_jump_total' => true,
        'hexagon_agility_clockwise' => true,
        'hexagon_agility_counter_clockwise' => true,
        'hexagon_agility_result' => true,
        'kick_test' => true,
        'sit_and_reach_one' => true,
        'sit_and_reach_two' => true,
        'sit_and_reach_result' => true,
        'stick_drop_test_one' => true,
        'stick_drop_test_two' => true,
        'stick_drop_test_three' => true,
        'stick_drop_test_four' => true,
        'stick_drop_test_result' => true,
        'zipper_test_one' => true,
        'zipper_test_two' => true,
        'zipper_test_result' => true,
        'status_id' => true,
        'remarks' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'department' => true,
        'user' => true,
        'establishment' => true,
        'level' => true,
        'grade' => true,
        'gender' => true,
        'status' => true,
    ];

    protected function _setName($value){
        return strtoupper($value);
    }

}
