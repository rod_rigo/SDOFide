<?php

use Dompdf\Dompdf;
use Dompdf\Options;

$path = WWW_ROOT. 'img'. DS. 'header.jpg';
$image = base64_encode(file_get_contents($path));
$mime = mime_content_type($path);
$header = 'data:'.($mime).';base64,'.($image);

$path = WWW_ROOT. 'img'. DS. 'footer.jpg';
$image = base64_encode(file_get_contents($path));
$mime = mime_content_type($path);
$footer = 'data:'.($mime).';base64,'.($image);

$options = new Options();
$options->set('isPhpEnabled', true);
$options->set('isRemoteEnabled', true);
$options->set('isHtml5ParserEnabled', true);

$html = '
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        html{
            margin: 0;
            padding: 0;
        }    
        table {
            width: 100%;
            border-collapse: collapse;
            height: 100%;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        td{
            height: 19px;
            text-align: center;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>';

for ($i = intval(0); $i < intval(count($projects)); $i+= intval(24)){
    if (intval($i) % intval(24) == intval(0)) {
        $html .= '<div style="height: 100%;">';
        $html .= '<img src="' . ($header) . '" style="width: 100%; height: 13%; position:relative; top:0;">';
        $html .= '<div style="height: 80%;">';
        $html .= '<table>';
        $html .= '<thead>';
        $html .= '<tr>';
        $html .= '<th>Title</th>';
        $html .= '<th>Author</th>';
        $html .= '<th>Position</th>';
        $html .= '<th>School</th>';
        $html .= '<th>Batch</th>';
        $html .= '<th>Date</th>';
        $html .= '<th>Category</th>';
        $html .= '<th>Remarks</th>';
        $html .= '</tr>';
        $html .= '<tbody>';

        for ($k = intval($i); $k < (intval($i) + intval(24)); $k++){
            try{
                $project = @$projects[intval($k)];
                $html .= '<tr>';
                $html .= '<td>'.(@$project['title']).'</td>';
                $html .= '<td>'.(@$project['author']).'</td>';
                $html .= '<td>'.(@$project['position']['position']).'</td>';
                $html .= '<td>'.(@$project['school']['school']).'</td>';
                $html .= '<td>'.(@$project['batch']).'</td>';
                $html .= '<td>'.(@$project['date']).'</td>';
                $html .= '<td>'.(@$project['category']['category']).'</td>';
                $html .= '<td>'.(@$project['remarks']).'</td>';
                $html .= '</tr>';
            }catch (\Exception $exception){
                $html .= '<tr>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '<td></td>';
                $html .= '</tr>';
            }
        }

        $html .= '<tbody>';
        $html .= '</thead>';

        $html .= '</table>';

        $html .= '</div>';
        $html .= '<img src="' . ($footer) . '" style="width: 100%; height: 7%; position:relative; bottom:0;">';
        $html .= '</div>';
        $html .= '<div style="page-break-after: always;"></div>';
    }
}

$html .= '</body>
</html>
';

// Instantiate Dompdf
$dompdf = new Dompdf($options);

// Load HTML content
$dompdf->loadHtml($html);

$dompdf->setPaper('A4');

// Render PDF (DOMPDF)
$dompdf->render();

// Output PDF to browser
$dompdf->stream('sample.pdf', array('Attachment' => false));

exit(0);