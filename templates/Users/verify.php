<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="p-5 mt-3">

</div>

<div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
    <div class="container py-5">
        <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
            <h5 class="fw-bold text-primary text-uppercase">Verify</h5>
            <h1 class="mb-0">Please Check Your Email For Verification Code, No Email? Please Check Your Spam Message</h1>
        </div>
        <div class="row g-5">
            <div class="col-sm-12 col-md-12 col-lg-12 wow slideInUp" data-wow-delay="0.3s">
                <?=$this->Form->create($user,['id' => 'form', 'type' => 'file'])?>
                <div class="row g-3">

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <strong class="py-2">
                            <?=$this->Form->label('code', ucwords('code'))?>
                        </strong>
                        <?=$this->Form->password('code',[
                            'id' => 'code',
                            'class' => 'form-control border-0 bg-light px-4',
                            'placeholder' => ucwords('code'),
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'style' => 'height: 55px;'
                        ])?>
                        <small></small>
                    </div>


                    <div class="col-sm-12 col-12">
                        <?= $this->Form->hidden('is_admin',[
                            'id' => 'is-admin',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_records',[
                            'id' => 'is-records',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_mapeh',[
                            'id' => 'is-mapeh',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_research',[
                            'id' => 'is-research',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_ict_monitoring',[
                            'id' => 'is-ict-monitoring',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_client',[
                            'id' => 'is-client',
                            'value' => intval(1)
                        ]);?>
                        <?= $this->Form->hidden('is_active',[
                            'id' => 'is-active',
                            'value' => intval(0)
                        ]);?>

                        <?=$this->Form->button('Submit',[
                            'type' => 'submit',
                            'class' => 'btn btn-primary w-100 py-3'
                        ])?>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('users/verify')?>
