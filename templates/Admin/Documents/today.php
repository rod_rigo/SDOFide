<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Document[]|\Cake\Collection\CollectionInterface $documents
 */
?>

<?=$this->Html->css('admin/documents/today')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'add'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="New Document">
            New Document
        </a>
    </div>

    <div class="col-sm-12 col-md-5 col-lg-4 mb-3">
        <?=$this->Form->label('memo', ucwords('memo'))?>
        <?=$this->Form->select('memo', $memos,[
            'class' => 'form-control rounded-0',
            'id' => 'memo',
            'required' => true,
            'empty' => ucwords('select memo'),
            'title' => ucwords('Please Fill Out This Field')
        ])?>
        <small></small>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Memo</th>
                        <th>Title</th>
                        <th>Is Published</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/documents/today')?>
