<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Project $project
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $categories
 * @var \Cake\Collection\CollectionInterface|string[] $statuses
 */
?>

    <script>
        var id = parseInt(<?=intval($project->id)?>);
    </script>

    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 h-em-75">
                            <embed src="<?=$this->Url->assetUrl('/files/'.($project->project_file))?>" id="embed" onwheel="" class="w-100 h-100" loading="lazy">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center align-items-center">
                    <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal" title="Close">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'index'])?>" turbolink id="toggle-modal" class="btn btn-primary rounded-0" title="Return">
                Return
            </a>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Project Form</h3>
                </div>
                <?=$this->Form->create($project,['id' => 'form', 'type' => 'file'])?>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <?=$this->Form->label('title', ucwords('title'))?>
                            <?=$this->Form->text('title',[
                                'required' => true,
                                'id' => 'title',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('title')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-5 col-lg-5 mt-3">
                            <?=$this->Form->label('author', ucwords('author'))?>
                            <?=$this->Form->text('author',[
                                'required' => true,
                                'id' => 'author',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('author')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                            <?=$this->Form->label('position_id', ucwords('position'))?>
                            <?=$this->Form->select('position_id', $positions,[
                                'required' => true,
                                'id' => 'position-id',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('Position')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <?=$this->Form->label('department_id', ucwords('department'))?>
                            <?=$this->Form->select('department_id', $departments,[
                                'required' => true,
                                'id' => 'department-id',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('department')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-2 col-lg-2 mt-3">
                            <?=$this->Form->label('batch', ucwords('batch'))?>
                            <?=$this->Form->number('batch',[
                                'required' => true,
                                'id' => 'batch',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('batch'),
                                'min' => 1
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <?=$this->Form->label('date', ucwords('date'))?>
                            <?=$this->Form->date('date',[
                                'id' => 'date',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('date')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                            <?=$this->Form->label('category_id', ucwords('category'))?>
                            <?=$this->Form->select('category_id', $categories,[
                                'required' => true,
                                'id' => 'category-id',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('category')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                            <?=$this->Form->label('status_id', ucwords('status'))?>
                            <?=$this->Form->select('status_id', $statuses,[
                                'required' => true,
                                'id' => 'status-id',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('status')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-5 col-lg-5 mt-3">
                            <?=$this->Form->label('due_date', ucwords('due date'))?>
                            <?=$this->Form->date('due_date',[
                                'required' => false,
                                'id' => 'due-date',
                                'class' => 'form-control form-control-border border-width-2',
                                'title' => ucwords('please fill out this field'),
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-5 mt-3">
                            <?=$this->Form->label('expired_at', ucwords('expired at'))?>
                            <?=$this->Form->date('expired_at',[
                                'required' => false,
                                'id' => 'expired-at',
                                'class' => 'form-control form-control-border border-width-2',
                                'title' => ucwords('please fill out this field'),
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-3 col-lg-2 mt-3 d-flex justify-content-start align-items-end">
                            <div class="icheck-primary d-inline">
                                <?= $this->Form->checkbox('published',[
                                    'id' => 'expired',
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval($project->is_expired)
                                ]); ?>
                                <?= $this->Form->label('expired', ucwords('expired')); ?>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <?=$this->Form->label('content', ucwords('abstract'))?>
                            <?=$this->Form->textarea('content',[
                                'required' => false,
                                'id' => 'content',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('content'),
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <?=$this->Form->label('remarks', ucwords('remarks'))?>
                            <?=$this->Form->textarea('remarks',[
                                'required' => true,
                                'id' => 'remarks',
                                'class' => 'form-control',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('remarks'),
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <div class="form-group">
                                <?=$this->Form->label('file', ucwords('file'))?>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <?=$this->Form->file('file',[
                                            'class' => 'custom-file-input',
                                            'required' => false,
                                            'id' => 'file',
                                            'accept' => 'application/pdf',
                                            'title' => ucwords('please fill out this field')
                                        ])?>
                                        <?=$this->Form->label('file', ucwords(explode('/',$project->project_file)[1]),[
                                            'class' => 'custom-file-label'
                                        ])?>
                                    </div>
                                    <div class="input-group-append bg-danger rounded-0" id="show-embed">
                                        <span class="input-group-text cursor-pointer bg-danger rounded-0">
                                            <i class="fas fa-file-pdf"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <div class="icheck-primary d-inline">
                                <?= $this->Form->checkbox('completed',[
                                    'id' => 'completed',
                                    'label' => false,
                                    'hiddenField' => false,
                                    'checked' => boolval($project->is_completed)
                                ]);?>
                                <?=$this->Form->label('completed', ucwords('completed')); ?>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <div class="icheck-primary d-inline">
                                <?php
//                                    $this->Form->checkbox('published',[
//                                        'id' => 'published',
//                                        'label' => false,
//                                        'hiddenField' => false,
//                                        'checked' => boolval($project->is_published)
//                                    ]);
                                ?>
                                <?php
//                                    $this->Form->label('published', ucwords('published'));
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-footer">
                    <?=$this->Form->hidden('project_file',[
                        'required' => true,
                        'id' => 'project-file',
                        'value' => $project->project_file
                    ])?>
                    <?=$this->Form->hidden('user_id',[
                        'required' => true,
                        'id' => 'user-id',
                        'value' => intval(@$auth['id'])
                    ])?>
                    <?=$this->Form->hidden('is_published',[
                        'required' => true,
                        'id' => 'is-published',
                        'value' => intval($project->is_published)
                    ])?>
                    <?=$this->Form->button('Reset',[
                        'class' => 'btn btn-danger',
                        'type' => 'reset'
                    ])?>
                    <?=$this->Form->button('Submit',[
                        'class' => 'btn btn-primary',
                        'type' => 'submit'
                    ])?>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row h-100">
                        <div class="col-sm-12 col-md-12 col-lg-12 h-80 overflow-y-auto" id="comments">
                            <?php foreach ($recentComments as $recentComment):?>
                                <div class="post">
                                    <div class="user-block">
                                    <span class="username">
                                        <a href="javascript:void(0);"><?=$recentComment->user->name?></a>
                                    </span>
                                        <span class="description">
                                        <?=@(new \Moment\Moment($recentComment->created,'Asia/Manila'))->fromNow()->getRelative()?>
                                    </span>
                                    </div>
                                    <p><?=$recentComment->comment?></p>
                                </div>
                            <?php endforeach;?>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 h-20 d-flex justify-content-center align-items-end">
                            <div class="post w-100">
                                <p>
                                <span class="float-right">
                                  <a href="javascript:void (0);" class="link-black text-sm hover-black" id="load-comments">
                                     <i class="fa fa-angle-right mr-1"></i>Load Comments
                                  </a>
                                </span>
                                    <span class="float-left">
                                  <a href="javascript:void (0);" class="link-black text-sm hover-black">
                                    <i class="far fa-comments mr-1"></i> Comments (<?=number_format(intval($comments))?>)
                                  </a>
                                </span>
                                </p>

                            </div>
                        </div>
                    </div>
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>

<?=$this->Html->script('Research/projects/view')?>