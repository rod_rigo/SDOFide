<?php
declare(strict_types=1);

namespace App\Controller\IctMonitoring;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;

/**
 * Statuses Controller
 *
 * @property \App\Model\Table\StatusesTable $Statuses
 * @method \App\Model\Entity\Status[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StatusesController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('ict-monitoring');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(!empty($user) && !boolval($user['is_ict_monitoring'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    public function getStatusesList($progressionId = null){
        $data = $this->Statuses->find('list',[
            'valueField' => function($query){
                return ucwords($query->status);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'is_active =' => intval(1),
            'progression_id =' => intval($progressionId)
        ])->order(['status' => 'ASC'],true);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($data));
    }

}
