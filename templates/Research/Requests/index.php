<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Request> $requests
 */
?>

    <div class="modal fade" id="modal">
        <div class="modal-dialog modal-lg">
            <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file']);?>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <?=$this->Form->label('purpose', ucwords('purpose'))?>
                            <?=$this->Form->textarea('purpose',[
                                'class' => 'form-control form-control-border border-width-2',
                                'id' => 'purpose',
                                'required' => true,
                                'placeholder' => ucwords('purpose'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field'),
                                'readonly' => true,
                            ])?>
                            <small></small>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                            <?=$this->Form->label('remarks', ucwords('remarks'))?>
                            <?=$this->Form->textarea('remarks',[
                                'class' => 'form-control form-control-border border-width-2',
                                'id' => 'remarks',
                                'required' => true,
                                'placeholder' => ucwords('remarks'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field'),
                            ])?>
                            <small></small>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-start align-items-end mt-3">
                            <div class="icheck-primary d-inline">
                                <?=$this->Form->checkbox('allowed',[
                                    'id' => 'allowed',
                                    'label' => false,
                                    'hiddenField' => false,
                                ])?>
                                <?=$this->Form->label('allowed', ucwords('allowed'))?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-end align-items-center">
                    <?=$this->Form->hidden('is_allowed',[
                        'required' => true,
                        'id' => 'is-allowed'
                    ])?>
                    <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                    <?=$this->Form->button(ucwords('Submit'),[
                        'class' => 'btn btn-success rounded-0',
                        'type' => 'submit',
                        'title' => ucwords('Submit')
                    ])?>
                </div>
            </div>
            <?=$this->Form->end();?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card p-3">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Requested By</th>
                            <th>Project</th>
                            <th>Author</th>
                            <th>Purpose</th>
                            <th>Is Allowed</th>
                            <th>Approved At</th>
                            <th>Remarks</th>
                            <th>Requested At</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?=$this->Html->script('research/requests/index')?>