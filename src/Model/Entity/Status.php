<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Status Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $progression_id
 * @property string $status
 * @property int $is_active
 * @property int $is_default
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Progression $progression
 * @property \App\Model\Entity\Tracking[] $trackings
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\Student[] $students
 * @property \App\Model\Entity\Evaluation[] $evaluations
 */
class Status extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'progression_id' => true,
        'status' => true,
        'is_active' => true,
        'is_default' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'progression' => true,
        'trackings' => true,
        'projects' => true,
        'students' => true,
        'evaluations' => true,
    ];

    protected function _setStatus($value){
        return ucwords($value);
    }

}
