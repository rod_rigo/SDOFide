<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Transactions Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\EstablishmentsTable&\Cake\ORM\Association\BelongsTo $Establishments
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\BelongsTo $Departments
 * @property \App\Model\Table\TrackingsTable&\Cake\ORM\Association\HasMany $Trackings
 *
 * @method \App\Model\Entity\Transaction newEmptyEntity()
 * @method \App\Model\Entity\Transaction newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Transaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Transaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\Transaction findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Transaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Transaction|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transaction saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransactionsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Establishments', [
            'foreignKey' => 'establishment_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Trackings', [
            'foreignKey' => 'transaction_id',
        ])->setDependent(true);
        $this->hasOne('CurrentTrackings', [
            'foreignKey' => 'transaction_id',
        ])->setClassName('Trackings');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('establishment_id')
            ->requirePresence('establishment_id', true)
            ->notEmptyString('establishment_id', ucwords('please fill out this field'), false);


        $validator
            ->numeric('department_id')
            ->requirePresence('department_id', true)
            ->notEmptyString('department_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('docsid')
            ->maxLength('docsid', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('docsid', true)
            ->notEmptyString('docsid');

        $validator
            ->scalar('transaction_name')
            ->maxLength('transaction_name', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('transaction_name', true)
            ->notEmptyString('transaction_name', ucwords('please fill out this field'), false);

        $validator
            ->date('date_release')
            ->allowEmptyDate('date_release');

        $validator
            ->scalar('received_by')
            ->maxLength('received_by', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('received_by', true)
            ->notEmptyString('received_by', ucwords('please fill out this field'), false);

        $validator
            ->date('date_received')
            ->allowEmptyDate('date_received');

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 4294967295)
            ->requirePresence('remarks', true)
            ->notEmptyString('remarks', ucwords('please fill out this field'), false);

        $validator
            ->scalar('document')
            ->maxLength('document', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('document', true)
            ->notEmptyString('document', ucwords('please fill out this field'), false);

        $validator
            ->numeric('has_file')
            ->notEmptyFile('has_file')
            ->add('has_file','has_file',[
                'rule'=> function($value){
                    $hasFile = [0, 1];

                    if(!in_array(intval($value), $hasFile)){
                        return ucwords('please select has file value');
                    }

                    return true;

                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['establishment_id'], 'Establishments'), ['errorField' => 'establishment_id']);
        $rules->add($rules->existsIn(['department_id'], 'Departments'), ['errorField' => 'department_id']);
        $rules->add($rules->isUnique(['docsid'], ucwords('this docs ID is already exists')), ['errorField' => 'docsid']);

        return $rules;
    }
}
