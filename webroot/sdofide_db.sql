/*
 Navicat Premium Data Transfer

 Source Server         : wampp
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : sdofide_db

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 04/05/2024 16:59:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for abouts
-- ----------------------------
DROP TABLE IF EXISTS `abouts`;
CREATE TABLE `abouts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `header` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `abouts to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `abouts to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of abouts
-- ----------------------------
INSERT INTO `abouts` VALUES (1, 2, 'ABOUT US', 'SDO SANTIAGO CITY RECORDS UNIT', 'about-img/6615e11a3a778377323874_2085746898448771_8555122959557130172_n.png', '<div style=\"-webkit-text-stroke-width:0px;background-color:rgb(255, 255, 255);color:rgb(0, 0, 0);float:left;font-family:&quot;Open Sans&quot;, Arial, sans-serif;font-size:14px;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;margin:0px 14.3906px 0px 28.7969px;orphans:2;padding:0px;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:normal;widows:2;width:436.797px;word-spacing:0px;\"><h2 style=\"font-family:DauphinPlain;font-size:24px;font-weight:400;line-height:24px;margin:0px 0px 10px;padding:0px;text-align:left;\">What is Lorem Ipsum?</h2><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;\"><strong style=\"margin:0px;padding:0px;\">Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div style=\"-webkit-text-stroke-width:0px;background-color:rgb(255, 255, 255);color:rgb(0, 0, 0);float:right;font-family:&quot;Open Sans&quot;, Arial, sans-serif;font-size:14px;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;margin:0px 28.7969px 0px 14.3906px;orphans:2;padding:0px;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:normal;widows:2;width:436.797px;word-spacing:0px;\"><h2 style=\"font-family:DauphinPlain;font-size:24px;font-weight:400;line-height:24px;margin:0px 0px 10px;padding:0px;text-align:left;\">Why do we use it?</h2><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div><p><br>&nbsp;</p><div style=\"-webkit-text-stroke-width:0px;background-color:rgb(255, 255, 255);color:rgb(0, 0, 0);float:left;font-family:&quot;Open Sans&quot;, Arial, sans-serif;font-size:14px;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;margin:0px 14.3906px 0px 28.7969px;orphans:2;padding:0px;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:normal;widows:2;width:436.797px;word-spacing:0px;\"><h2 style=\"font-family:DauphinPlain;font-size:24px;font-weight:400;line-height:24px;margin:0px 0px 10px;padding:0px;text-align:left;\">Where does it come from?</h2><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p></div><div style=\"-webkit-text-stroke-width:0px;background-color:rgb(255, 255, 255);color:rgb(0, 0, 0);float:right;font-family:&quot;Open Sans&quot;, Arial, sans-serif;font-size:14px;font-style:normal;font-variant-caps:normal;font-variant-ligatures:normal;font-weight:400;letter-spacing:normal;margin:0px 28.7969px 0px 14.3906px;orphans:2;padding:0px;text-align:left;text-decoration-color:initial;text-decoration-style:initial;text-decoration-thickness:initial;text-indent:0px;text-transform:none;white-space:normal;widows:2;width:436.797px;word-spacing:0px;\"><h2 style=\"font-family:DauphinPlain;font-size:24px;font-weight:400;line-height:24px;margin:0px 0px 10px;padding:0px;text-align:left;\">Where can I get some?</h2><p style=\"margin:0px 0px 15px;padding:0px;text-align:justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p></div>', 'PROVIDES ADMINISTRATIVE SUPPORT TO THE MANAGEMENT AND STAFF OF THE REGION TO ENSURE THE RECEIPT, RECORDING, AND DISTRIBUTION OF OFFICIAL CORRESPONDENCE AND DOCUMENTS; SAFEKEEPING, PRESERVATION, AND RETRIEVAL OF RECORDS AND FILES OF OPERATIONAL, LEGAL AND HISTORICAL VALUE; AND THE DISPOSITION OF FILES AND RECORDS ACCORDING TO EXISTING LAWS AND POLICIES; THROUGH THE ESTABLISHMENT AND MAINTENANCE OF AN EFFECTIVE AND EFFICIENT RECORDS MANAGEMENT SYSTEM IN THE REGIONAL OFFICE AND PROVISION OF TECHNICAL ASSISTANCE TO THE SCHOOLS DIVISION OFFICES.', 1, '2024-04-09 18:00:18', '2024-04-10 16:45:14', NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `categories to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `categories to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 2, 'Research', '2024-04-21 18:57:08', '2024-04-21 18:57:08', NULL);

-- ----------------------------
-- Table structure for classrooms
-- ----------------------------
DROP TABLE IF EXISTS `classrooms`;
CREATE TABLE `classrooms`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `evaluation_id` bigint UNSIGNED NOT NULL,
  `condition_id` bigint UNSIGNED NOT NULL,
  `indicator_id` bigint UNSIGNED NOT NULL,
  `is_yes` tinyint NULL DEFAULT 0,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `classrooms to evaluations`(`evaluation_id` ASC) USING BTREE,
  INDEX `classrooms to conditions`(`condition_id` ASC) USING BTREE,
  INDEX `classrooms to indicators`(`indicator_id` ASC) USING BTREE,
  CONSTRAINT `classrooms to conditions` FOREIGN KEY (`condition_id`) REFERENCES `conditions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `classrooms to evaluations` FOREIGN KEY (`evaluation_id`) REFERENCES `evaluations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `classrooms to indicators` FOREIGN KEY (`indicator_id`) REFERENCES `indicators` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classrooms
-- ----------------------------

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `project_id` bigint UNSIGNED NOT NULL,
  `comment` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `comments to users`(`user_id` ASC) USING BTREE,
  INDEX `comments to projects`(`project_id` ASC) USING BTREE,
  CONSTRAINT `comments to projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `comments to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for conditions
-- ----------------------------
DROP TABLE IF EXISTS `conditions`;
CREATE TABLE `conditions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `conditions to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `conditions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of conditions
-- ----------------------------
INSERT INTO `conditions` VALUES (1, 2, 'SECURITY & SAFETY', '2024-04-28 22:28:55', '2024-04-28 22:29:13', NULL);
INSERT INTO `conditions` VALUES (2, 2, 'LAB ACTIVITY MONITORING', '2024-04-28 22:29:17', '2024-04-28 22:32:13', NULL);
INSERT INTO `conditions` VALUES (3, 2, 'LEARNING ENVIRONMENT', '2024-04-28 22:32:31', '2024-04-28 22:32:31', NULL);

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `header` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `latitude` double NOT NULL,
  `longtitude` double NOT NULL,
  `contact_mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `contacts to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `contacts to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 2, 'CONTACT US', 'IF YOU HAVE ANY QUERY, FEEL FREE TO CONTACT US', 'Children\'s Park, Calaocan, Santiago City', 16.684736483077, 121.55144797073, '(078) 682 - 0156', 'santiago.city@deped.gov.ph', '\"SED UT PERSPICIATIS UNDE OMNIS ISTE NATUS ERROR SIT VOLUPTATEM ACCUSANTIUM DOLOREMQUE LAUDANTIUM, TOTAM REM APERIAM, EAQUE IPSA QUAE AB ILLO INVENTORE VERITATIS ET QUASI ARCHITECTO BEATAE VITAE DICTA SUNT EXPLICABO. NEMO ENIM IPSAM VOLUPTATEM QUIA VOLUPTAS SIT ASPERNATUR AUT ODIT AUT FUGIT, SED QUIA CONSEQUUNTUR MAGNI DOLORES EOS QUI RATIONE VOLUPTATEM SEQUI NESCIUNT. NEQUE PORRO QUISQUAM EST, QUI DOLOREM IPSUM QUIA DOLOR SIT AMET, CONSECTETUR, ADIPISCI VELIT, SED QUIA NON NUMQUAM EIUS MODI TEMPORA INCIDUNT UT LABORE ET DOLORE MAGNAM ALIQUAM QUAERAT VOLUPTATEM. UT ENIM AD MINIMA VENIAM, QUIS NOSTRUM EXERCITATIONEM ULLAM CORPORIS SUSCIPIT LABORIOSAM, NISI UT ALIQUID EX EA COMMODI CONSEQUATUR? QUIS AUTEM VEL EUM IURE REPREHENDERIT QUI IN EA VOLUPTATE VELIT ESSE QUAM NIHIL MOLESTIAE CONSEQUATUR, VEL ILLUM QUI DOLOREM EUM FUGIAT QUO VOLUPTAS NULLA PARIATUR?\"', '<p>-</p>', 1, '2024-04-09 18:33:38', '2024-04-09 22:35:03', NULL);

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `establishment_id` bigint UNSIGNED NOT NULL,
  `department` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `departments to users`(`user_id` ASC) USING BTREE,
  INDEX `departments to establishments`(`establishment_id` ASC) USING BTREE,
  CONSTRAINT `departments to establishments` FOREIGN KEY (`establishment_id`) REFERENCES `establishments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `departments to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES (43, 2, 4, 'Baluarte Elementary School', 1, '2024-04-10 00:14:17', '2024-04-10 00:14:17', NULL);
INSERT INTO `departments` VALUES (44, 2, 4, 'Calaocan Elementary School', 1, '2024-04-10 00:14:54', '2024-04-10 00:14:54', NULL);
INSERT INTO `departments` VALUES (45, 2, 6, 'LGU Santiago City - CENRO', 1, '2024-04-10 00:15:09', '2024-04-10 00:15:30', NULL);
INSERT INTO `departments` VALUES (46, 2, 5, 'Landbank Of The Philippines', 1, '2024-04-10 00:15:47', '2024-04-10 00:15:47', NULL);

-- ----------------------------
-- Table structure for documents
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `memo_id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_published` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `documents to users`(`user_id` ASC) USING BTREE,
  INDEX `documents to memos`(`memo_id` ASC) USING BTREE,
  CONSTRAINT `documents to memos` FOREIGN KEY (`memo_id`) REFERENCES `memos` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `documents to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of documents
-- ----------------------------
INSERT INTO `documents` VALUES (1, 2, 1, 'DM163-S.-2024-DSOW-FOR-THE-DIVISION-TRAINING-OF-TRAINERS', '1/DM163-S.-2024-DSOW-FOR-THE-DIVISION-TRAINING-OF-TRAINERS.pdf', 1, '2024-04-10 00:11:20', '2024-04-10 00:11:20', NULL);
INSERT INTO `documents` VALUES (2, 2, 1, 'DM164-S.-2024-CORRIGENDUM-TO-DM155-S.-2024', '1/DM164-S.-2024-CORRIGENDUM-TO-DM155-S.-2024.pdf', 1, '2024-04-10 00:11:47', '2024-04-10 02:50:05', NULL);
INSERT INTO `documents` VALUES (3, 2, 2, 'DM062-S.-2024-2024-DIVISION-SCREENING-AND-ACCREDITATION-COMMITTEE-DSAC-AND-SCHEDULE-OF-SCREENING-AND-VALIDATION-OF-PERTINENT-DOCUMENTS-OF-ALL-ATHLETES-AND-COACHES', '2/DM062-S.-2024-2024-DIVISION-SCREENING-AND-ACCREDITATION-COMMITTEE-DSAC-AND-SCHEDULE-OF-SCREENING-AND-VALIDATION-OF-PERTINENT-DOCUMENTS-OF-ALL-ATHLETES-AND-COACHES.pdf', 0, '2024-04-10 00:12:32', '2024-04-10 16:33:20', NULL);

-- ----------------------------
-- Table structure for equipments
-- ----------------------------
DROP TABLE IF EXISTS `equipments`;
CREATE TABLE `equipments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `evaluation_id` bigint UNSIGNED NOT NULL,
  `dcp_batch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `year_delivered` year NULL DEFAULT NULL,
  `package_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `warranty_expiration_date` date NULL DEFAULT NULL,
  `is_dr_available` tinyint NULL DEFAULT NULL,
  `dr_document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_iar_available` tinyint NULL DEFAULT NULL,
  `iar_document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_irp_available` tinyint NULL DEFAULT NULL,
  `irp_document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_ptr_available` tinyint NULL DEFAULT NULL,
  `ptr_document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `equipments to evaluations`(`evaluation_id` ASC) USING BTREE,
  CONSTRAINT `equipments to evaluations` FOREIGN KEY (`evaluation_id`) REFERENCES `evaluations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of equipments
-- ----------------------------

-- ----------------------------
-- Table structure for establishments
-- ----------------------------
DROP TABLE IF EXISTS `establishments`;
CREATE TABLE `establishments`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `establishment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_default` tinyint NOT NULL DEFAULT 0,
  `is_active` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `establishments to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `establishments to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of establishments
-- ----------------------------
INSERT INTO `establishments` VALUES (4, 2, 'School', 1, 1, '2024-04-10 00:13:49', '2024-04-21 20:05:28', NULL);
INSERT INTO `establishments` VALUES (5, 2, 'Others', 0, 1, '2024-04-10 00:13:55', '2024-04-10 00:13:55', NULL);
INSERT INTO `establishments` VALUES (6, 2, 'Office', 0, 1, '2024-04-10 00:14:00', '2024-04-10 00:14:00', NULL);

-- ----------------------------
-- Table structure for evaluations
-- ----------------------------
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE `evaluations`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `head_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `monitoring_type_id` bigint UNSIGNED NOT NULL,
  `analysis` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `recommendations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `dr_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `air_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `irp_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ptr_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `evaluations to users`(`user_id` ASC) USING BTREE,
  INDEX `evaluations to heads`(`head_id` ASC) USING BTREE,
  INDEX `evaluations to departments`(`department_id` ASC) USING BTREE,
  INDEX `evaluations to monitoring_types`(`monitoring_type_id` ASC) USING BTREE,
  CONSTRAINT `evaluations to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `evaluations to heads` FOREIGN KEY (`head_id`) REFERENCES `heads` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `evaluations to monitoring_types` FOREIGN KEY (`monitoring_type_id`) REFERENCES `monitoring_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `evaluations to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of evaluations
-- ----------------------------

-- ----------------------------
-- Table structure for genders
-- ----------------------------
DROP TABLE IF EXISTS `genders`;
CREATE TABLE `genders`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `genders to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `genders to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of genders
-- ----------------------------
INSERT INTO `genders` VALUES (1, 2, 'Males', 1, '2024-04-21 23:28:42', '2024-04-21 23:28:47', NULL);

-- ----------------------------
-- Table structure for grades
-- ----------------------------
DROP TABLE IF EXISTS `grades`;
CREATE TABLE `grades`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `level_id` bigint UNSIGNED NOT NULL,
  `grade` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `grades to users`(`user_id` ASC) USING BTREE,
  INDEX `grades to levels`(`level_id` ASC) USING BTREE,
  CONSTRAINT `grades to levels` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `grades to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of grades
-- ----------------------------
INSERT INTO `grades` VALUES (1, 2, 1, 'Grade 1', 1, '2024-04-22 00:02:44', '2024-04-22 00:02:44', NULL);

-- ----------------------------
-- Table structure for heads
-- ----------------------------
DROP TABLE IF EXISTS `heads`;
CREATE TABLE `heads`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `establishment_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `head` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `ict_coordinator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `property_custodian` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `heads to departments`(`department_id` ASC) USING BTREE,
  INDEX `heads to establishments`(`establishment_id` ASC) USING BTREE,
  CONSTRAINT `heads to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `heads to establishments` FOREIGN KEY (`establishment_id`) REFERENCES `establishments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of heads
-- ----------------------------
INSERT INTO `heads` VALUES (1, 2, 4, 44, 'HEAD', 'ICT', 'CUSTODIAN', '2024-04-28 21:40:36', '2024-04-28 21:42:43', NULL);
INSERT INTO `heads` VALUES (2, 2, 4, 43, 'HEAD', 'ICT', 'CUSTODIAN', '2024-04-29 03:12:09', '2024-04-29 03:12:09', NULL);

-- ----------------------------
-- Table structure for indicators
-- ----------------------------
DROP TABLE IF EXISTS `indicators`;
CREATE TABLE `indicators`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `condition_id` bigint UNSIGNED NOT NULL,
  `indicator` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `indicators to users`(`user_id` ASC) USING BTREE,
  INDEX `indicators to conditions`(`condition_id` ASC) USING BTREE,
  CONSTRAINT `indicators to conditions` FOREIGN KEY (`condition_id`) REFERENCES `conditions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `indicators to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of indicators
-- ----------------------------
INSERT INTO `indicators` VALUES (1, 2, 1, 'Grills Are Installed To Doors And Windows And Other Holes/ Exits From The Class Room', '2024-04-28 22:49:00', '2024-04-28 22:49:00', NULL);
INSERT INTO `indicators` VALUES (2, 2, 1, 'A Security Personnel Is Designated', '2024-04-28 22:50:39', '2024-04-28 22:50:39', NULL);
INSERT INTO `indicators` VALUES (3, 2, 1, 'Doors Are Protected With Double Padlocks', '2024-04-28 22:51:00', '2024-04-28 22:51:00', NULL);
INSERT INTO `indicators` VALUES (4, 2, 2, 'Class Schedules Are Readable And Are Posted In Visible Area/s', '2024-04-28 22:51:41', '2024-04-28 22:51:41', NULL);
INSERT INTO `indicators` VALUES (5, 2, 2, 'Logbooks Are Utilized', '2024-04-28 22:51:54', '2024-04-28 22:51:54', NULL);
INSERT INTO `indicators` VALUES (6, 2, 2, 'DCPs Are Available And Usable By All Learning Areas', '2024-04-28 22:52:57', '2024-04-28 22:52:57', NULL);
INSERT INTO `indicators` VALUES (7, 2, 2, 'User Guidelines Are Readable And Are Posted In Visible Area/s', '2024-04-28 22:53:48', '2024-04-28 22:53:48', NULL);
INSERT INTO `indicators` VALUES (8, 2, 3, 'The Room Is Well-Ventilated (Electric Fan, Air-Condition Unit Are Installed)', '2024-04-28 22:55:03', '2024-04-28 22:55:03', NULL);
INSERT INTO `indicators` VALUES (9, 2, 3, 'Walls Are Free From Destructive Design', '2024-04-28 22:55:23', '2024-04-28 22:55:23', NULL);
INSERT INTO `indicators` VALUES (10, 2, 3, 'The Computer Laboratory Is Well-Lit', '2024-04-28 22:56:00', '2024-04-28 22:56:00', NULL);
INSERT INTO `indicators` VALUES (11, 2, 3, 'Chairs Are Sufficient In Number (i.e 50 And Above)', '2024-04-28 22:57:01', '2024-04-28 22:57:01', NULL);
INSERT INTO `indicators` VALUES (12, 2, 3, 'Tables Are In Good Condition', '2024-04-28 22:57:36', '2024-04-28 22:57:36', NULL);
INSERT INTO `indicators` VALUES (13, 2, 3, 'The Computer Laboratory Is Clean And Orderly', '2024-04-28 22:57:55', '2024-04-28 22:57:55', NULL);
INSERT INTO `indicators` VALUES (14, 2, 3, 'The Computer Laboratory Space Is Sufficient', '2024-04-28 22:58:25', '2024-04-28 22:58:25', NULL);

-- ----------------------------
-- Table structure for levels
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `levels to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `levels to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of levels
-- ----------------------------
INSERT INTO `levels` VALUES (1, 2, 'Elementary', 1, '2024-04-21 23:39:14', '2024-04-21 23:39:14', NULL);

-- ----------------------------
-- Table structure for memos
-- ----------------------------
DROP TABLE IF EXISTS `memos`;
CREATE TABLE `memos`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `is_shown` tinyint NOT NULL DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `memos to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `memos to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of memos
-- ----------------------------
INSERT INTO `memos` VALUES (1, 2, 'Division Memorandum', 'DM', 1, 1, '2024-04-09 23:14:57', '2024-04-10 16:43:49', NULL);
INSERT INTO `memos` VALUES (2, 2, 'Division Order', 'DO', 1, 1, '2024-04-09 23:15:12', '2024-04-09 23:15:12', NULL);
INSERT INTO `memos` VALUES (3, 2, 'Travel Order', 'TO', 1, 1, '2024-04-10 00:12:55', '2024-04-10 00:12:55', NULL);

-- ----------------------------
-- Table structure for monitoring_dates
-- ----------------------------
DROP TABLE IF EXISTS `monitoring_dates`;
CREATE TABLE `monitoring_dates`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `evaluation_id` bigint UNSIGNED NOT NULL,
  `monitoring_type_id` bigint UNSIGNED NOT NULL,
  `date` date NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `monitoring_dates to evaluations`(`evaluation_id` ASC) USING BTREE,
  INDEX `monitoring_types to monitoring_types`(`monitoring_type_id` ASC) USING BTREE,
  CONSTRAINT `monitoring_dates to evaluations` FOREIGN KEY (`evaluation_id`) REFERENCES `evaluations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `monitoring_types to monitoring_types` FOREIGN KEY (`monitoring_type_id`) REFERENCES `monitoring_types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of monitoring_dates
-- ----------------------------

-- ----------------------------
-- Table structure for monitoring_types
-- ----------------------------
DROP TABLE IF EXISTS `monitoring_types`;
CREATE TABLE `monitoring_types`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `monitoring_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `monitoring_types to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `monitoring_types to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of monitoring_types
-- ----------------------------
INSERT INTO `monitoring_types` VALUES (1, 2, 'First Visit', '2024-04-28 22:19:07', '2024-04-28 22:21:09', NULL);

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS `positions`;
CREATE TABLE `positions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `positions to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `positions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO `positions` VALUES (1, 2, 'Administratve Officer 5', '2024-04-21 18:57:17', '2024-04-21 18:57:17', NULL);

-- ----------------------------
-- Table structure for profiles
-- ----------------------------
DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `establishment_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `position_id` bigint UNSIGNED NOT NULL,
  `is_admin` bigint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `profiles to users`(`user_id` ASC) USING BTREE,
  INDEX `profiles to establishments`(`establishment_id` ASC) USING BTREE,
  INDEX `profiles to departments`(`department_id` ASC) USING BTREE,
  CONSTRAINT `profiles to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `profiles to establishments` FOREIGN KEY (`establishment_id`) REFERENCES `establishments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `profiles to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of profiles
-- ----------------------------
INSERT INTO `profiles` VALUES (1, 5, 4, 43, 1, 1, '2024-04-21 22:40:18', '2024-04-21 22:40:18', NULL);
INSERT INTO `profiles` VALUES (2, 6, 4, 43, 1, 1, '2024-04-29 02:48:43', '2024-04-29 02:48:43', NULL);

-- ----------------------------
-- Table structure for progressions
-- ----------------------------
DROP TABLE IF EXISTS `progressions`;
CREATE TABLE `progressions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `progression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `is_default` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `progressions to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `progressions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of progressions
-- ----------------------------
INSERT INTO `progressions` VALUES (2, 2, 'On Going', 1, 1, '2024-04-10 00:16:07', '2024-04-10 00:16:07', NULL);
INSERT INTO `progressions` VALUES (3, 2, 'Released', 1, 0, '2024-04-10 00:16:15', '2024-04-10 00:16:23', NULL);

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `batch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date` date NOT NULL,
  `category_id` bigint UNSIGNED NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status_id` bigint UNSIGNED NOT NULL,
  `project_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_published` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `projects to users`(`user_id` ASC) USING BTREE,
  INDEX `projects to schools`(`department_id` ASC) USING BTREE,
  INDEX `projects to positions`(`position_id` ASC) USING BTREE,
  INDEX `projects to categories`(`category_id` ASC) USING BTREE,
  INDEX `projects to statuses`(`status_id` ASC) USING BTREE,
  CONSTRAINT `projects to categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `projects to positions` FOREIGN KEY (`position_id`) REFERENCES `positions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `projects to statuses` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `projects to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of projects
-- ----------------------------

-- ----------------------------
-- Table structure for ratings
-- ----------------------------
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE `ratings`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `project_id` bigint UNSIGNED NOT NULL,
  `rating` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ratings to users`(`user_id` ASC) USING BTREE,
  INDEX `ratings to projects`(`project_id` ASC) USING BTREE,
  CONSTRAINT `ratings to projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ratings to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ratings
-- ----------------------------

-- ----------------------------
-- Table structure for remember_me_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_phinxlog`;
CREATE TABLE `remember_me_phinxlog`  (
  `version` bigint NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_phinxlog
-- ----------------------------

-- ----------------------------
-- Table structure for remember_me_tokens
-- ----------------------------
DROP TABLE IF EXISTS `remember_me_tokens`;
CREATE TABLE `remember_me_tokens`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `model` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `foreign_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `U_token_identifier`(`model` ASC, `foreign_id` ASC, `series` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of remember_me_tokens
-- ----------------------------
INSERT INTO `remember_me_tokens` VALUES (5, '2024-04-09 22:24:05', '2024-04-09 22:24:05', 'Users', '2', 'c8565583ca8b0ad34dcf7b8c05383fae7b19d6eb', 'be66050affc4f9a33205e6ff7f60f51a53da10a9', '2024-05-09 22:24:05');
INSERT INTO `remember_me_tokens` VALUES (6, '2024-04-10 02:49:11', '2024-04-10 15:25:22', 'Users', '2', '5efdf679284186c131042b3876318b79a5bb97e8', 'ee93b4dbc01863bd833022eac4718f0bde0fe15f', '2024-05-10 15:25:22');
INSERT INTO `remember_me_tokens` VALUES (16, '2024-04-21 23:13:48', '2024-04-21 23:13:48', 'Users', '2', 'e3630611c86721e9cf294de4987637f35fc9e85d', '78032933a46c3896aea195decc4939c5804e05bd', '2024-05-21 23:13:48');
INSERT INTO `remember_me_tokens` VALUES (33, '2024-05-04 19:13:22', '2024-05-04 19:13:22', 'Users', '2', 'f0f51ca3100b7f6feb3d8bd9a1880fd1210bf563', 'fba0a5166af92f7a8713d91c3851068b8faabee2', '2024-06-03 19:13:22');
INSERT INTO `remember_me_tokens` VALUES (39, '2024-05-05 00:53:57', '2024-05-05 00:53:57', 'Users', '6', '5f62bc8bba56b0817f9845ab4ae74a285cc8887a', '6628726ea068c48f1858dcea8e8fbdea2d7f9805', '2024-06-04 00:53:57');

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `project_id` bigint UNSIGNED NOT NULL,
  `purpose` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `key` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_allowed` tinyint NOT NULL DEFAULT 0,
  `is_agreed` tinyint NOT NULL DEFAULT 0,
  `approved_at` datetime NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `requests to users`(`user_id` ASC) USING BTREE,
  INDEX `requests to projects`(`project_id` ASC) USING BTREE,
  CONSTRAINT `requests to projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `requests to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of requests
-- ----------------------------

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `progression_id` bigint UNSIGNED NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `is_default` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `statuses to users`(`user_id` ASC) USING BTREE,
  INDEX `statuses to progression`(`progression_id` ASC) USING BTREE,
  CONSTRAINT `statuses to progression` FOREIGN KEY (`progression_id`) REFERENCES `progressions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `statuses to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of statuses
-- ----------------------------
INSERT INTO `statuses` VALUES (3, 2, 2, 'For Checking', 1, 1, '2024-04-10 00:16:48', '2024-04-10 00:16:48', NULL);
INSERT INTO `statuses` VALUES (4, 2, 3, 'Completed', 1, 1, '2024-04-10 00:16:55', '2024-04-10 00:16:55', NULL);

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `department_id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `establishment_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `level_id` bigint UNSIGNED NOT NULL,
  `grade_id` bigint NOT NULL,
  `gender_id` bigint UNSIGNED NOT NULL,
  `age` double NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `bmi` double NOT NULL,
  `resting_heart_rest` double NOT NULL,
  `pulse_rate` double NOT NULL,
  `partial_curl_up` double NOT NULL,
  `push_up` double NOT NULL,
  `stork_stand_left` double NOT NULL,
  `stork_stand_right` double NOT NULL,
  `meter_run` double NOT NULL,
  `shuttle_run` double NOT NULL,
  `tive_hand_wall` double NOT NULL,
  `ruler_drop_test` double NOT NULL,
  `vertical_jump` double NOT NULL,
  `standing_long_jump_one` double NOT NULL,
  `standing_long_jump_two` double NOT NULL,
  `standing_long_jump_total` double NOT NULL,
  `hexagon_agility_clockwise` double NOT NULL,
  `hexagon_agility_counter_clockwise` double NOT NULL,
  `hexagon_agility_result` double NOT NULL,
  `kick_test` double NOT NULL,
  `sit_and_reach_one` double NOT NULL,
  `sit_and_reach_two` double NOT NULL,
  `sit_and_reach_result` double NOT NULL,
  `stick_drop_test_one` double NOT NULL,
  `stick_drop_test_two` double NOT NULL,
  `stick_drop_test_three` double NOT NULL,
  `stick_drop_test_four` double NOT NULL,
  `stick_drop_test_result` double NOT NULL,
  `zipper_test_one` double NOT NULL,
  `zipper_test_two` double NOT NULL,
  `zipper_test_result` double NOT NULL,
  `status_id` bigint UNSIGNED NOT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 306 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (305, 44, 2, 4, 'RYOMEN SUKUNA', 1, 1, 1, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 3, '-', '2024-04-27 15:44:21', '2024-04-27 23:44:21', NULL);
INSERT INTO `students` VALUES (306, 43, 5, 4, 'RYOMEN SUKUNA', 1, 1, 1, 100, 22, 100, 454.54545454545, 100, 22, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 4, '-ddddd', '2024-04-27 16:51:42', '2024-04-28 00:51:42', NULL);

-- ----------------------------
-- Table structure for subscriptions
-- ----------------------------
DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `evaluation_id` bigint UNSIGNED NOT NULL,
  `isp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `type` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `location` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `average_speed` double NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `subscriptions to evaluations`(`evaluation_id` ASC) USING BTREE,
  CONSTRAINT `subscriptions to evaluations` FOREIGN KEY (`evaluation_id`) REFERENCES `evaluations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subscriptions
-- ----------------------------

-- ----------------------------
-- Table structure for teams
-- ----------------------------
DROP TABLE IF EXISTS `teams`;
CREATE TABLE `teams`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `team_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_active` tinyint NOT NULL DEFAULT 0,
  `order_position` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `teams to users`(`user_id` ASC) USING BTREE,
  CONSTRAINT `teams to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of teams
-- ----------------------------
INSERT INTO `teams` VALUES (2, 2, 'MA ANGELA L. PARAGAS', 'team-img/6615e1265a017377323874_2085746898448771_8555122959557130172_n.png', 'ADMINISTRATIVE OFFICER IV', 1, 1, '2024-04-09 18:14:04', '2024-04-10 16:45:26', NULL);
INSERT INTO `teams` VALUES (3, 2, 'DJSON B. JULIO', 'team-img/6615e12cbcb6f377323874_2085746898448771_8555122959557130172_n.png', 'ADMINISTRATIVE ASSISTANT III', 1, 2, '2024-04-09 22:25:52', '2024-04-10 16:45:32', NULL);

-- ----------------------------
-- Table structure for trackings
-- ----------------------------
DROP TABLE IF EXISTS `trackings`;
CREATE TABLE `trackings`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `transaction_id` bigint UNSIGNED NOT NULL,
  `progression_id` bigint UNSIGNED NOT NULL,
  `status_id` bigint UNSIGNED NOT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `trackings to users`(`user_id` ASC) USING BTREE,
  INDEX `trackings to transactions`(`transaction_id` ASC) USING BTREE,
  INDEX `trackings to progressions`(`progression_id` ASC) USING BTREE,
  INDEX `trackings to statuses`(`status_id` ASC) USING BTREE,
  CONSTRAINT `trackings to progressions` FOREIGN KEY (`progression_id`) REFERENCES `progressions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trackings to statuses` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trackings to transactions` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `trackings to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of trackings
-- ----------------------------

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `docsid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `establishment_id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `transaction_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date_release` date NULL DEFAULT NULL,
  `received_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date_received` date NULL DEFAULT NULL,
  `remarks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `document` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `has_file` tinyint NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `transactions to users`(`user_id` ASC) USING BTREE,
  INDEX `transactions to establishments`(`establishment_id` ASC) USING BTREE,
  INDEX `transactions to departments`(`department_id` ASC) USING BTREE,
  CONSTRAINT `transactions to departments` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `transactions to establishments` FOREIGN KEY (`establishment_id`) REFERENCES `establishments` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `transactions to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of transactions
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_admin` tinyint NOT NULL DEFAULT 0,
  `is_records` tinyint NOT NULL DEFAULT 0,
  `is_mapeh` tinyint NOT NULL DEFAULT 0,
  `is_research` tinyint NOT NULL DEFAULT 0,
  `is_ict_monitoring` tinyint NOT NULL DEFAULT 0,
  `is_client` tinyint NOT NULL DEFAULT 0,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (2, 'rodrigo', 'admin', 'cabotaje.rodrigo@gmail.com', '$2y$10$q73YTHRg2s2png40Ph0PZODf3rZbjtUcGZAqs3mJScSd45dwKf17C', 1, 1, 1, 1, 1, 0, '6635e84332e266635e84332e2a', '2024-04-09 04:34:26', '2024-05-04 23:48:19', NULL);
INSERT INTO `users` VALUES (3, 'records', 'records', 'records@gmail.com', '$2y$10$ez2TRqJnShOuhS.rdogzXO7de/Nt5Zhmt673P2B6wVzazBfxiqcaq', 0, 1, 0, 0, 0, 0, '6635a08aa41cd6635a08aa41d1', '2024-04-21 19:01:19', '2024-05-04 18:42:18', NULL);
INSERT INTO `users` VALUES (4, 'research', 'research', 'research@gmail.com', '$2y$10$y5MMg//xaxomUIiPFuUoI.8uXGDuSQwo04cfAi/9AjbukgNO5JEbq', 0, 0, 0, 1, 0, 0, '6635b52888d2e6635b52888d32', '2024-04-21 21:41:56', '2024-05-04 20:10:16', NULL);
INSERT INTO `users` VALUES (5, 'mapeh', 'mapeh', 'mapeh@gmail.com', '$2y$10$PDrAno3IOjlvBocHNMSrZO5jKkWA9c80JRhvXO3lqXgrb/m0p4/0e', 0, 0, 1, 0, 0, 0, '6635ac2d88be56635ac2d88bea', '2024-04-21 22:40:18', '2024-05-04 19:31:57', NULL);
INSERT INTO `users` VALUES (6, 'ict', 'ict', 'ict@gmail.com', '$2y$10$hREmdTAVdYJKInz4Ch5tN.c0mdlGrxpIOviFFpVUhhRSwqnPnVw3S', 0, 0, 0, 0, 1, 0, '6635f7a51e2666635f7a51e26a', '2024-04-29 02:48:43', '2024-05-05 00:53:57', NULL);

-- ----------------------------
-- Table structure for views
-- ----------------------------
DROP TABLE IF EXISTS `views`;
CREATE TABLE `views`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint UNSIGNED NOT NULL,
  `project_id` bigint UNSIGNED NOT NULL,
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `year` year NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `views to users`(`user_id` ASC) USING BTREE,
  INDEX `views to projects`(`project_id` ASC) USING BTREE,
  CONSTRAINT `views to projects` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `views to users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of views
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
