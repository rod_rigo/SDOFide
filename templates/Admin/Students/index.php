<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student[]|\Cake\Collection\CollectionInterface $students
 */
?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<div class="row">
    <?=$this->Form->create(null,['id' => 'form', 'type' => 'file', 'class' => 'col-sm-12 col-md-6 col-lg-6 mb-3'])?>
        <?=$this->Form->label('department', ucwords('Department'))?>
        <div class="input-group mb-3">
            <?=$this->Form->select('department', $departments,[
                'class' => 'form-control',
                'id' => 'department',
                'empty' => ucwords('All'),
                'title' => ucwords('please select Department')
            ])?>
            <div class="input-group-append">
                <?=$this->Form->button('Submit',[
                    'type' => 'submit',
                    'class' => 'input-group-text'
                ])?>
            </div>
        </div>
    <?=$this->Form->end()?>
    <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Students', 'action' => 'upload'])?>" class="btn btn-primary rounded-0 mx-2" title="Upload">
            Upload
        </a>
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Students', 'action' => 'add'])?>" id="toggle-modal" class="btn btn-primary rounded-0 mx-2" title="New Student">
            New Student
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Level</th>
                        <th>Grade</th>
                        <th>Gender</th>
                        <th>Age</th>
                        <th>Status</th>
                        <th>Scale</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 h-100">
        <div class="card">
            <div class="card-header">

            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <canvas height="400" width="800" id="student-chart" style="height: 600px !important;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/students/index')?>

