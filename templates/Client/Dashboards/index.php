<?php
/**
 * @var \App\View\AppView $this
 */
?>


<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
        <div class="card h-50">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="ion ion-clipboard mr-1"></i>
                    Request List
                </h3>
                <div class="card-tools">

                </div>
            </div>
            <div class="card-body">
                <ul class="todo-list overflow-auto overflow-y-auto h-100">
                    <?php if(!$requests->isEmpty()):?>
                        <?php foreach ($requests as $key => $request):?>
                            <li>
                                <span class="handle ui-sortable-handle">
                                    <i class="fas fa-ellipsis-v"></i>
                                    <i class="fas fa-ellipsis-v"></i>
                                </span>
                                <div class="icheck-primary d-inline ml-2">
                                    <?=$this->Form->checkbox('requests[]',[
                                        'id' => 'requests-'.($key),
                                        'hiddenField' => false,
                                        'checked' => boolval($request->is_allowed)
                                    ])?>
                                    <?=$this->Form->label('request.'.($key),null)?>
                                </div>
                                <span class="text"><?=strtoupper($request->project->title)?></span>
                                <small class="badge badge-danger">
                                    <i class="far fa-calendar"></i>
                                    <?=@(new \Moment\Moment($request->created,'Asia/Manila'))->fromNow()->getRelative()?>
                                </small>
                                <div class="tools">
                                    <?php if(boolval($request->is_allowed)):?>
                                        <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Requests', 'action' => 'download', intval($request->id)])?>" class="text-dark" target="_blank">
                                            <i class="fas fa-download" data-id="<?=intval($request->id)?>"></i>
                                        </a>
                                    <?php endif;?>
                                </div>
                            </li>
                        <?php endforeach;?>
                    <?php else:?>
                        <li>No Request List Available</li>
                    <?php endif;?>
                </ul>
            </div>

        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Recently Added Projects</h3>
                <div class="card-tools">

                </div>
            </div>

            <div class="card-body p-0">
                <ul class="products-list product-list-in-card pl-2 pr-2 h-100 overflow-y-auto">
                    <?php if(!$projects->isEmpty()):?>
                        <?php foreach ($projects as $key => $project):?>
                            <li class="item">
                                <div class="product-img">
                                    <img src="<?=$this->Url->assetUrl('/img/OIP.jpg')?>" alt="Product Image" class="img-size-50" loading="lazy">
                                </div>
                                <div class="product-info">
                                    <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Projects', 'action' => 'view', intval($project->id)])?>" class="product-title" turbolink><?=strtoupper($project->title)?>
                                        <span class="badge badge-warning float-right"><?=intval($project->batch)?></span></a>
                                    <span class="product-description">
                                        <?=strtoupper($project->author).' '.ucwords($project->position->position).', '.ucwords($project->category->category)?>
                                    </span>
                                </div>
                            </li>
                        <?php endforeach;?>
                    <?php else:?>
                        <li class="item">
                            <div class="product-img">
                                <img src="<?=$this->Url->assetUrl('/img/OIP.jpg')?>" alt="Product Image" class="img-size-50" loading="lazy">
                            </div>
                            <div class="product-info">
                                <a href="javascript:void(0)" class="product-title">
                                    No Projects In The List
                                </a>
                                <span class="product-description">

                                </span>
                            </div>
                        </li>
                    <?php endif;?>
                </ul>
            </div>
            <div class="card-footer text-center">
                <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Projects', 'action' => 'index'])?>" class="uppercase" turbolink>
                    Go To Projects
                </a>
            </div>
        </div>
    </div>
    <?php foreach ($comments as $key => $comment):?>
        <div class=" col-sm-12 col-md-6 col-lg-6 mt-2">
            <div class="card card-widget">
                <div class="card-header">
                    <div class="user-block">

                    <span class="username">
                        <a href="javascript:void (0);"><?=strtoupper($comment->user->name)?></a>
                    </span>
                        <span class="description">
                              <?=@(new \Moment\Moment($comment->created,'Asia/Manila'))->fromNow()->getRelative()?>
                        </span>
                    </div>
                    <div class="card-tools">

                    </div>
                </div>
                <div class="card-body">
                    <p><?=$comment->comment?></p>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>

