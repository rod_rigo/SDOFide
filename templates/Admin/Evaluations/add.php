<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Evaluation $evaluation
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $heads
 * @var \Cake\Collection\CollectionInterface|string[] $departments
 */
?>

<?= $this->Form->create($evaluation,['type' => 'file', 'id' => 'form', 'class' => 'row']) ?>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">A. Basic Details</h3>
            </div>

            <div class="form-horizontal">
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-8 col-lg-7">
                            <?=$this->Form->label('head_id', ucwords('School'))?>
                            <?=$this->Form->select('head_id', $heads,[
                                'class' => 'form-control',
                                'id' => 'head-id',
                                'required' => true,
                                'empty' => ucwords('Select School'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-5">
                            <?=$this->Form->label('monitoring_type_id', ucwords('monitoring type'))?>
                            <?=$this->Form->select('monitoring_type_id', $monitoringTypes,[
                                'class' => 'form-control',
                                'id' => 'monitoring-type-id',
                                'required' => true,
                                'empty' => ucwords('select monitoring type'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->label('progression_id', ucwords('progression'))?>
                            <?=$this->Form->select('progression_id', $progressions,[
                                'class' => 'form-control',
                                'id' => 'progression-id',
                                'required' => true,
                                'empty' => ucwords('select progression'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->label('status_id', ucwords('status'))?>
                            <?=$this->Form->select('status_id', [],[
                                'class' => 'form-control',
                                'id' => 'status-id',
                                'required' => true,
                                'empty' => ucwords('Choose Progression'),
                                'pattern' => '(.){1,}',
                                'title' => ucwords('Please Fill Out This Field')
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <?=$this->Form->label('date', ucwords('date'))?>
                            <?=$this->Form->date('date',[
                                'class' => 'form-control',
                                'id' => 'date',
                                'required' => true,
                                'title' => ucwords('Please Fill Out This Field')
                            ])?>
                            <small></small>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title text-uppercase">B. DCP Equipment Details:</h3>
            </div>
            <div class="form-horizontal">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-head-fixed text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>INDICATOR</th>
                                        <th>1st Delivery</th>
                                        <th>2nd Delivery</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>DCP Batch</td>
                                            <td>
                                                <?=$this->Form->text('equipments.0.dcp_batch',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-0-dcp-batch'
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->text('equipments.1.dcp_batch',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-1-dcp-batch'
                                                ])?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Year Delivered</td>
                                            <td>
                                                <?=$this->Form->year('equipments.0.year_delivered',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-0-year-delivered',
                                                    'min' => 2000
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->year('equipments.1.year_delivered',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-1-year-delivered',
                                                    'min' => 2000
                                                ])?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Package Type</td>
                                            <td>
                                                <?=$this->Form->text('equipments.0.package_type',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-0-package-type'
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->text('equipments.1.package_type',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-1-package-type'
                                                ])?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Location</td>
                                            <td>
                                                <?=$this->Form->text('equipments.0.location',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-0-location'
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->text('equipments.1.location',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-1-location'
                                                ])?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Warranty Expiration Date</td>
                                            <td>
                                                <?=$this->Form->date('equipments.0.warranty_expiration_date',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-0-dcp-batch'
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->date('equipments.1.warranty_expiration_date',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'equipments-1-dcp-batch'
                                                ])?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>DR Available?</td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.0.is_dr_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-0-is-dr-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_dr_yes.0',[
                                                            'id' => 'is-dr-yes-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-dr-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_dr_yes.0','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_dr_no.0',[
                                                            'id' => 'is-dr-no-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-dr-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_dr_no.0','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.1.is_dr_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-1-is-dr-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_dr_yes.1',[
                                                            'id' => 'is-dr-yes-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-dr-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_dr_yes.1','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_dr_no.1',[
                                                            'id' => 'is-dr-no-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-dr-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_dr_no.1','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>AIR Available?</td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.0.is_iar_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-0-is-iar-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_iar_yes.0',[
                                                            'id' => 'is-iar-yes-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-iar-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_iar_yes.0','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_iar_no.0',[
                                                            'id' => 'is-iar-no-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-iar-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_iar_no.0','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.1.is_iar_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-1-is-iar-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_iar_yes.1',[
                                                            'id' => 'is-iar-yes-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-iar-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_iar_yes.1','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_iar_no.1',[
                                                            'id' => 'is-iar-no-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-iar-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_iar_no.1','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>IRP Available?</td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.0.is_irp_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-0-is-irp-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_irp_yes.0',[
                                                            'id' => 'is-irp-yes-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-irp-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_irp_yes.0','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_irp_no.0',[
                                                            'id' => 'is-irp-no-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-irp-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_irp_no.0','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.1.is_irp_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-1-is-irp-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_irp_yes.1',[
                                                            'id' => 'is-irp-yes-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-irp-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_irp_yes.1','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_irp_no.1',[
                                                            'id' => 'is-irp-no-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-irp-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_irp_no.1','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>PTR Available?</td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.0.is_ptr_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-0-is-ptr-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_ptr_yes.0',[
                                                            'id' => 'is-ptr-yes-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-ptr-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_ptr_yes.0','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_ptr_no.0',[
                                                            'id' => 'is-ptr-no-0',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(0),
                                                            'class' => 'is-ptr-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_ptr_no.0','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-row justify-content-around align-items-center">
                                                    <?=$this->Form->hidden('equipments.1.is_ptr_available',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'equipments-1-is-ptr-available'
                                                    ])?>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_ptr_yes.1',[
                                                            'id' => 'is-ptr-yes-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-ptr-yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('is_ptr_yes.1','Yes')?>
                                                    </div>

                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('is_ptr_no.1',[
                                                            'id' => 'is-ptr-no-1',
                                                            'hiddenField' => false,
                                                            'data-id' => intval(1),
                                                            'class' => 'is-ptr-no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('is_ptr_no.1','No')?>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title text-uppercase">C. E-Classroom/Laboratory Conditions:</h3>
            </div>
            <div class="form-horizontal">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-head-fixed text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>INDICATOR</th>
                                        <th>YES</th>
                                        <th>NO</th>
                                        <th>REMARKS</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $c = 0;?>
                                    <?php foreach ($conditions as $condition):?>
                                        <tr>
                                            <td><strong><?=strtoupper($condition->condition)?></strong></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <?php foreach ($condition->indicators as $indicator):?>
                                            <tr>
                                                <td>
                                                    <?=ucwords($indicator->indicator)?>
                                                </td>
                                                <td class="text-center">
                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('yes.'.(intval($c)),[
                                                            'id' => 'yes-'.(intval($c)),
                                                            'hiddenField' => false,
                                                            'data-id' => intval($c),
                                                            'class' => 'yes',
                                                            'label' => false,
                                                            'value' => intval(1)
                                                        ])?>
                                                        <?=$this->Form->label('yes.'.(intval($c)),'')?>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <div class="icheck-primary">
                                                        <?=$this->Form->checkbox('no.'.(intval($c)),[
                                                            'id' => 'no-'.(intval($c)),
                                                            'hiddenField' => false,
                                                            'data-id' => intval($c),
                                                            'class' => 'no',
                                                            'label' => false,
                                                            'value' => intval(2)
                                                        ])?>
                                                        <?=$this->Form->label('no.'.(intval($c)),'')?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?=$this->Form->text('classrooms.'.(intval($c)).'.remarks',[
                                                        'class' => 'form-control form-control-border',
                                                        'id' => 'classrooms-'.(intval($c)).'-remarks',
                                                        'placeholder' => ucwords('Remarks')
                                                    ])?>
                                                </td>
                                                <?=$this->Form->hidden('classrooms.'.(intval($c)).'.condition_id',[
                                                    'id' => 'classrooms-'.(intval($c)).'-condition-id',
                                                    'value' => intval($condition->id)
                                                ])?>
                                                <?=$this->Form->hidden('classrooms.'.(intval($c)).'.indicator_id',[
                                                    'id' => 'classrooms-'.(intval($c)).'-indicator-id',
                                                    'value' => intval($indicator->id)
                                                ])?>
                                                <?=$this->Form->hidden('classrooms.'.(intval($c)).'.is_yes',[
                                                    'id' => 'classrooms-'.(intval($c)).'-is-yes',
                                                    'value' => intval(0)
                                                ])?>
                                            </tr>
                                            <?php $c++?>
                                        <?php endforeach;?>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-uppercase">D. Internet Connectivity Subscription (used by learners and teachers):</h3>
        </div>
        <div class="form-horizontal">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-head-fixed text-nowrap">
                                <thead>
                                <tr>
                                    <th>ISP</th>
                                    <th>TYPE</th>
                                    <th>LOCATION</th>
                                    <th>AVERAGE SPEED (MBPS)</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for($s = 0; $s < 3; $s++):?>
                                        <tr>
                                            <td>
                                                <?=$this->Form->text('subscriptions.'.(intval($s)).'.isp',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'subscriptions-'.(intval($s)).'-isp',
                                                    'placeholder' => ucwords('ISP')
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->text('subscriptions.'.(intval($s)).'.type',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'subscriptions-'.(intval($s)).'-type',
                                                    'placeholder' => ucwords('Type')
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->text('subscriptions.'.(intval($s)).'.location',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'subscriptions-'.(intval($s)).'-location',
                                                    'placeholder' => ucwords('Location')
                                                ])?>
                                            </td>
                                            <td>
                                                <?=$this->Form->text('subscriptions.'.(intval($s)).'.average_speed',[
                                                    'class' => 'form-control form-control-border',
                                                    'id' => 'subscriptions-'.(intval($s)).'-average-speed',
                                                    'placeholder' => ucwords('Average Speed')
                                                ])?>
                                            </td>
                                        </tr>
                                    <?php endfor;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title text-uppercase">E. Findings/Analysis</h3>
            </div>
            <div class="form-horizontal">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('analysis', ucwords('analysis'))?>
                            <?=$this->Form->textarea('analysis',[
                                'class' => 'form-control',
                                'id' => 'analysis',
                                'required' => false,
                                'placeholder' => ucwords('analysis'),
                            ])?>
                            <small></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title text-uppercase">F. Recommendations</h3>
            </div>
            <div class="form-horizontal">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <?=$this->Form->label('recommendations', ucwords('recommendations'))?>
                            <?=$this->Form->textarea('recommendations',[
                                'class' => 'form-control',
                                'id' => 'recommendations',
                                'required' => false,
                                'placeholder' => ucwords('recommendations'),
                            ])?>
                            <small></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="card-footer d-flex justify-content-end align-items-center">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Evaluations', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                        Return
                    </a>
                    <?= $this->Form->button(__('Submit'),[
                        'class' => 'btn btn-success rounded-0',
                        'title' => ucwords('Submit')
                    ])?>
                    <?=$this->Form->hidden('user_id',[
                        'id' => 'user-id',
                        'required' => true,
                        'readonly' => true,
                        'value' => intval(@$auth['id'])
                    ])?>
                    <?=$this->Form->hidden('department_id',[
                        'id' => 'department-id',
                        'required' => true,
                        'readonly' => true,
                        'value' => intval(@$departments->id)
                    ])?>
                </div>
            </div>
        </div>
    </div>
<?= $this->Form->end() ?>

<?=$this->Html->script('admin/evaluations/add')?>