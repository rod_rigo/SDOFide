<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MonitoringDatesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MonitoringDatesTable Test Case
 */
class MonitoringDatesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MonitoringDatesTable
     */
    protected $MonitoringDates;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MonitoringDates',
        'app.Evaluations',
        'app.MonitoringTypes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('MonitoringDates') ? [] : ['className' => MonitoringDatesTable::class];
        $this->MonitoringDates = $this->getTableLocator()->get('MonitoringDates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MonitoringDates);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\MonitoringDatesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\MonitoringDatesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
