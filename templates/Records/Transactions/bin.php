<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Transaction[]|\Cake\Collection\CollectionInterface $transactions
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Docs ID</th>
                        <th>Transaction Name</th>
                        <th>Establishment</th>
                        <th>Department</th>
                        <th>Date Release</th>
                        <th>Received By</th>
                        <th>Date Received</th>
                        <th>Progression</th>
                        <th>Status</th>
                        <th>Remarks</th>
                        <th>Modified By</th>
                        <th>Deleted</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('records/transactions/bin')?>
