'use strict';
$(document).ready(function (e) {

    const baseurl = mainurl+'evaluations/edit/'+(id);

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action:'replace'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('.is-dr-yes').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-dr-no-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-dr-available').val(value);
    });

    $('.is-dr-no').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-dr-yes-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-dr-available').val(value);
    });

    $('.is-iar-yes').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-iar-no-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-iar-available').val(value);
    });

    $('.is-iar-no').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-iar-yes-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-iar-available').val(value);
    });

    $('.is-irp-yes').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-irp-no-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-irp-available').val(value);
    });

    $('.is-irp-no').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-irp-yes-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-irp-available').val(value);
    });

    $('.is-ptr-yes').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-ptr-no-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-ptr-available').val(value);
    });

    $('.is-ptr-no').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#is-ptr-yes-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#equipments-'+(dataId)+'-is-ptr-available').val(value);
    });

    $('.yes').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#no-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#classrooms-'+(dataId)+'-is-yes').val(value);
    });

    $('.no').change(function () {
        var value = $(this).val();
        var dataId = $(this).attr('data-id');
        $('#yes-'+(dataId)+'').prop('checked', false);
        $(this).prop('checked', true);
        $('#classrooms-'+(dataId)+'-is-yes').val(value);
    });

    $('#progression-id').change(function (e) {
        var value = $(this).val();
        if(value){
            getStatusesList(value);
        }
    });

    function getStatusesList(progressionId) {
        $.ajax({
            url: mainurl+'statuses/getStatusesList/'+(progressionId),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('#status-id').empty().prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#status-id').append('<option value="">Select Status</option>');
            if(parseInt((Object.keys(data)).length) >= parseInt(1)){
                $.map(data, function (data, key) {
                    $('#status-id').append('<option value="'+(key)+'">'+(data)+'</option>');
                });
            }
            $('#status-id').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});