'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'projects/';
    var url = '';
    var isPublished = [
        'No',
        'Yes'
    ];

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getProjectsOngoing',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {

        },
        buttons:[
            {
                text: 'PDF',
                action: function (e) {
                    Swal.fire({
                        title:'Export To PDF',
                        text: null,
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            // window.open(baseurl+'pdf');

                            var data = table.rows( {search: 'applied'} ).data().toArray();
                            var content = [
                                [
                                    { text: 'Title', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Author', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Position', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'School', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Batch', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Date', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Category', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Due Date', bold: true, fontSize: 10, alignment: 'center' },
                                    { text: 'Remarks', bold: true, fontSize: 10, alignment: 'center' }
                                ]
                            ];
                            $.map(data, function (value, key) {
                                content.push([
                                    {
                                        text : value.title,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.author,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.position.position,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.school.school,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : parseInt(value.batch),
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.date,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.category.category,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.due_date,
                                        fontSize: 10,
                                        alignment: 'center'
                                    },
                                    {
                                        text : value.remarks,
                                        fontSize: 10,
                                        alignment: 'center'
                                    }
                                ]);
                            });


                            var docDefinition = {
                                info: {
                                    title: 'Project List',
                                    author: null,
                                    subject: null,
                                    keywords: null,
                                    creator: null,
                                    creationDate: moment().format('Y-m-d H:m:s A'),
                                    modDate: moment().format('Y-m-d H:m:s A'),
                                },
                                pageOrientation: 'landscape',
                                pageSize: 'A4',
                                pageMargins: [ 10, 120, 10, 65 ],
                                header: function (currentPage, pageCount) {
                                    return {
                                        image:header,
                                        width: 800,
                                        height: 120,
                                        margin: [ 1, 1, 1, 1 ],
                                        alignment: 'center'
                                    };
                                },
                                footer: function(currentPage, pageCount) {
                                    return {
                                        image:footer,
                                        width: 800,
                                        height: 65,
                                        margin: [ 1, 1, 1, 1 ],
                                        alignment: 'center'
                                    };
                                },
                                content: [
                                    {
                                        layout: 'lightHorizontalLines', // optional
                                        table: {
                                            headerRows: 1,
                                            widths: [88, 88, 88, 88, 88, 88, 88, 88],
                                            body: content,
                                        }
                                    },
                                    {
                                        width: '100%',
                                        text: 'Prepared:',
                                        alignment:'left',
                                        margin: [ 0, 15, 0, 5 ]
                                    },
                                    {
                                        width: '100%',
                                        text: ('Mark Vincent Fernandez').toUpperCase(),
                                        alignment:'left',
                                        bold: true,
                                        margin: [ 0, 10, 0, 3 ]
                                    },
                                    {
                                        width: '100%',
                                        text: 'Division Research Focal Person',
                                        alignment:'left',
                                        italics: true,
                                        margin: [ 0, 0, 0, 5 ]
                                    },
                                    {
                                        width: '100%',
                                        text: 'Reviewed:',
                                        alignment:'left',
                                        margin: [ 0, 15, 0, 5 ]
                                    },
                                    {
                                        columns:[
                                            {
                                                text: ('Rosalia B Gutierrez EdD').toUpperCase(),
                                                bold:true,
                                            },
                                            {
                                                text: ('Jacqueline S. Ramos PhD, CESE').toUpperCase(),
                                                bold:true,

                                            },
                                        ]
                                    },
                                    {
                                        columns:[
                                            {
                                                text: 'Chief Education Supervisor, SGOD',
                                                italics:true,
                                            },
                                            {
                                                text: 'OIC-Assistant Schools Division Superintendent',
                                                italics:true,

                                            },
                                        ]
                                    },
                                    {
                                        columns:[
                                            {
                                                text: null,
                                            },
                                            {
                                                text: 'Chairperson, Schools Division Research Committee',
                                                italics:true,
                                            },
                                        ]
                                    },
                                    {
                                        width: '100%',
                                        text: 'Noted:',
                                        alignment:'left',
                                        margin: [ 0, 15, 0, 5 ]
                                    },
                                    {
                                        width: '100%',
                                        text: ('Flordeliza C. Gecobe PhD, CESO V').toUpperCase(),
                                        alignment:'left',
                                        bold: true,
                                        margin: [ 0, 10, 0, 3 ]
                                    },
                                    {
                                        width: '100%',
                                        text: 'Schools Division Superintendent',
                                        alignment:'left',
                                        italics: true,
                                        margin: [ 0, 0, 0, 5 ]
                                    },
                                ],
                            };

                            pdfMake.createPdf(docDefinition).open();
                        }
                    })
                },
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                footer:true
            },
            {
                text: 'DOCX',
                action: function (e) {
                    Swal.fire({
                        title:'Export To DOCX',
                        text: null,
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            // window.open(baseurl+'pdf');

                            var data = table.rows( {search: 'applied'} ).data().toArray();
                            var rows = [
                                new docx.TableRow({
                                    children: [
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Title',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Author',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Position',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'School',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Batch',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Date',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Category',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Due Date',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: 'Remarks',
                                                            bold: true,
                                                            size:28
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                    ],
                                    tableHeader: true,
                                    cantSplit: true,
                                }),
                            ];
                            $.map(data, function (value, key) {
                                rows.push(new docx.TableRow({
                                    children: [
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.title,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.author,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.position.position,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.school.school,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.batch,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.date,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.category.category,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.due_date,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                        new docx.TableCell({
                                            width: {
                                                size: 5000,
                                                type: docx.WidthType.DXA,
                                            },
                                            margins: {
                                                top: docx.convertInchesToTwip(0.18),
                                                bottom: docx.convertInchesToTwip(0.18),
                                                left: docx.convertInchesToTwip(0.18),
                                                right: docx.convertInchesToTwip(0.18),
                                            },
                                            verticalAlign: docx.VerticalAlign.CENTER,
                                            children: [
                                                new docx.Paragraph({
                                                    text: null,
                                                    alignment: docx.AlignmentType.CENTER,
                                                    children:[
                                                        new docx.TextRun({
                                                            text: value.remarks,
                                                            bold: false,
                                                            size: 22,
                                                        }),
                                                    ]
                                                })
                                            ],
                                        }),
                                    ],
                                    tableHeader: false,
                                    cantSplit: false,
                                }));
                            });

                            var doc = new docx.Document({
                                creator: null,
                                description: null,
                                title: null,
                                subject: null,
                                keywords: null,
                                lastModifiedBy: null,
                                revision: null,
                                externalStyles: null,
                                styles: null,
                                numbering: null,
                                footnotes: null,
                                hyperlinks: null,
                                background: null,
                                sections: [
                                    {
                                        properties: {
                                            page: {
                                                size: {
                                                    width: 16840, // Landscape orientation width for A4 size
                                                    height: 11905 // Landscape orientation height for A4 size
                                                },
                                                orientation: docx.PageOrientation.LANDSCAPE, // Set landscape orientation
                                                type: docx.SectionType.CONTINUOUS,
                                            }
                                        },
                                        headers: {
                                            default: new docx.Header({
                                                children: [
                                                    new docx.Paragraph({
                                                        children: [
                                                            new docx.ImageRun({
                                                                data: header,
                                                                transformation: {
                                                                    width: 1000,
                                                                    height: 200,
                                                                },
                                                            }),
                                                        ],
                                                    }),
                                                ],
                                            }),
                                        },
                                        footers: {
                                            default: new docx.Footer({
                                                children: [
                                                    new docx.Paragraph({
                                                        children: [
                                                            new docx.ImageRun({
                                                                data: footer,
                                                                transformation: {
                                                                    width: 1000,
                                                                    height: 100,
                                                                },
                                                            }),
                                                        ],
                                                    }),
                                                ],
                                            }),
                                        },
                                        children: [
                                            new docx.Paragraph({
                                                text: null,
                                                alignment: docx.AlignmentType.CENTER,
                                                bold: true,
                                                heading: docx.HeadingLevel.HEADING_1,
                                                children:[
                                                    new docx.TextRun({
                                                        text: 'Research',
                                                        bold: true,
                                                        size:40
                                                    }),
                                                ],
                                                pageBreakBefore: false,
                                            }),

                                            new docx.Table({
                                                columnWidths: [3505, 5505],
                                                rows: rows,
                                                indent: {
                                                    size: 600,
                                                    type: docx.WidthType.DXA,
                                                }
                                            }),

                                        ],
                                    }
                                ]
                            });

                            docx.Packer.toBlob(doc).then(function (blob) {
                                saveAs(blob, 'Projects.docx');
                            });

                        }
                    })
                },
                attr:  {
                    id: 'pdf',
                    class:'btn btn-primary rounded-0',
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            // {
            //     targets: [10],
            //     data: null,
            //     render: function(data,type,row,meta){
            //         return isPublished[parseInt(row.is_published)];
            //     }
            // },
            {
                targets: [12],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [13],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'title'},
            { data: 'author'},
            { data: 'position.position'},
            { data: 'department.department'},
            { data: 'batch'},
            { data: 'date'},
            { data: 'category.category'},
            { data: 'status.status'},
            { data: 'remarks'},
            // { data: 'is_published'},
            { data: 'due_date'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'view/'+(parseInt(dataId));
        Turbolinks.visit(href,{action:'replace'});
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    $('#category').on('input',  function (e) {
        filters();
    });

    function filters() {
        var category = $('#category option:selected').val();
        table.columns([7])
            .search( (category) ? '^'+(category)+'$' : (category), true, false )
            .draw();

        var position = $('#position option:selected').val();
        table.columns([3])
            .search( (position) ? '^'+(position)+'$' : (position), true, false )
            .draw();

        var school = $('#school option:selected').val();
        table.columns([4])
            .search( (school) ? '^'+(school)+'$' : (school), true, false )
            .draw();
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});