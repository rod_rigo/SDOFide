<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Project> $projects
 */

$path = WWW_ROOT. 'img'. DS. 'header.jpg';
$image = base64_encode(file_get_contents($path));
$mime = mime_content_type($path);
$header = 'data:'.($mime).';base64,'.($image);

$path = WWW_ROOT. 'img'. DS. 'footer.jpg';
$image = base64_encode(file_get_contents($path));
$mime = mime_content_type($path);
$footer = 'data:'.($mime).';base64,'.($image);

?>

    <style>
        .dataTables_length{
            width: 25%;
            float: left;
        }
        .dt-buttons{
            position: relative;
            width: 50%;
        }
        .dataTables_filter{
            width: 25%;
            float: right;
        }
        @media (max-width: 700px) {
            .dataTables_length{
                width: 100%;
            }
            .dt-buttons{
                width: 100%;
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center;
                margin: 1em;
            }
            .dataTables_filter{
                width: 100%;
            }
        }
    </style>

    <?=$this->Html->script([
        '/pdf-make/js/pdfmake',
        '/pdf-make/js/vfs_fonts',
        '/docx-js/js/docx-js',
        '/file-saver/js/FileSaver'
    ])?>
    <script>
        var header = '<?=$header?>';
        var footer = '<?=$footer?>';
    </script>
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'add'])?>" turbolink id="toggle-modal" class="btn btn-primary rounded-0" title="New Project">
            New Project
        </a>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
        <?=$this->Form->label('category', ucwords('category'))?>
        <?=$this->Form->select('category', $categories,[
            'class' => 'form-control rounded-0',
            'id' => 'category',
            'empty' => ucwords('category')
        ])?>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
        <?=$this->Form->label('position', ucwords('position'))?>
        <?=$this->Form->select('position', $positions,[
            'class' => 'form-control rounded-0',
            'id' => 'position',
            'empty' => ucwords('position')
        ])?>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
        <?=$this->Form->label('school', ucwords('school'))?>
        <?=$this->Form->select('school', $departments,[
            'class' => 'form-control rounded-0',
            'id' => 'school',
            'empty' => ucwords('school')
        ])?>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Position</th>
                        <th>Department</th>
                        <th>Batch</th>
                        <th>Completion Date</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Remarks</th>
<!--                        <th>Published</th>-->
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<?=$this->Html->script('admin/projects/index')?>
