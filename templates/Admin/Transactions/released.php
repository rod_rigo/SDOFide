<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Transaction[]|\Cake\Collection\CollectionInterface $transactions
 */
?>

<?=$this->Html->css('admin/transactions/released')?>

<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-8 mb-3">
        <?=$this->Form->create(null,['class' => 'row', 'id' => 'form', 'type' => 'file'])?>
            <div class="col-sm-12 col-md-3 col-lg-4">
                <?=$this->Form->label('start_date', ucwords('Start Date'))?>
                <?=$this->Form->date('start_date',[
                    'class' => 'form-control rounded-0',
                    'id' => 'start-date',
                    'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d')
                ])?>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-4">
                <?=$this->Form->label('end_date', ucwords('End Date'))?>
                <?=$this->Form->date('end_date',[
                    'class' => 'form-control rounded-0',
                    'id' => 'end-date',
                    'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d')
                ])?>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-2">
                <?=$this->Form->label('records', ucwords('Records'))?>
                <?=$this->Form->number('records',[
                    'class' => 'form-control rounded-0',
                    'id' => 'records',
                    'value' => 10000,
                    'min' => 10000
                ])?>
            </div>
            <div class="col-sm-12 col-md-2 col-lg-2 d-flex justify-content-start align-items-end">
                <?=$this->Form->button('Search',[
                    'type' => 'submit',
                    'class' => 'btn btn-primary rounded-0',
                ])?>
            </div>
        <?=$this->Form->end()?>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-4 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'add'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="New Transaction">
            New Transaction
        </a>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Docs ID</th>
                        <th>Transaction Name</th>
                        <th>Establishment</th>
                        <th>Department</th>
                        <th>Date Release</th>
                        <th>Received By</th>
                        <th>Date Received</th>
                        <th>Progression</th>
                        <th>Status</th>
                        <th>Remarks</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/transactions/released')?>
