<?php
/**
 * @var \App\View\AppView $this
 *
 */
?>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Student Form</h3>
                </div>
                <?= $this->Form->create($student,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-12 col-md-4 col-lg-3 mt-3">
                            <?=$this->Form->label('gender_id', ucwords('gender'))?>
                            <?=$this->Form->select('gender_id', $genders,[
                                'class' => 'form-control',
                                'id' => 'gender-id',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('Select gender'),
                                'pattern' => '(.){1,}',
                                'required' => true,
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-5 col-lg-5 mt-3">
                            <?=$this->Form->label('department_id', ucwords('Department'))?>
                            <?=$this->Form->select('department_id', $departments,[
                                'class' => 'form-control',
                                'id' => 'department-id',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('Select department'),
                                'pattern' => '(.){1,}',
                                'required' => true,
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <?=$this->Form->label('level_id', ucwords('Level'))?>
                            <?=$this->Form->select('level_id', $levels,[
                                'class' => 'form-control',
                                'id' => 'level-id',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('Select level'),
                                'pattern' => '(.){1,}',
                                'required' => true,
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                            <?=$this->Form->label('grade_id', ucwords('Grade'))?>
                            <?=$this->Form->select('grade_id', [],[
                                'class' => 'form-control',
                                'id' => 'grade-id',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('Choose Level'),
                                'pattern' => '(.){1,}',
                                'required' => true,
                            ])?>
                            <small></small>
                        </div>


                        <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                            <?=$this->Form->label('status_id', ucwords('status'))?>
                            <?=$this->Form->select('status_id', $statuses,[
                                'class' => 'form-control',
                                'id' => 'status-id',
                                'title' => ucwords('please fill out this field'),
                                'empty' => ucwords('Select status'),
                                'pattern' => '(.){1,}',
                                'required' => true,
                            ])?>
                            <small></small>
                        </div>

                        <div class="col-sm-12 col-md-5 col-lg-5 mt-3">
                            <?=$this->Form->label('file', ucwords('file'))?>
                            <?=$this->Form->file('file',[
                                'class' => 'form-control',
                                'id' => 'file',
                                'title' => ucwords('please fill out this field'),
                                'accept' => '.xlsx',
                                'required' => true,
                            ])?>
                            <small></small>
                        </div>

                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end align-items-center">
                    <?=$this->Form->hidden('establishment_id',[
                        'id' => 'establishment-id',
                        'required' => true,
                        'readonly' => true,
                        'value' => intval($establishment->id)
                    ])?>
                    <?=$this->Form->hidden('user_id',[
                        'id' => 'user-id',
                        'required' => true,
                        'readonly' => true,
                        'value' => intval(@$auth['id'])
                    ])?>
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Students', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                        Return
                    </a>
                    <?= $this->Form->button(__('Submit'),[
                        'class' => 'btn btn-success rounded-0',
                        'title' => ucwords('Submit')
                    ])?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>

<?=$this->Html->script('admin/students/upload')?>