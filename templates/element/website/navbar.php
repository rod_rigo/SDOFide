<?php
/**
 * @var \App\View\AppView $this
 */

$memos = \Cake\ORM\TableRegistry::getTableLocator()->get('Memos')
    ->find()
    ->where([
        'is_shown =' => intval(1)
    ])
    ->order(['memo' => 'ASC'],true);
?>

<div class="container-fluid position-relative p-0">
    <nav class="navbar navbar-expand-lg navbar-dark px-5 py-3 py-lg-0">
        <a href="javascript:void(0);" class="navbar-brand p-0 d-flex flex-wrap">
            <img src="<?=$this->Url->assetUrl('/img/do-logo.png')?>" class="w-25" height="80" width="80" style="object-fit: contain !important;">
            <h5 class="m-0 w-75 d-flex justify-content-center align-items-center">
                Republic of the Philippines <br>
                Schools Division Office of Santiago City
            </h5>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="fa fa-bars"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto py-0">
                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Websites', 'action' => 'index'])?>" turbolink class="nav-item nav-link">Home</a>
                <?php if(!$memos->isEmpty()):?>
                    <div class="nav-item dropdown">
                        <a href="javascript:void(0);" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Memo</a>
                        <div class="dropdown-menu m-0">
                            <?php foreach($memos as $memo):?>
                                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Memos', 'action' => 'index', intval($memo->id), '?' => ['time' => time()]])?>" turbolink class="dropdown-item"><?=ucwords($memo->memo)?></a>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>
                <?php if(strtolower($controller) == strtolower('websites')):?>
                    <a href="javascript:void(0);" class="nav-item nav-link"><i class="fa fa-minus fa-rotate-90"></i></a>
                    <a href="javascript:void(0);" data-target="#about" class="nav-item nav-link scroll-link">About</a>
                    <a href="javascript:void(0);" data-target="#team" class="nav-item nav-link scroll-link">Team</a>
                    <a href="javascript:void(0);" data-target="#contact" class="nav-item nav-link scroll-link">Contact</a>
                <?php endif;?>
                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'register'])?>" class="nav-item nav-link">Register</a>
                <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'login'])?>" class="nav-item nav-link">Login</a>
            </div>
        </div>
    </nav>
</div>
