<?php

use Cake\Filesystem\File;

// Define the filename for the PDF
$filename = 'PFT';

// Function to get the base64 encoded image with mime type
function getBase64Image($filePath) {
    $image = base64_encode(file_get_contents($filePath));
    $mime = mime_content_type($filePath);
    return 'data:' . $mime . ';base64,' . $image;
}

// Paths to the header and footer images
$headerPath = WWW_ROOT . 'img' . DS . ($banner->header ?? 'header.jpg');
$footerPath = WWW_ROOT . 'img' . DS . ($banner->footer ?? 'footer.png');

// Check if header and footer files exist, else use default images
if (!file_exists($headerPath)) {
    $headerPath = WWW_ROOT . 'img' . DS . 'header.jpg';
}
if (!file_exists($footerPath)) {
    $footerPath = WWW_ROOT . 'img' . DS . 'footer.png';
}

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    public $headerPath;
    public $footerPath;

    public function __construct($headerPath, $footerPath) {
        parent::__construct();
        $this->headerPath = $headerPath;
        $this->footerPath = $footerPath;
    }

    // Page header
    public function Header() {
        // Logo
        $this->Image($this->headerPath, 20, 0, 250, 40, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 15, '', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Logo
        $this->Image($this->footerPath, 20, $this->getPageHeight() - 25, 250, 25, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// Create new PDF document
$pdf = new MYPDF($headerPath, $footerPath);

// Set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor(strval(@$auth['name']));
$pdf->SetTitle($filename);
$pdf->SetSubject($filename);
$pdf->SetKeywords($filename);
$pdf->setPageOrientation('landscape');

// Set default header data
$pdf->SetHeaderData('', 0, '', '');

// Remove default header/footer
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);

// Set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// Set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// Set margins
$pdf->SetMargins(6, 45, 6, false);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// Set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// Set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// Add a page
$pdf->AddPage('L', array(216, 331)); // Landscape orientation, A4 size (297mm x 210mm)

// HTML content for the table with 29 headers
$content = '
<table border="1" cellpadding="4">
    <thead>
        <tr>
            <th style="font-size: 10px;">Status</th>
            <th style="font-size: 10px;">Name</th>
            <th style="font-size: 10px;">School</th>
            <th style="font-size: 10px;">Grade</th>
            <th style="font-size: 10px;">Sex</th>
            <th style="font-size: 10px;">Age</th>
            <th style="font-size: 10px;">Height</th>
            <th style="font-size: 10px;">Weight</th>
            <th style="font-size: 10px;">BMI</th>
            <th style="font-size: 10px;">BMP B</th>
            <th style="font-size: 10px;">BMP A</th>
            <th style="font-size: 10px;">BP</th>
            <th style="font-size: 10px;">PU</th>
            <th style="font-size: 10px;">SSTILL</th>
            <th style="font-size: 10px;">SST RL</th>
            <th style="font-size: 10px;">40M RUN</th>
            <th style="font-size: 10px;">ZT L</th>
            <th style="font-size: 10px;">ZT R</th>
            <th style="font-size: 10px;">SDT 1st</th>
            <th style="font-size: 10px;">SDT 2nd</th>
            <th style="font-size: 10px;">SDT 3rd</th>
            <th style="font-size: 10px;">SDT MS</th>
            <th style="font-size: 10px;">SR 1st</th>
            <th style="font-size: 10px;">SR 2nd</th>
            <th style="font-size: 10px;">SIPA</th>
            <th style="font-size: 10px;">HAT C</th>
            <th style="font-size: 10px;">HAT CC</th>
            <th style="font-size: 10px;">SLJ 1st</th>
            <th style="font-size: 10px;">SLJ 2nd</th>
        </tr>
    </thead>
    <tbody>';

// Loop through students array and add rows
foreach ($students as $key => $student) {
    $content .= '<tr>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['status']['status'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['name'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['department']['department'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['level']['level']).' '.@$student['grade']['grade']).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['gender']['gender'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['age'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['height'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['weight'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['bmi'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['resting_heart_rate'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['pulse_rate'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['partial_curl_up'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['push_up'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['stork_stand_left'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['stork_stand_right'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['meter_run'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['zipper_test_one'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['zipper_test_two'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['stick_drop_test_one'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['stick_drop_test_two'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['stick_drop_test_three'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['stick_drop_test_four'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['sit_and_reach_one'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['sit_and_reach_two'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['kick_test'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['hexagon_agility_clockwise'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['hexagon_agility_counter_clockwise'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['standing_long_jump_one'])).'</td>';
    $content .= '<td style="font-size: 10px;">'.(strval(@$student['standing_long_jump_two'])).'</td>';
    $content .= '</tr>';
}

$content .= '</tbody></table>';

$content .= '<p style="line-height: 50px;">Prepared By : '.(strval($auth['name'])).'</p>';

// HTML content for the certificate
$body = '<body>' . $content . '</body>';

// Set some text to print
$html = <<<HTML
$body
HTML;

// Print a block of text using Write()
$pdf->writeHTML($html, true, false, true, false, '');

// Close and output PDF document
$pdf->lastPage();
$pdf->Output($filename . '.pdf', 'I');

exit(0);
?>
