<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Project Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $author
 * @property int $position_id
 * @property int $department_id
 * @property string $batch
 * @property \Cake\I18n\FrozenDate $date
 * @property int $category_id
 * @property string $content
 * @property int $status_id
 * @property string $project_file
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $due_date
 * @property \Cake\I18n\FrozenTime|null $expired_at
 * @property int $is_completed
 * @property int $is_expired
 * @property int $is_published
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Position $position
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Rating[] $ratings
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\View[] $views
 */
class Project extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'title' => true,
        'author' => true,
        'position_id' => true,
        'department_id' => true,
        'batch' => true,
        'date' => true,
        'category_id' => true,
        'content' => true,
        'status_id' => true,
        'project_file' => true,
        'remarks' => true,
        'due_date' => true,
        'expired_at' => true,
        'is_completed' => true,
        'is_expired' => true,
        'is_published' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'position' => true,
        'department' => true,
        'category' => true,
        'status' => true,
        'comments' => true,
        'ratings' => true,
        'requests' => true,
        'views' => true,
    ];
}
