<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Users Model
 *
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\HasMany $Departments
 * @property \App\Model\Table\EstablishmentsTable&\Cake\ORM\Association\HasMany $Establishments
 * @property \App\Model\Table\ProgressionsTable&\Cake\ORM\Association\HasMany $Progressions
 * @property \App\Model\Table\StatusesTable&\Cake\ORM\Association\HasMany $Statuses
 * @property \App\Model\Table\TrackingsTable&\Cake\ORM\Association\HasMany $Trackings
 * @property \App\Model\Table\TransactionsTable&\Cake\ORM\Association\HasMany $Transactions
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Departments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Establishments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Progressions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Statuses', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Trackings', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Transactions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Memos', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Abouts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Abouts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Contacts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Teams', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Categories', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Positions', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Projects', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Views', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasOne('Profiles', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Genders', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Levels', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Grades', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Students', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('name', true)
            ->notEmptyString('name', ucwords('please fill out this field'),false);

        $validator
            ->scalar('username')
            ->maxLength('username', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('username', true)
            ->notEmptyString('username', ucwords('please fill out this field'),false);

        $validator
            ->email('email')
            ->maxLength('name', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('email', true)
            ->notEmptyString('email', ucwords('please fill out this field'),false);

        $validator
            ->scalar('password')
            ->maxLength('password', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('password', 'create')
            ->notEmptyString('password', ucwords('please fill out this field'),true);

        $validator
            ->numeric('is_admin')
            ->notEmptyString('is_admin')
            ->add('is_admin','is_admin',[
                'rule'=> function($value){
                    $isAdmin = [0, 1];

                    if(!in_array(intval($value), $isAdmin)){
                        return ucwords('please is admin value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('is_records')
            ->notEmptyString('is_records')
            ->add('is_records','is_records',[
                'rule'=> function($value){
                    $isRecords = [0, 1];

                    if(!in_array(intval($value), $isRecords)){
                        return ucwords('please is records value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('is_mapeh')
            ->notEmptyString('is_mapeh')
            ->add('is_mapeh','is_mapeh',[
                'rule'=> function($value){
                    $isRecords = [0, 1];

                    if(!in_array(intval($value), $isRecords)){
                        return ucwords('please is mapeh value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('is_research')
            ->notEmptyString('is_research')
            ->add('is_research','is_research',[
                'rule'=> function($value){
                    $isResearch = [0, 1];

                    if(!in_array(intval($value), $isResearch)){
                        return ucwords('please is research value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('is_ict_monitoring')
            ->notEmptyString('is_ict_monitoring')
            ->add('is_ict_monitoring','is_ict_monitoring',[
                'rule'=> function($value){
                    $isIctMonitoring = [0, 1];

                    if(!in_array(intval($value), $isIctMonitoring)){
                        return ucwords('please is ict monitoring value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('is_client')
            ->notEmptyString('is_client')
            ->add('is_client','is_client',[
                'rule'=> function($value){
                    $isClient = [0, 1];

                    if(!in_array(intval($value), $isClient)){
                        return ucwords('please is client value');
                    }

                    return true;

                }
            ]);

        $validator
            ->scalar('token')
            ->maxLength('token', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('token', true)
            ->notEmptyString('token', ucwords('please fill out this field'),false);

        $validator
            ->allowEmptyString('is_active');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    public function isOwnedBy($paramId, $userId)
    {
        return $this->exists(['id' => $paramId, 'Users.id' => $userId]);
    }

    public function findAuth(Query $query, array $options)
    {
        $query->join([
                'profiles' => [
                    'table' => 'profiles',
                    'type' => 'LEFT',
                    'conditions' => [
                        'profiles.user_id = Users.id'
                    ],
                ],
                'departments' => [
                    'table' => 'departments',
                    'type' => 'LEFT',
                    'conditions' => [
                        'departments.id = profiles.department_id'
                    ],
                ],
            ])
            ->select([
                'department' => 'departments.department',
                'department_id' => 'departments.id',
                'profile_is_admin' => 'profiles.is_admin'
            ])
            ->where([
            'OR' => [ //<-- we use OR operator for our SQL
                'username' => $options['username'], //<-- username column
                'email' => $options['username'] //<-- email column
            ]], [], true)
        ->enableAutoFields();

        return $query;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }
}
