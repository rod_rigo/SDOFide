<?php
declare(strict_types=1);

namespace App\Controller\Client;

use App\Controller\AppController;
use Cake\Event\EventInterface;
use Cake\Http\Exception\ForbiddenException;

/**
 * Ratings Controller
 *
 * @property \App\Model\Table\RatingsTable $Ratings
 * @method \App\Model\Entity\Rating[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RatingsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('client');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_client'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rating = $this->Ratings->newEmptyEntity();
        if ($this->request->is('post')) {
           $rating = $this->Ratings->find()
               ->where([
                   'user_id =' => intval($this->Auth->user('id')),
                   'project_id =' => intval($this->request->getData('project_id'))
               ])
               ->first();
           if(!empty($rating)){
               $rating = $this->Ratings->patchEntity($rating, $this->request->getData());
               $rating->user_id = $this->Auth->user('id');
               if ($this->Ratings->save($rating)) {
                   $result = ['message' => ucwords('the rating has been saved'), 'result' => ucwords('success')];
                   return $this->response->withStatus(200)->withType('application/json')
                       ->withStringBody(json_encode($result));
               }else{
                   foreach ($rating->getErrors() as $key => $value){
                       $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                           'errors' => $rating->getErrors()];
                       return $this->response->withStatus(422)->withType('application/json')
                           ->withStringBody(json_encode($result));
                   }
               }
           }else{
               $rating = $this->Ratings->newEmptyEntity();
               $rating = $this->Ratings->patchEntity($rating, $this->request->getData());
               $rating->user_id = $this->Auth->user('id');
               if ($this->Ratings->save($rating)) {
                   $result = ['message' => ucwords('the rating has been saved'), 'result' => ucwords('success')];
                   return $this->response->withStatus(200)->withType('application/json')
                       ->withStringBody(json_encode($result));
               }else{
                   foreach ($rating->getErrors() as $key => $value){
                       $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                           'errors' => $rating->getErrors()];
                       return $this->response->withStatus(422)->withType('application/json')
                           ->withStringBody(json_encode($result));
                   }
               }
           }

        }
    }

}
