<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\Event\EventInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Moment\Moment;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Projects Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PositionsTable&\Cake\ORM\Association\BelongsTo $Positions
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\BelongsTo $Departments
 * @property \App\Model\Table\CategoriesTable&\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\StatusesTable&\Cake\ORM\Association\BelongsTo $Statuses
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\RatingsTable&\Cake\ORM\Association\HasMany $Ratings
 * @property \App\Model\Table\RequestsTable&\Cake\ORM\Association\HasMany $Requests
 * @property \App\Model\Table\ViewsTable&\Cake\ORM\Association\HasMany $Views
 *
 * @method \App\Model\Entity\Project newEmptyEntity()
 * @method \App\Model\Entity\Project newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Project[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Project get($primaryKey, $options = [])
 * @method \App\Model\Entity\Project findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Project patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Project[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Project|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Project saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Project[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Project[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Project[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Project[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('projects');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Positions', [
            'foreignKey' => 'position_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Statuses', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('Ratings', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('Requests', [
            'foreignKey' => 'project_id',
        ]);
        $this->hasMany('Views', [
            'foreignKey' => 'project_id',
        ]);
    }

    public function beforeFind(EventInterface $event, Query $query, \ArrayObject $options, $primary)
    {
        try{
            $this->updateAll([
                'expired_at' => new \DateTime(),
                'is_expired' => intval(1)
            ],[
                'due_date <' => (new Moment(null,'Asia/Manila'))->format('Y-m-d')
            ]);
        }catch (\Exception $exception){

        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', true)
            ->notEmptyString('title', ucwords('please fill out this field'), false);

        $validator
            ->scalar('position_id')
            ->numeric('position_id', ucwords('please select out this field'))
            ->requirePresence('position_id', true)
            ->notEmptyString('position_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('department_id')
            ->numeric('department_id', ucwords('please select out this field'))
            ->requirePresence('department_id', true)
            ->notEmptyString('department_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('category_id')
            ->numeric('category_id', ucwords('please select out this field'))
            ->requirePresence('category_id', true)
            ->notEmptyString('category_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('status_id')
            ->numeric('status_id', ucwords('please select out this field'))
            ->requirePresence('status_id', true)
            ->notEmptyString('status_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('author')
            ->maxLength('author', 255)
            ->requirePresence('author', true)
            ->notEmptyString('author', ucwords('please fill out this field'), false);

        $validator
            ->scalar('batch')
            ->maxLength('batch', 255)
            ->requirePresence('batch', true)
            ->notEmptyString('batch', ucwords('please fill out this field'), false);

        $validator
            ->date('date')
            ->requirePresence('date', true)
            ->notEmptyDate('date', ucwords('please fill out this field'), false);

        $validator
            ->scalar('content')
            ->maxLength('content', 4294967295)
            ->requirePresence('content', true)
            ->notEmptyString('content', ucwords('please fill out this field'), false);

        $validator
            ->scalar('project_file')
            ->maxLength('project_file', 255)
            ->requirePresence('project_file', false)
            ->allowEmptyString('project_file');

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 4294967295)
            ->requirePresence('remarks', true)
            ->notEmptyString('remarks', ucwords('please fill out this field'), false);

        $validator
            ->date('expired_at')
            ->allowEmptyDate('expired_at');

        $validator
            ->notEmptyString('is_completed');

        $validator
            ->notEmptyString('is_expired');

        $validator
            ->numeric('is_published')
            ->notEmptyString('is_published')
            ->add('is_published','is_published',[
                'rule'=> function($value){
                    $isPublished = [0, 1];

                    if(!in_array(intval($value), $isPublished)){
                        return ucwords('please select published value');
                    }

                    return true;

                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['position_id'], 'Positions'), ['errorField' => 'position_id']);
        $rules->add($rules->existsIn(['department_id'], 'Departments'), ['errorField' => 'department_id']);
        $rules->add($rules->existsIn(['category_id'], 'Categories'), ['errorField' => 'category_id']);
        $rules->add($rules->existsIn(['status_id'], 'Statuses'), ['errorField' => 'status_id']);

        return $rules;
    }
}
