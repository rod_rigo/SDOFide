<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="p-5 mt-3">

</div>

<div class="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
    <div class="container py-5">
        <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
            <h5 class="fw-bold text-primary text-uppercase">Contact Us</h5>
            <h1 class="mb-0">If You Have Any Query, Feel Free To Contact Us</h1>
        </div>
        <div class="row g-5">
            <div class="col-sm-12 col-md-12 col-lg-12 wow slideInUp" data-wow-delay="0.3s">
                <?=$this->Form->create($user,['id' => 'form', 'type' => 'file'])?>
                <div class="row g-3">

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <strong class="py-2">
                            <?=$this->Form->label('name', ucwords('name'))?>
                        </strong>
                        <?=$this->Form->text('name',[
                            'id' => 'name',
                            'class' => 'form-control border-0 bg-light px-4',
                            'placeholder' => ucwords('Name'),
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'style' => 'height: 55px;'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <strong class="py-2">
                            <?=$this->Form->label('profile.department_id', ucwords('Department'))?>
                        </strong>
                        <?=$this->Form->select('profile.department_id', $departments,[
                            'id' => 'profile-department-id',
                            'class' => 'form-control border-0 bg-light px-4',
                            'empty' => ucwords('Select Department'),
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'style' => 'height: 55px;'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <strong class="py-2">
                            <?=$this->Form->label('username', ucwords('username'))?>
                        </strong>
                        <?=$this->Form->text('username',[
                            'id' => 'username',
                            'class' => 'form-control border-0 bg-light px-4',
                            'placeholder' => ucwords('username'),
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'style' => 'height: 55px;'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <strong class="py-2">
                            <?=$this->Form->label('email', ucwords('email'))?>
                        </strong>
                        <?=$this->Form->email('email',[
                            'id' => 'email',
                            'class' => 'form-control border-0 bg-light px-4',
                            'placeholder' => ucwords('email'),
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'style' => 'height: 55px;'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <strong class="py-2">
                            <?=$this->Form->label('password', ucwords('password'))?>
                        </strong>
                        <?=$this->Form->password('password',[
                            'id' => 'password',
                            'class' => 'form-control border-0 bg-light px-4',
                            'placeholder' => ucwords('password'),
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'style' => 'height: 55px;'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <strong class="py-2">
                            <?=$this->Form->label('confirm_password', ucwords('password (Confirm)'))?>
                        </strong>
                        <?=$this->Form->password('confirm_password',[
                            'id' => 'confirm-password',
                            'class' => 'form-control border-0 bg-light px-4',
                            'placeholder' => ucwords('password (Confirm)'),
                            'required' => true,
                            'title' => ucwords('please fill out this field'),
                            'style' => 'height: 55px;'
                        ])?>
                        <small></small>
                    </div>



                    <div class="col-sm-12 col-12">
                        <?= $this->Form->hidden('profile.establishment_id',[
                            'id' => 'profile-establishment-id',
                            'value' => intval($establishment->id)
                        ]);?>
                        <?= $this->Form->hidden('is_admin',[
                            'id' => 'is-admin',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_records',[
                            'id' => 'is-records',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_mapeh',[
                            'id' => 'is-mapeh',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_research',[
                            'id' => 'is-research',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_ict_monitoring',[
                            'id' => 'is-ict-monitoring',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_client',[
                            'id' => 'is-client',
                            'value' => intval(1)
                        ]);?>
                        <?= $this->Form->hidden('profile.is_admin',[
                            'id' => 'profile-is-admin',
                            'value' => intval(0)
                        ]);?>
                        <?= $this->Form->hidden('is_active',[
                            'id' => 'is-active',
                            'value' => intval(1)
                        ]);?>

                        <?=$this->Form->button('Submit',[
                            'type' => 'submit',
                            'class' => 'btn btn-primary w-100 py-3'
                        ])?>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>
</div>
<script>
    'use strict';
    $(document).ready(function (e) {

        var baseurl = window.location.href;

        $('#form').submit(function (e) {
            e.preventDefault();
            var data = new FormData(this);
            $.ajax({
                url: baseurl,
                type: 'POST',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'JSON',
                beforeSend: function (e) {
                    Swal.fire({
                        icon: 'info',
                        title: null,
                        text: 'Please Wait!...',
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        timerProgressBar: false,
                        didOpen: function () {
                            Swal.showLoading();
                        }
                    });
                    $('.form-control').removeClass('is-invalid');
                    $('small').empty();
                    $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                },
            }).done(function (data, status, xhr) {
                $('#form')[0].reset();
                swal('success', null, data.message);
                Turbolinks.visit(data.redirect,{action: 'advance'});
            }).fail(function (data, status, xhr) {
                const errors = data.responseJSON.errors;

                swal('warning', null, data.responseJSON.message);

                $.map(errors, function (value, key) {
                    var name = key;
                    $.map(value, function (value, key) {
                        $('[name="'+(name)+'"]').addClass('is-invalid');
                        $('[name="'+(name)+'"]').next('small').text(value);
                    });
                });

                $('button[type="submit"], button[type="reset"]').prop('disabled', false);


            });
        });

        $('#name').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Name');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#username').on('input', function (e) {
            var regex = /^(.){1,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please Enter A Username');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#email').on('input', function (e) {
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Please @ In Email');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#password').on('input', function (e) {
            var regex = /^(.){6,}$/;
            var value = $(this).val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Password Must Be 6 Characters Long');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        $('#confirm-password').on('input', function (e) {
            var regex = /^(.){6,}$/;
            var value = $(this).val();
            var password = $('#password').val();

            if(!value.match(regex)){
                $(this).addClass('is-invalid').next('small').text('Password Must Be 6 Characters Long');
                return true;
            }

            if(value != password){
                $(this).addClass('is-invalid').next('small').text('Password Do Not Matched');
                return true;
            }

            $(this).removeClass('is-invalid').next('small').empty();

        });

        function swal(icon, result, message){
            Swal.fire({
                icon: icon,
                title: result,
                text: message,
                timer: 5000,
                timerProgressBar:true,
            })
        }

    });
</script>