'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'documents/getDocuments/'+(id)+'?search=';

    $('#search').on('input', function (e) {
        $('button[type="submit"]').html('<i class="fa fa-spinner fa-pulse"></i>');
        setTimeout(function () {
            getDocuments();
        }, 800);
    });

    $(document).on('click', '.view', function (e) {
        e.preventDefault();
        var dataUrl = $(this).attr('data-url');
        var title = $(this).attr('title');
        var date = new Date();
        $.ajax({
            url: (dataUrl)+'?timestamp='+(date.getMilliseconds()),
            type: 'HEAD',
            method: 'HEAD',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $('#embed').attr('src', (dataUrl)+'?timestamp='+(date.getMilliseconds())).attr('title', title);
            $('#modal').modal('toggle');
        }).fail(function (data, status, xhr) {
            swal('error', null, 'File Not Found')
        });
    });

    $(document).on('click', '.download', function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = mainurl+'documents/download/'+(parseInt(dataId));
        Swal.fire({
            title: 'Download File',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }
        });
    });

    function getDocuments() {
        $.ajax({
            url: baseurl+($('#search').val()),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {
                $('button[type="submit"]').html('<i class="fa fa-spinner fa-pulse"></i>');
            },
        }).done(function (data, status, xhr) {
            $('button[type="submit"]').html('<i class="bi bi-search"></i>');
            var html = '';
            $.map(data, function (data, key) {
                html += '<div class="cpl-sm-12 col-md-12 col-lg-12 m-2">' +
                    '<div class="blog-item bg-light rounded overflow-hidden">'+
                    '<div class="p-4">'+
                    '<div class="d-flex mb-3">'+
                    '<small class="me-3">'+
                    '<i class="far fa-user text-primary me-2"></i>'+
                    (data.user.username)+
                    '</small>'+
                    '<small>'+
                    '<i class="far fa-calendar-alt text-primary me-2"></i>'+
                    (moment(data.modified).format('Y-MM-DD'))+
                    '</small>'+
                    '</div>'+
                    '<h5 class="mb-3">'+(data.title)+'</h5>'+
                    '<a class="text-uppercase view btn btn-primary" href="javascript:void(0);" data-url="/recordsunitoffice/memos/'+(data.document)+'" title="'+((data.title).toUpperCase())+'" style="color: #091e3e;">View <i class="bi bi-arrow-up"></i></a>' +
                    ' | '+
                    '<a class="text-uppercase download btn btn-primary" href="javascript:void(0);" data-id="'+(parseInt(data.id))+'" title="'+((data.title).toUpperCase())+'" style="background: #ec3125 !important; border: #ec3125 !important; color: white !important;"> Download <i class="fa fa-download"></i> </a>'+
                    '</div>'+
                    '</div>'+
                    '</div>'
            });
            $('#document-list').html(html);
        }).fail(function (data, status, xhr) {

        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        });
    }

});