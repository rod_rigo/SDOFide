<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact $contact
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<?=$this->Html->css('admin/contacts/add')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Contact Form</h3>
            </div>
            <?= $this->Form->create($contact,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('header', ucwords('header'))?>
                        <?=$this->Form->text('header',[
                            'class' => 'form-control',
                            'id' => 'header',
                            'required' => true,
                            'placeholder' => ucwords('header'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('title', ucwords('Title'))?>
                        <?=$this->Form->text('title',[
                            'class' => 'form-control',
                            'id' => 'title',
                            'required' => true,
                            'placeholder' => ucwords('Title'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('contact_mobile', ucwords('Contact/Tel'))?>
                        <?=$this->Form->text('contact_mobile',[
                            'class' => 'form-control',
                            'id' => 'contact-mobile',
                            'required' => true,
                            'placeholder' => ucwords('Contact/Tel'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('email', ucwords('email'))?>
                        <?=$this->Form->email('email',[
                            'class' => 'form-control',
                            'id' => 'email',
                            'required' => true,
                            'placeholder' => ucwords('email'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div id="map"></div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('address', ucwords('address'))?>
                        <?=$this->Form->textarea('address',[
                            'class' => 'form-control',
                            'id' => 'address',
                            'required' => true,
                            'placeholder' => ucwords('address'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('latitude', ucwords('latitude'))?>
                        <?=$this->Form->number('latitude',[
                            'class' => 'form-control',
                            'id' => 'latitude',
                            'required' => true,
                            'min' => 0,
                            'placeholder' => ucwords('latitude'),
                            'pattern' => '([0-9\.]){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('longtitude', ucwords('longtitude'))?>
                        <?=$this->Form->number('longtitude',[
                            'class' => 'form-control',
                            'id' => 'longtitude',
                            'required' => true,
                            'min' => 0,
                            'placeholder' => ucwords('longtitude'),
                            'pattern' => '([0-9\.]){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('description', ucwords('description'))?>
                        <?=$this->Form->textarea('description',[
                            'class' => 'form-control',
                            'id' => 'description',
                            'required' => true,
                            'placeholder' => ucwords('description'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('content', ucwords('content'))?>
                        <?=$this->Form->textarea('content',[
                            'class' => 'form-control',
                            'id' => 'content',
                            'required' => false,
                            'value' => '-'
                        ])?>
                        <small id="content-text"></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?=$this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ])?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Contacts', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/contacts/add')?>

