<?php
/**
 * @var \App\View\AppView $this
 */
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
        <img src="<?=$this->Url->assetUrl('/img/do-logo.png')?>" class="img-circle w-75" loading="lazy" title="Deped Logo" alt="DEPED">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-header">Navigation</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('dashboards') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('users') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Establishments', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('establishments') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Establishments
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Departments', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('departments') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Departments
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Progressions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('progressions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>
                            Progressions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Statuses', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('statuses') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ul"></i>
                        <p>
                            Statuses
                        </p>
                    </a>
                </li>

                <li class="nav-header">Repository</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Memos', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('memos') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-sticky-note"></i>
                        <p>
                            Memos
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('documents') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('documents') && !in_array(strtolower($action),[strtolower('bin')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-archive"></i>
                        <p>
                            Documents
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('transactions') && !in_array(strtolower($action),[strtolower('bin'), strtolower('released'), strtolower('received')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && !in_array(strtolower($action),[strtolower('bin'), strtolower('released'), strtolower('received')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Transactions
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('transactions') && in_array(strtolower($action),[strtolower('released'), strtolower('received')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && in_array(strtolower($action),[strtolower('released'), strtolower('received')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>
                            Reports
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'released'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('released'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Released</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'received'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('received'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Received</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">Research</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Categories', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('categories') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>
                            Categories
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('positions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Positions
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('projects') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('projects') && !in_array(strtolower($action),[strtolower('bin')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-file-archive"></i>
                        <p>
                            Projects
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'completed'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('completed') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Completed</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'ongoing'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('ongoing') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ongoing</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'expired'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('expired') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Expired</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item <?=(strtolower($controller) == strtolower('requests') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && !in_array(strtolower($action),[strtolower('bin'), strtolower('released'), strtolower('received')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Requests
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">PFT</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Genders', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('genders') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-genderless"></i>
                        <p>
                            Genders
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Levels', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('levels') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-alt"></i>
                        <p>
                            Levels
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Grades', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('grades') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Grades
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Students', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('students') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Students
                        </p>
                    </a>
                </li>

                <li class="nav-header">DCP</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Heads', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('heads') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Heads
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'MonitoringTypes', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('monitoringtypes') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-tasks"></i>
                        <p>
                            Monitoring Types
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Conditions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('conditions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-window-restore"></i>
                        <p>
                            Conditions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Indicators', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('indicators') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-window-maximize"></i>
                        <p>
                            Indicators
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Evaluations', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('evaluations') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-file-alt"></i>
                        <p>
                            Evaluations
                        </p>
                    </a>
                </li>

                <li class="nav-header">Website</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Abouts', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('abouts') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-question"></i>
                        <p>
                            Abouts
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Contacts', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('contacts') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-map-marked"></i>
                        <p>
                            Contacts
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teams', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('teams') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Teams
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Banners', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('banners') && strtolower('index') == strtolower($action))? 'active': null;?>">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Banners</p>
                    </a>
                </li>

                <li class="nav-header">Archive</li>
                <li class="nav-item <?=(strtolower($action) == strtolower('bin'))? 'menu-is-opening menu-open': null;?>">
                    <a href="#" class="nav-link <?=(strtolower($action) == strtolower('bin'))? 'active': null;?>">
                        <i class="nav-icon fas fa-trash"></i>
                        <p>
                            Bin
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Establishments', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('establishments') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Establishments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Departments', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('departments') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Departments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Progressions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('progressions') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Progressions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Statuses', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('statuses') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Statuses</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('transactions') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Transactions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Memos', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('memos') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Memos</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('documents') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Documents</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Abouts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('abouts') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Abouts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Contacts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('contacts') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Contacts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Teams', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('teams') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Teams</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Sign Out
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
