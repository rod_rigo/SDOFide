<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<script>
    var id = parseInt(<?=intval($user->id)?>);
</script>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">User Form</h3>
            </div>
            <?= $this->Form->create($user,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="form-group row">
                    <?=$this->Form->label('name', ucwords('Name'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('name',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('Name'),
                            'required' => true,
                            'id' => 'name',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('email', ucwords('email'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->email('email',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('email'),
                            'required' => true,
                            'id' => 'email',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('username', ucwords('Username'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('username',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('Username'),
                            'required' => true,
                            'id' => 'username',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('school', ucwords('school'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('school',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('school'),
                            'required' => true,
                            'id' => 'school',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('position', ucwords('position'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->text('position',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('position'),
                            'required' => true,
                            'id' => 'position',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>
            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('token',[
                    'id' => 'token',
                    'value' => uniqid()
                ]);?>
                <?= $this->Form->hidden('is_admin',[
                    'id' => 'is-admin',
                    'value' => intval(0)
                ]);?>
                <?= $this->Form->hidden('is_client',[
                    'id' => 'is-client',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval($user->is_active)
                ]);?>
                <?= $this->Form->hidden('token',[
                    'id' => 'token',
                    'value' => uniqid()
                ]);?>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">User Form</h3>
            </div>
            <?= $this->Form->create($user,['type' => 'file', 'id' => 'password-form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">

                <div class="form-group row">
                    <?=$this->Form->label('current_password', ucwords('Password (Current)'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->password('current_password',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('Password (Current)'),
                            'required' => true,
                            'id' => 'current-password',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('password', ucwords('Password (New)'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->password('password',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('Password (New)'),
                            'required' => true,
                            'id' => 'password',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field'),
                            'value' => ''
                        ]);?>
                        <small></small>
                    </div>
                </div>

                <div class="form-group row">
                    <?=$this->Form->label('confirm_password', ucwords('Password (Confirm)'),[
                        'class' => 'col-sm-2 col-form-label'
                    ])?>
                    <div class="col-sm-10">
                        <?= $this->Form->password('confirm_password',[
                            'class' => 'form-control form-control-border border-width-2',
                            'placeholder' => ucwords('Password (Confirm)'),
                            'required' => true,
                            'id' => 'confirm-password',
                            'pattern' => '(.){1,}',
                            'title' => ucwords('please fill out this field')
                        ]);?>
                        <small></small>
                    </div>
                </div>

            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('token',[
                    'id' => 'token',
                    'value' => uniqid()
                ]);?>
                <?= $this->Form->hidden('is_admin',[
                    'id' => 'is-admin',
                    'value' => intval(0)
                ]);?>
                <?= $this->Form->hidden('is_client',[
                    'id' => 'is-client',
                    'value' => intval(1)
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval($user->is_active)
                ]);?>
                <?= $this->Form->hidden('token',[
                    'id' => 'token',
                    'value' => uniqid()
                ]);?>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('client/users/edit')?>
