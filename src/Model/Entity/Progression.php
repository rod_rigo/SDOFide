<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Progression Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $progression
 * @property int $is_active
 * @property int $is_default
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Status[] $statuses
 * @property \App\Model\Entity\Tracking[] $trackings
 * @property \App\Model\Entity\Evaluation[] $evaluations
 */
class Progression extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'progression' => true,
        'is_active' => true,
        'is_default' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'statuses' => true,
        'trackings' => true,
        'evaluations' => true,
    ];

    protected function _setProgression($value){
        return ucwords($value);
    }
}
