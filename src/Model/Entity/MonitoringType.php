<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MonitoringType Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $monitoring_type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\MonitoringDate[] $monitoring_dates
 * @property \App\Model\Entity\Evaluation[] $evaluation
 */
class MonitoringType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'monitoring_type' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'monitoring_dates' => true,
        'evaluations' => true,
    ];

    protected function _setMonitoringType($value){
        return ucwords($value);
    }

}
