<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Transaction $transaction
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $establishments
 * @var \Cake\Collection\CollectionInterface|string[] $departments
 */
?>

<?=$this->Html->css('records/transactions/add')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Transaction Form</h3>
            </div>
            <?= $this->Form->create($transaction,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('docsid', ucwords('docsid'))?>
                        <?=$this->Form->text('docsid',[
                            'class' => 'form-control',
                            'id' => 'docsid',
                            'required' => true,
                            'placeholder' => ucwords('docsid'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-8">
                        <?=$this->Form->label('transaction_name', ucwords('transaction name'))?>
                        <?=$this->Form->text('transaction_name',[
                            'class' => 'form-control',
                            'id' => 'transaction-name',
                            'required' => true,
                            'placeholder' => ucwords('transaction name'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('establishment_id', ucwords('establishment'))?>
                        <?=$this->Form->select('establishment_id', $establishments,[
                            'class' => 'form-control',
                            'id' => 'establishment-id',
                            'required' => true,
                            'empty' => ucwords('select establishment'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('department_id', ucwords('department'))?>
                        <?=$this->Form->select('department_id', [],[
                            'class' => 'form-control',
                            'id' => 'department-id',
                            'required' => true,
                            'empty' => ucwords('Choose establishment'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div class="row no-gutters">
                            <div class="col-sm-12 col-md-3 col-lg-2 d-flex justify-content-start align-items-end ml-0">
                                <div class="icheck-primary d-inline">
                                    <?=$this->Form->checkbox('others',[
                                        'id' => 'others',
                                        'label' => false,
                                        'hiddenField' => false,
                                    ])?>
                                    <?=$this->Form->label('others', ucwords('Others (Department)'))?>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-9 col-lg-10 mr-0">
                                <?=$this->Form->text('other',[
                                    'class' => 'form-control form-control-border',
                                    'id' => 'other',
                                    'pattern' => '(.){1,}',
                                    'required' => false,
                                    'readonly' => true,
                                    'placeholder' => ucwords('Department'),
                                    'title' => ucwords('please fill out this field')
                                ])?>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                        <?=$this->Form->label('date_release', ucwords('date release'))?>
                        <?=$this->Form->date('date_release',[
                            'class' => 'form-control',
                            'id' => 'date-release',
                            'required' => false,
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('received_by', ucwords('received by'))?>
                        <?=$this->Form->text('received_by',[
                            'class' => 'form-control',
                            'id' => 'received-by',
                            'required' => true,
                            'placeholder' => ucwords('docsid'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'value' => '-'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                        <?=$this->Form->label('date_received', ucwords('date received'))?>
                        <?=$this->Form->date('date_received',[
                            'class' => 'form-control',
                            'id' => 'date-received',
                            'required' => false,
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('remarks', ucwords('remarks'))?>
                        <?=$this->Form->textarea('remarks',[
                            'class' => 'form-control',
                            'id' => 'remarks',
                            'required' => false,
                            'value' => '-'
                        ])?>
                        <small id="remarks-text"></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('file', ucwords('file'))?>
                        <?=$this->Form->file('file',[
                            'class' => 'form-control',
                            'id' => 'file',
                            'required' => false,
                            'accept' => '.pdf, .docx'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-0 d-flex justify-content-center align-items-center mt-1">
                        <embed src="" id="embed" width="0" height="0">
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <a link href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?=$this->Form->hidden('trackings.0.user_id',[
                    'id' => 'trackings-0-user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?=$this->Form->hidden('trackings.0.progression_id',[
                    'id' => 'trackings-0-progression-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($progression->id)
                ])?>
                <?=$this->Form->hidden('trackings.0.status_id',[
                    'id' => 'trackings-0-status-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($status->id)
                ])?>
                <?=$this->Form->hidden('trackings.0.remarks',[
                    'id' => 'trackings-0-remarks',
                    'required' => true,
                    'readonly' => true,
                    'value' => ucwords('Uploaded')
                ])?>
                <?=$this->Form->hidden('has_file',[
                    'id' => 'has-file',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ])?>
                <?=$this->Form->hidden('document',[
                    'id' => 'document',
                    'required' => true,
                    'readonly' => true,
                    'value' => uniqid()
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('records/transactions/add')?>
