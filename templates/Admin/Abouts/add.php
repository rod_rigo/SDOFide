<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\About $about
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<?=$this->Html->css('admin/abouts/add')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">About Form</h3>
            </div>
            <?= $this->Form->create($about,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-8 col-lg-7">
                      <div class="row">
                          <div class="col-sm-12 col-md-12 col-lg-12">
                              <?=$this->Form->label('header', ucwords('header'))?>
                              <?=$this->Form->text('header',[
                                  'class' => 'form-control',
                                  'id' => 'header',
                                  'required' => true,
                                  'placeholder' => ucwords('header'),
                                  'pattern' => '(.){1,}',
                                  'title' => ucwords('Please Fill Out This Field')
                              ])?>
                              <small></small>
                          </div>

                          <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                              <?=$this->Form->label('title', ucwords('title'))?>
                              <?=$this->Form->text('title',[
                                  'class' => 'form-control',
                                  'id' => 'title',
                                  'required' => true,
                                  'placeholder' => ucwords('title'),
                                  'pattern' => '(.){1,}',
                                  'title' => ucwords('Please Fill Out This Field')
                              ])?>
                              <small></small>
                          </div>

                          <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                              <?=$this->Form->label('description', ucwords('description'))?>
                              <?=$this->Form->textarea('description',[
                                  'class' => 'form-control',
                                  'id' => 'description',
                                  'required' => true,
                                  'placeholder' => ucwords('description')
                              ])?>
                              <small></small>
                          </div>

                          <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                              <?=$this->Form->label('file', ucwords('file (Thumbnail)'))?>
                              <?=$this->Form->file('file',[
                                  'class' => 'form-control',
                                  'id' => 'file',
                                  'required' => true,
                                  'accept' => 'image/*',
                                  'title' => ucwords('Please Fill Out This Field')
                              ])?>
                              <small></small>
                          </div>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-5">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 text-center p-2">
                                <img src="<?=$this->Url->assetUrl('/img/records.png')?>" class="object-fit-contain" alt="Image Thumbnail" id="image-preview" height="350" width="350" loading="lazy">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('content', ucwords('content'))?>
                        <?=$this->Form->textarea('content',[
                            'class' => 'form-control',
                            'id' => 'content',
                            'required' => false,
                            'value' => '-'
                        ])?>
                        <small id="content-text"></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?=$this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ])?>
                <?=$this->Form->hidden('thumbnail',[
                    'id' => 'thumbnail',
                    'required' => true,
                    'readonly' => true,
                    'value' => uniqid()
                ])?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Abouts', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/abouts/add')?>
