<?php
/**
 * @var \App\View\AppView $this
 */
?>

<script>
    var id = parseInt(<?=intval($id)?>);
</script>

<div class="modal fade" id="modal" data-bs-backdrop="static">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <embed src="javascript:void(0);" id="embed" style="width: 100%;" height="800" title="">
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">

            </div>
        </div>
    </div>
</div>

<div class="p-5 mt-3">

</div>

<div class="container-fluid py-5 wow fadeInUp">
    <div class="container py-5">
        <div class="row g-5">

            <!-- Sidebar Start -->
            <div class=" col-sm-12 col-md-5 col-lg-4 mt-2">
                <!-- Search Form Start -->
                <?=$this->Form->create(null,['type' => 'file', 'id' => 'form', 'class' => 'mb-5', 'method' => 'get'])?>
                    <div class="input-group">
                        <?=$this->Form->search('search',[
                            'class' => 'form-control p-3',
                            'id' => 'search',
                            'placeholder' => ucwords('Search Here...'),
                            'required' => true,
                            'value' => $search
                        ])?>
                        <?=$this->Form->button('<i class="bi bi-search"></i>',[
                            'class' => 'btn btn-primary px-4',
                            'escapeTitle' => false,
                            'type' => 'submit'
                        ])?>
                    </div>
                <?=$this->Form->end()?>
                <!-- Search Form End -->
            </div>
            <!-- Sidebar End -->

            <!-- Blog list Start -->
            <div class=" col-sm-12 col-md-7 col-lg-8 mt-2">
                <div class="row g-5" id="document-list">

                    <?php if(!$documents->isEmpty()):?>
                        <?php foreach ($documents as $document):?>
                            <div class="cpl-sm-12 col-md-12 col-lg-12 m-2">
                                <div class="blog-item bg-light rounded overflow-hidden">
                                    <div class="p-4">
                                        <div class="d-flex mb-3">
                                            <small class="me-3">
                                                <i class="far fa-user text-primary me-2"></i>
                                                <?=$document->user->username?>
                                            </small>
                                            <small>
                                                <i class="far fa-calendar-alt text-primary me-2"></i>
                                                <?=date('Y-m-d', strtotime($document->modified))?>
                                            </small>
                                        </div>
                                        <h5 class="mb-3"> <?=strtoupper($document->title)?></h5>
                                        <a class="text-uppercase view btn btn-primary" href="javascript:void(0);" data-url="<?=$this->Url->assetUrl('/memos/'.($document->document))?>" title="<?=strtoupper($document->title)?>" style="color: #091e3e;">
                                            View <i class="bi bi-arrow-up"></i>
                                        </a>
                                        |
                                        <a class="text-uppercase download btn btn-primary" href="javascript:void(0);" data-id="<?=intval($document->id)?>" title="<?=strtoupper($document->title)?>" style="background: #ec3125 !important; border: #ec3125 !important; color: white !important;">
                                            Download <i class="fa fa-download"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>

                </div>
            </div>
            <!-- Blog list End -->
        </div>
    </div>
</div>
<?=$this->Html->script('memos/index')?>

