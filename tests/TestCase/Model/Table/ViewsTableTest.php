<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewsTable Test Case
 */
class ViewsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewsTable
     */
    protected $Views;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Views',
        'app.Users',
        'app.Projects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Views') ? [] : ['className' => ViewsTable::class];
        $this->Views = $this->getTableLocator()->get('Views', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Views);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ViewsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ViewsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
