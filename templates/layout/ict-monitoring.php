<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'SDOFIDE';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?> |
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('csrf-token', $this->request->getAttribute('csrfToken')) ?>
    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <?=$this->Html->css([
        '/plugins/fontawesome-free/css/all.min',
        '/select2/css/select2',
        '/dist/css/adminlte.min',
        '/jquery/css/jquery-ui',
        '/i-check/css/icheck-bootstrap',
        '/datatables/css/dataTables.bootstrap4.min',
        '/datatables/css/jquery.dataTables.min',
        '/datatables/css/responsive.bootstrap4.min',
        '/datatables/css/buttons/buttons.bootstrap4.min',
        '/datatables/css/buttons/buttons.dataTables.min',
        '/leaflet/css/leaflet',
        '/leaflet/css/leaflet-search',
        'custom',
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/ck-editor/js/ckeditor',
        '/sweet-alert/js/sweetalert2',
        '/sweet-alert/js/sweetalert2.all',
        '/moment/js/moment',
        '/chartjs/js/chart',
        '/chartjs/js/chartjs-plugin-autocolors',
        '/turbo-links/js/turbolinks',
        '/datatables/js/dataTables.bootstrap4.min',
        '/datatables/js/jquery.dataTables.min',
        '/datatables/js/dataTables.responsive.min',
        '/datatables/js/responsive.bootstrap4.min',
        '/datatables/js/buttons/buttons.bootstrap4.min',
        '/datatables/js/buttons/buttons.print.min',
        '/datatables/js/buttons/html2pdf.bundle.min',
        '/datatables/js/buttons/jszip.min',
        '/datatables/js/buttons/pdfmake.min',
        '/datatables/js/buttons/vfs_fonts',
        '/datatables/js/buttons/dataTables.buttons.min',
        '/datatables/js/buttons/buttons.html5.min',
        '/select2/js/select2',
        '/leaflet/js/leaflet',
        '/leaflet/js/leaflet-search'
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var mainurl = window.location.origin+'/SDOFide/ict-monitoring/';
    </script>

</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

    <!-- Navbar -->
    <?=$this->element('ict-monitoring/navbar')?>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?=$this->element('ict-monitoring/header')?>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <?=$this->fetch('content')?>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <?=$this->element('ict-monitoring/footer')?>
</div>

<?=$this->Html->script([
    '/plugins/bootstrap/js/bootstrap.bundle.min',
    '/dist/js/adminlte.min'
])?>
<script>
    Turbolinks.start();
    $('a[turbolink]').click(function (e) {
        var href = $(this).attr('href');
        Turbolinks.visit(href,{action:'replace'});
    });
    $('input[type="number"]').keypress(function (e) {
        var key = e.key;
        var regex = /^([0-9]){1,}$/;
        if(!key.match(regex)){
            e.preventDefault();
        }
    });
</script>
</body>
</html>
