'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'dashboards/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    var transaction_canvas_month = document.querySelector('#transaction-chart-month').getContext('2d');
    var transaction_chart_month;

    function getMonth() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getTransactionsMonth?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {
                $('#year').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.month).toUpperCase());
            });
            $('#year').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getMonthChart() {
        transaction_chart_month =  new Chart(transaction_canvas_month, {
            type: 'bar',
            data: {
                labels: getMonth().label,
                datasets: [
                    {
                        label:'Total Transactions',
                        data: getMonth().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: $('#year').val(),
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var transaction_canvas_week = document.querySelector('#transaction-chart-week').getContext('2d');
    var transaction_chart_week;

    function getWeek() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getTransactionsWeek',
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.day).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getWeekChart() {
        transaction_chart_week =  new Chart(transaction_canvas_week, {
            type: 'bar',
            data: {
                labels: getWeek().label,
                datasets: [
                    {
                        label:'Total Transactions',
                        data: getWeek().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'This Week',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var department_canvas = document.querySelector('#department-chart').getContext('2d');
    var department_chart;

    function getDepartment() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getTransactionsDepartments?year='+($('#year').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {
                $('#year').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.department).toUpperCase());
            });
            $('#year').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getDepartmentChart() {
        department_chart =  new Chart(department_canvas, {
            type: 'bar',
            data: {
                labels: getDepartment().label,
                datasets: [
                    {
                        label:'Total Reports',
                        data: getDepartment().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: $('#year').val(),
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var establishment_canvas = document.querySelector('#establishment-chart').getContext('2d');
    var establishment_chart;

    function getEstablishment() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getEstablishments',
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.establishment).toUpperCase());
            });
            $('#year').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getEstablishmentChart() {
        establishment_chart =  new Chart(establishment_canvas, {
            type: 'doughnut',
            data: {
                labels: getEstablishment().label,
                datasets: [
                    {
                        label:'Total Establishments',
                        data: getEstablishment().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Establishments',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }


    var progression_canvas = document.querySelector('#progression-chart').getContext('2d');
    var progression_chart;

    function getProgressions() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getProgressions?start_date='+($('#progression-start-date').val())+'&end_date='+($('#progression-end-date').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.progression).toUpperCase());
            });
            $('#year').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getProgressionsChart() {
        progression_chart =  new Chart(progression_canvas, {
            type: 'doughnut',
            data: {
                labels: getProgressions().label,
                datasets: [
                    {
                        label:'Total Progressions',
                        data: getProgressions().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Progressions',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var status_canvas = document.querySelector('#status-chart').getContext('2d');
    var status_chart;

    function getStatuses() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getStatuses?start_date='+($('#status-start-date').val())+'&end_date='+($('#status-end-date').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.status).toUpperCase());
            });
            $('#year').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getStatusesChart() {
        status_chart =  new Chart(status_canvas, {
            type: 'polarArea',
            data: {
                labels: getStatuses().label,
                datasets: [
                    {
                        label:'Total Status',
                        data: getStatuses().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Statuses',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    function transactions() {
        $.ajax({
            url:baseurl+'transactions',
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                $('#total-transactions-today, #total-transactions-week, #total-transactions-month, #total-transactions-year').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-transactions-today').text(parseFloat(data.day));
            $('#total-transactions-week').text(parseFloat(data.week));
            $('#total-transactions-month').text(parseFloat(data.month));
            $('#total-transactions-year').text(parseFloat(data.year));
        }).fail(function (data, status, xhr) {

        });
    }

    function documents() {
        $.ajax({
            url:baseurl+'documents',
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                $('#total-documents-today, #total-documents-week, #total-documents-month, #total-documents-year').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-documents-today').text(parseFloat(data.day));
            $('#total-documents-week').text(parseFloat(data.week));
            $('#total-documents-month').text(parseFloat(data.month));
            $('#total-documents-year').text(parseFloat(data.year));
        }).fail(function (data, status, xhr) {

        });
    }

    $('#year').change(function (e) {
        var value = $(this).val();
        if(value){
            transaction_chart_month.destroy();
            getMonth();
            getMonthChart();

            setTimeout(function () {
                department_chart.destroy();
                getDepartment();
                getDepartmentChart();
            }, 1000);
        }
    });

    $('#toggle-progression').click(function (e) {
        $('#progression-modal').modal('toggle');
        getProgressionsChart();
    });

    $('#toggle-status').click(function (e) {
        $('#status-modal').modal('toggle');
        getStatusesChart();
    });

    $('#progression-form').submit(function (e) {
        e.preventDefault();
        progression_chart.destroy();
        getProgressions();
        getProgressionsChart();
    });

    $('#status-form').submit(function (e) {
        e.preventDefault();
        status_chart.destroy();
        getStatuses();
        getStatusesChart();
    });

    documents();
    transactions();
    getMonthChart();
    getWeekChart();
    getDepartmentChart();
    getEstablishmentChart();

});