<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MonitoringDate Entity
 *
 * @property int $id
 * @property int $evaluation_id
 * @property int $monitoring_type_id
 * @property \Cake\I18n\FrozenDate|null $date
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Evaluation $evaluation
 * @property \App\Model\Entity\MonitoringType $monitoring_type
 */
class MonitoringDate extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'evaluation_id' => true,
        'monitoring_type_id' => true,
        'date' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'evaluation' => true,
        'monitoring_type' => true,
    ];
}
