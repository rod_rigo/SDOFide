<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Document[]|\Cake\Collection\CollectionInterface $documents
 */
?>

<?=$this->Html->css('records/documents/index')?>

<?=$this->Form->create(null,['class' => 'row', 'id' => 'form', 'type' => 'file'])?>
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Documents', 'action' => 'add'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="New Document">
            New Document
        </a>
    </div>

    <div class="col-sm-12 col-md-5 col-lg-3 mb-3">
        <?=$this->Form->label('memo', ucwords('memo'))?>
        <?=$this->Form->select('memo', $memos,[
            'class' => 'form-control rounded-0',
            'id' => 'memo',
            'required' => true,
            'empty' => ucwords('select memo'),
            'title' => ucwords('Please Fill Out This Field')
        ])?>
        <small></small>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-2 mb-3">
        <?=$this->Form->label('start_date', ucwords('Start Date'))?>
        <?=$this->Form->date('start_date',[
            'id' => 'start-date',
            'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
            'class' => 'form-control form-control-border',
            'title' => ucwords('Start Date'),
            'required' => true,
        ])?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2 mb-3">
        <?=$this->Form->label('end_date', ucwords('End Date'))?>
        <?=$this->Form->date('end_date',[
            'id' => 'end-date',
            'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d'),
            'class' => 'form-control form-control-border',
            'title' => ucwords('End Date'),
            'required' => true
        ])?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2 mb-3">
        <?=$this->Form->label('records', ucwords('Records'))?>
        <?=$this->Form->number('records',[
            'id' => 'records',
            'value' => 10000,
            'class' => 'form-control form-control-border',
            'title' => ucwords('Records'),
            'min' => 10000,
            'max' => 50000,
            'required' => true
        ])?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2 mb-3 d-flex justify-content-start align-items-end">
        <?=$this->Form->button('Submit',[
            'class' => 'btn btn-primary rounded-0',
            'type' => 'submit'
        ])?>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Memo</th>
                        <th>Title</th>
                        <th>Is Published</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
<?=$this->Form->end()?>

<?=$this->Html->script('records/documents/index')?>
