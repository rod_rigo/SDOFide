<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Document Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $memo_id
 * @property string $title
 * @property string $document
 * @property int $is_published
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Memo $memo
 */
class Document extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'memo_id' => true,
        'title' => true,
        'document' => true,
        'is_published' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'memo' => true,
    ];

    protected function _setTitle($value){
        return strtoupper($value);
    }

}
