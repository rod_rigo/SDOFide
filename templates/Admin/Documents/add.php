<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Document $document
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $memos
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Document Form</h3>
            </div>
            <?= $this->Form->create($document,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('title', ucwords('title'))?>
                        <?=$this->Form->text('title',[
                            'class' => 'form-control',
                            'id' => 'title',
                            'required' => true,
                            'placeholder' => ucwords('title'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('memo_id', ucwords('memo'))?>
                        <?=$this->Form->select('memo_id', $memos,[
                            'class' => 'form-control',
                            'id' => 'memo-id',
                            'required' => true,
                            'empty' => ucwords('select memo'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('file', ucwords('file'))?>
                        <?=$this->Form->file('file',[
                            'class' => 'form-control',
                            'id' => 'file',
                            'required' => true,
                            'accept' => '.pdf, .docx'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-0 d-flex justify-content-center align-items-center mt-3">
                        <embed src="" id="embed" width="0" height="0">
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('published',[
                                'id' => 'published',
                                'label' => false,
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('published', ucwords('published'))?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Documents', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?=$this->Form->hidden('is_published',[
                    'id' => 'is-published',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(1)
                ])?>
                <?=$this->Form->hidden('document',[
                    'id' => 'document',
                    'required' => true,
                    'readonly' => true,
                    'value' => uniqid()
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/documents/add')?>
