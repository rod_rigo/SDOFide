<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transaction Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $docsid
 * @property int $establishment_id
 * @property int $department_id
 * @property string $transaction_name
 * @property \Cake\I18n\FrozenDate|null $date_release
 * @property string $received_by
 * @property \Cake\I18n\FrozenDate|null $date_received
 * @property string $remarks
 * @property string $document
 * @property int $has_file
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Establishment $establishment
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Tracking[] $trackings
 */
class Transaction extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'docsid' => true,
        'establishment_id' => true,
        'department_id' => true,
        'transaction_name' => true,
        'date_release' => true,
        'received_by' => true,
        'date_received' => true,
        'remarks' => true,
        'document' => true,
        'has_file' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'establishment' => true,
        'department' => true,
        'trackings' => true,
    ];

    protected function _setDocsid($value){
        return strtoupper($value);
    }

    protected function _setTransactionName($value){
        return strtoupper($value);
    }

}
