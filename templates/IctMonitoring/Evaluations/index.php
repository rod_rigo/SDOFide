<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Evaluation[]|\Cake\Collection\CollectionInterface $evaluations
 */

use Cake\Filesystem\File;

$file = new File(WWW_ROOT . 'img' . DS . $banner->header);
$file = new \Cake\Filesystem\File(WWW_ROOT. 'img'. DS. $banner->header);
if($file->exists()){
    $path = WWW_ROOT. 'img'. DS. $banner->header;
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $header = 'data:'.($mime).';base64,'.($image);
}else{
    $path = WWW_ROOT.'img'. DS.'header.jpg';
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $header = 'data:'.($mime).';base64,'.($image);
}
$headerPath = $path;

$file = new File(WWW_ROOT . 'img' . DS . $banner->footer);
if ($file->exists()) {
    $path = WWW_ROOT. 'img'. DS. $banner->footer;
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $footer = 'data:'.($mime).';base64,'.($image);
}else{
    $path = WWW_ROOT.'img'. DS.'footer.png';
    $image = base64_encode(file_get_contents($path));
    $mime = mime_content_type($path);
    $footer = 'data:'.($mime).';base64,'.($image);
}
$footerPath = $path;

$officer = strtoupper($banner->officer);
$position = strtoupper($banner->position);

?>

<style>
    .dataTables_length{
        width: 25%;
        float: left;
    }
    .dt-buttons{
        position: relative;
        width: 50%;
    }
    .dataTables_filter{
        width: 25%;
        float: right;
    }
    @media (max-width: 700px) {
        .dataTables_length{
            width: 100%;
        }
        .dt-buttons{
            width: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
            margin: 1em;
        }
        .dataTables_filter{
            width: 100%;
        }
    }
</style>

<?=$this->Html->script([
    '/pdf-make/js/pdfmake',
    '/pdf-make/js/vfs_fonts',
])?>

<script>
    var header = '<?=$header?>';
    var footer = '<?=$footer?>';
    var officer = '<?=$officer?>';
    var position = '<?=$position?>';
</script>

<?=$this->Form->create(null,['class' => 'row', 'id' => 'form', 'type' => 'file'])?>
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'IctMonitoring', 'controller' => 'Evaluations', 'action' => 'add'])?>" turbolink id="toggle-modal" class="btn btn-primary rounded-0" title="New Evaluation">
            New Evaluation
        </a>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2 mb-3">
        <?=$this->Form->label('start_date', ucwords('Start Date'))?>
        <?=$this->Form->date('start_date',[
            'id' => 'start-date',
            'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d'),
            'class' => 'form-control form-control-border',
            'title' => ucwords('Start Date'),
            'required' => true,
        ])?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2 mb-3">
        <?=$this->Form->label('end_date', ucwords('End Date'))?>
        <?=$this->Form->date('end_date',[
            'id' => 'end-date',
            'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('year')->format('Y-m-d'),
            'class' => 'form-control form-control-border',
            'title' => ucwords('End Date'),
            'required' => true
        ])?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2 mb-3">
        <?=$this->Form->label('records', ucwords('Records'))?>
        <?=$this->Form->number('records',[
            'id' => 'records',
            'value' => 10000,
            'class' => 'form-control form-control-border',
            'title' => ucwords('Records'),
            'min' => 10000,
            'max' => 50000,
            'required' => true
        ])?>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-2 mb-3 d-flex justify-content-start align-items-end">
        <?=$this->Form->button('Submit',[
            'class' => 'btn btn-primary rounded-0',
            'type' => 'submit'
        ])?>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Department</th>
                        <th>Head</th>
                        <th>Monitoring Type</th>
                        <th>Progression</th>
                        <th>Status</th>
                        <th>Analysis</th>
                        <th>Recommendation</th>
                        <th>Date</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
<?=$this->Form->end()?>

<?php if(boolval($auth['profile_is_admin'])):?>
    <div class="row">
        <div class="col-sm-12 col-md-4 col-lg-6">
            <?=$this->Form->year('year',[
                'id' => 'year',
                'class' => 'form-control form-control-border',
                'min' => 2000,
                'value' => date('Y')
            ])?>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3 h-100">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <canvas height="400" width="800" id="month-chart" style="height: 600px !important;"></canvas>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <canvas height="400" width="800" id="monitoring-type-chart" style="height: 600px !important;"></canvas>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-12 mt-3">
                            <canvas height="400" width="800" id="department-chart" style="height: 600px !important;"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>

<?=$this->Html->script('ict-monitoring/evaluations/index')?>




