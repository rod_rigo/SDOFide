<?php
/**
 * @var \App\View\AppView $this
 */
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
        <img src="<?=$this->Url->assetUrl('/img/do-logo.png')?>" class="img-circle w-75" loading="lazy" title="Deped Logo" alt="DEPED">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-header">Navigation</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Dashboards', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('dashboards') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Establishments', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('establishments') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Establishments
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Departments', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('departments') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Departments
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Progressions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('progressions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>
                            Progressions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Statuses', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('statuses') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ul"></i>
                        <p>
                            Statuses
                        </p>
                    </a>
                </li>

                <li class="nav-header">Repository</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Memos', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('memos') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-sticky-note"></i>
                        <p>
                            Memos
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('documents') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('documents') && !in_array(strtolower($action),[strtolower('bin')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-archive"></i>
                        <p>
                            Documents
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Documents', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Documents', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Documents', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Documents', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Documents', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('documents') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('transactions') && !in_array(strtolower($action),[strtolower('bin'), strtolower('released'), strtolower('received')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && !in_array(strtolower($action),[strtolower('bin'), strtolower('released'), strtolower('received')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Transactions
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('transactions') && in_array(strtolower($action),[strtolower('released'), strtolower('received')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && in_array(strtolower($action),[strtolower('released'), strtolower('received')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>
                            Reports
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'released'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('released'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Released</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'received'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('received'))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Received</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">Archive</li>
                <li class="nav-item <?=(strtolower($action) == strtolower('bin'))? 'menu-is-opening menu-open': null;?>">
                    <a href="#" class="nav-link <?=(strtolower($action) == strtolower('bin'))? 'active': null;?>">
                        <i class="nav-icon fas fa-trash"></i>
                        <p>
                            Bin
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Establishments', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('establishments') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Establishments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Departments', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('departments') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Departments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Progressions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('progressions') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Progressions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Statuses', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('statuses') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Statuses</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('transactions') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Transactions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Memos', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('memos') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Memos</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Documents', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('documents') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Documents</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Abouts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('abouts') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Abouts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Contacts', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('contacts') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Contacts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Teams', 'action' => 'bin'])?>" class="nav-link <?=(strtolower('teams') == strtolower($controller) && strtolower('bin') == strtolower($action))? 'active': null?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Teams</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Sign Out
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
