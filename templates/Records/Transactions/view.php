<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Transaction $transaction
 */

$path = WWW_ROOT. 'files'. DS. strval($transaction->document);
$folder = new \Cake\Filesystem\File($path);

?>

<script>
    var id = parseInt(<?=intval($transaction->id)?>);
</script>

<?=$this->Html->css('records/transactions/view')?>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-lg">
        <?=$this->Form->create($entity,['id' => 'tracking-form', 'type' => 'file']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('progression_id', ucwords('progression'))?>
                        <?=$this->Form->select('progression_id', $progressions,[
                            'class' => 'form-control',
                            'id' => 'progression-id',
                            'required' => true,
                            'empty' => ucwords('select progression'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('status_id', ucwords('status'))?>
                        <?=$this->Form->select('status_id', $statuses,[
                            'class' => 'form-control',
                            'id' => 'status-id',
                            'required' => true,
                            'empty' => ucwords('choose progression'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('remarks', ucwords('remarks'))?>
                        <?=$this->Form->textarea('remarks',[
                            'class' => 'form-control',
                            'id' => 'trackings-remarks',
                            'required' => true,
                            'placeholder' => ucwords('Remarks'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'value' => '-'
                        ])?>
                        <small></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-end align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?=$this->Form->hidden('transaction_id',[
                    'id' => 'transaction-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($transaction->id)
                ])?>
                <?=$this->Form->button(ucwords('Reset'),[
                    'class' => 'btn btn-danger rounded-0',
                    'type' => 'reset',
                    'title' => ucwords('Reset')
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <?php if(boolval($transaction->has_file)):?>
            <button class="btn btn-primary rounded-0 mx-1" id="download" title="Download File" data-id="<?=intval($transaction->id)?>">
                Download File
            </button>
        <?php endif;?>
        <button id="toggle-modal" class="btn btn-primary rounded-0" title="New Tracking">
            New Tracking
        </button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Transaction Form</h3>
            </div>
            <?= $this->Form->create($transaction,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('docsid', ucwords('docsid'))?>
                        <?=$this->Form->text('docsid',[
                            'class' => 'form-control',
                            'id' => 'docsid',
                            'required' => true,
                            'placeholder' => ucwords('docsid'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-7 col-lg-8">
                        <?=$this->Form->label('transaction_name', ucwords('transaction name'))?>
                        <?=$this->Form->text('transaction_name',[
                            'class' => 'form-control',
                            'id' => 'transaction-name',
                            'required' => true,
                            'placeholder' => ucwords('transaction name'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('establishment_id', ucwords('establishment'))?>
                        <?=$this->Form->select('establishment_id', $establishments,[
                            'class' => 'form-control',
                            'id' => 'establishment-id',
                            'required' => true,
                            'empty' => ucwords('select establishment'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('department_id', ucwords('department'))?>
                        <?=$this->Form->select('department_id', $departments,[
                            'class' => 'form-control',
                            'id' => 'department-id',
                            'required' => true,
                            'empty' => ucwords('Choose establishment'),
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                        <?=$this->Form->label('date_release', ucwords('date release'))?>
                        <?=$this->Form->date('date_release',[
                            'class' => 'form-control',
                            'id' => 'date-release',
                            'required' => false,
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-3">
                        <?=$this->Form->label('received_by', ucwords('received by'))?>
                        <?=$this->Form->text('received_by',[
                            'class' => 'form-control',
                            'id' => 'received-by',
                            'required' => true,
                            'placeholder' => ucwords('docsid'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field'),
                            'value' => $transaction->received_by
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                        <?=$this->Form->label('date_received', ucwords('date received'))?>
                        <?=$this->Form->date('date_received',[
                            'class' => 'form-control',
                            'id' => 'date-received',
                            'required' => false,
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('remarks', ucwords('remarks'))?>
                        <?=$this->Form->textarea('remarks',[
                            'class' => 'form-control',
                            'id' => 'remarks',
                            'required' => false,
                            'value' => $transaction->remarks
                        ])?>
                        <small id="remarks-text"></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('file', ucwords('file'))?>
                        <?=$this->Form->file('file',[
                            'class' => 'form-control',
                            'id' => 'file',
                            'required' => false,
                            'accept' => '.pdf, .docx'
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-0 d-flex justify-content-center align-items-center mt-1">
                        <embed src="<?=$this->Url->assetUrl('/files/'.($transaction->document))?>" id="embed" width="800" height="<?=($folder->exists()? strval(800): strval(0))?>">
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <a link href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <?=$this->Form->hidden('has_file',[
                    'id' => 'has-file',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($transaction->has_file)
                ])?>
                <?=$this->Form->hidden('document',[
                    'id' => 'document',
                    'required' => true,
                    'readonly' => true,
                    'value' => $transaction->document
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="timeline" id="trackings">
            <?php foreach ($transaction->trackings as $key => $tracking):?>
                <div class="time-label" data-id="<?=intval($tracking->id)?>">
                    <span class="bg-info">
                        <?=date('Y F d', strtotime($tracking->created))?>
                        <br>
                        <?=strtoupper($tracking->user->name)?>
                    </span>
                </div>
                <div data-id="<?=intval($tracking->id)?>">
                    <i class="fas fa-bullseye bg-success"></i>
                    <div class="timeline-item">
                        <span class="time">
                            <i class="fas fa-clock"></i>
                            <?=date('h:i A', strtotime($tracking->created))?>
                        </span>
                        <h3 class="timeline-header">
                            <a href="javascript:void (0);">
                                <?=ucwords($tracking->progression->progression)?>
                            </a> -
                            <?=ucwords($tracking->status->status)?>
                        </h3>

                        <div class="timeline-body">
                            <?=$tracking->remarks?>
                        </div>
                        <div class="timeline-footer">
                            <a href="javascript:void(0);" class="btn btn-danger btn-sm delete" data-id="<?=intval($tracking->id)?>" title="delete">Delete</a>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <!-- /.col -->
</div>

<?=$this->Html->script('records/transactions/view')?>
