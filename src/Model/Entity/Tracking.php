<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tracking Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $transaction_id
 * @property int $progression_id
 * @property int $status_id
 * @property string $remarks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Transaction $transaction
 * @property \App\Model\Entity\Progression $progression
 * @property \App\Model\Entity\Status $status
 */
class Tracking extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'transaction_id' => true,
        'progression_id' => true,
        'status_id' => true,
        'remarks' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'transaction' => true,
        'progression' => true,
        'status' => true,
    ];

    protected function _setRemarks($value){
        return ucwords($value);
    }

}
