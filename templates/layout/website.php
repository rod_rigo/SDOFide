<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'SDOFIDE';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?> |
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('csrf-token', $this->request->getAttribute('csrfToken')) ?>
    <meta name="turbolinks-cache-control" content="cache">
    <meta name="turbolinks-visit-control" content="reload">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700;800&family=Rubik:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <?=$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css')?>
    <?=$this->Html->css('https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css')?>
    <?=$this->Html->css([
        '/start-up/lib/owlcarousel/assets/owl.carousel.min',
        '/start-up/lib/animate/animate.min',
        '/start-up/css/bootstrap.min',
        '/start-up/css/style',
        '/leaflet/css/leaflet',
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/sweet-alert/js/sweetalert2',
        '/sweet-alert/js/sweetalert2.all',
        '/moment/js/moment',
        '/leaflet/js/leaflet',
        '/leaflet/js/leaflet-search',
        '/scroll-to/js/jquery-scrollto',
        '/turbo-links/js/turbolinks',
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var mainurl = window.location.origin+'/SDOFide/';
    </script>

</head>
<body>

<!-- Spinner Start -->
<div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
</div>
<!-- Spinner End -->

<!-- Topbar Start -->
    <?=$this->element('website/top-bar')?>
<!-- Topbar End -->


<!-- Navbar & Carousel Start -->
    <?=$this->element('website/navbar')?>
<!-- Navbar & Carousel End -->

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

<!-- Footer Start -->
    <?=$this->element('website/footer')?>
<!-- Footer End -->


<!-- Back to Top -->
<a href="javascript:void(0);" class="btn btn-lg btn-primary btn-lg-square rounded back-to-top">
    <i class="bi bi-arrow-up"></i>
</a>

<!-- JavaScript Libraries -->
<?=$this->Html->script('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js')?>
<?=$this->Html->script([
    '/start-up/lib/wow/wow.min',
    '/start-up/lib/easing/easing.min',
    '/start-up/lib/waypoints/waypoints.min',
    '/start-up/lib/counterup/counterup.min',
    '/start-up/lib/owlcarousel/owl.carousel.min',
    '/start-up/js/main'
])?>
<script>
    Turbolinks.start();
    $('a[turbolink]').click(function (e) {
        var href = $(this).attr('href');
        Turbolinks.visit(href,{action:'replace'});
    });
</script>
</body>
</html>
