<?php
/**
 * @var \App\View\AppView $this
 */
?>

<script>
    var projectId = parseInt(<?=intval($project->id)?>);
    var userId = parseInt(<?=intval(@$auth['id'])?>);
    var rate = parseFloat(<?=doubleval(@$rating->rating)?>)
</script>

<div class="modal fade" id="modal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <?=$this->Form->create($entity,['id' => 'form', 'type' => 'file', 'class' => 'w-100']);?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?=$this->Form->label('purpose', ucwords('purpose'))?>
                        <?=$this->Form->textarea('purpose',[
                            'class' => 'form-control form-control-border border-width-2',
                            'id' => 'purpose',
                            'required' => true,
                            'placeholder' => ucwords('purpose'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                        <?=$this->Form->label('password', ucwords('password'))?>
                        <?=$this->Form->password('password',[
                            'class' => 'form-control form-control-border border-width-2',
                            'id' => 'password',
                            'required' => true,
                            'placeholder' => ucwords('password'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>
                    <div class="col-sm-12 col-md-3 col-lg-3 d-flex justify-content-start align-items-end mt-3">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('agreed',[
                                'id' => 'agreed',
                                'label' => false,
                                'hiddenField' => false,
                            ])?>
                            <?=$this->Form->label('agreed', ucwords('agreed'))?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center align-items-center">
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => @$auth['id']
                ])?>
                <?=$this->Form->hidden('project_id',[
                    'id' => 'project-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($project->id)
                ])?>
                <?=$this->Form->hidden('token',[
                    'id' => 'token',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(time())
                ])?>
                <?=$this->Form->hidden('key',[
                    'id' => 'key',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(rand(1,9999))
                ])?>
                <?=$this->Form->hidden('is_allowed',[
                    'id' => 'is-allowed',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ])?>
                <?=$this->Form->hidden('is_agreed',[
                    'id' => 'is-agreed',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(0)
                ])?>
                <?=$this->Form->hidden('remarks',[
                    'id' => 'remarks',
                    'required' => true,
                    'readonly' => true,
                    'value' => '-'
                ])?>
                <button type="button" class="btn btn-default rounded-0" data-dismiss="modal" title="Close">Close</button>
                <?=$this->Form->button(ucwords('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'type' => 'submit',
                    'title' => ucwords('Submit')
                ])?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</div>

<div class="row">

    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <button type="button" id="toggle-modal" class="btn btn-primary rounded-0" title="Request This Project">
            Request This Project
        </button>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-start align-items-center mb-3">
       <h1> <?=doubleval($ratings)?> Users Rate This Project</h1>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6 d-flex justify-content-end align-items-center mb-3">
        <div id="rating"></div>
    </div>

    <div class="col-sm-12 col-md-7 col-lg-8 mb-3">

        <!-- About Me Box -->
        <div class="card card-primary h-100">
            <div class="card-header">
                <h3 class="card-title"><?=strtoupper($project->title)?></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <strong><i class="fas fa-building mr-1"></i> School</strong>

                <p class="text-muted">
                    <?=strtoupper($project->department->department)?>
                </p>

                <hr>

                <strong><i class="fas fa-list-ol mr-1"></i> Batch</strong>

                <p class="text-muted"><?=$project->batch?></p>

                <hr>

                <strong><i class="fas fa-list-ul mr-1"></i> Category</strong>

                <p class="text-muted"><?=ucwords($project->category->category)?></p>

                <hr>

                <strong><i class="fas fa-tasks mr-1"></i> Status</strong>

                <p class="text-muted"><?=ucwords($project->status->status)?></p>

                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Abstract </strong>

                <p class="text-muted">
                    <?=$project->content?>
                </p>

                <hr>

                <strong><i class="fas fa-pencil-alt mr-1"></i> Remarks</strong>

                <p class="text-muted">
                    <?=$project->remarks?>
                </p>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
    <div class="col-sm-12 col-md-5 col-lg-4 mb-3">
        <div class="card h-100">
            <div class="card-body">
                <div class="row h-100">
                    <div class="col-sm-12 col-md-12 col-lg-12 h-80 overflow-y-auto" id="comments">
                        <?php foreach ($recentComments as $recentComment):?>
                            <div class="post">
                                <div class="user-block">
                                    <span class="username">
                                        <a href="javascript:void(0);"><?=$recentComment->user->name?></a>
                                    </span>
                                    <span class="description">
                                        <?=@(new \Moment\Moment($recentComment->created,'Asia/Manila'))->fromNow()->getRelative()?>
                                    </span>
                                </div>
                                <p><?=$recentComment->comment?></p>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 h-20 d-flex justify-content-center align-items-end">
                        <div class="post w-100">
                            <p>
                                <span class="float-right">
                                  <a href="javascript:void (0);" class="link-black text-sm hover-black" id="load-comments">
                                     <i class="fa fa-angle-right mr-1"></i>Load Comments
                                  </a>
                                </span>
                                <span class="float-left">
                                  <a href="javascript:void (0);" class="link-black text-sm hover-black">
                                    <i class="far fa-comments mr-1"></i> Comments (<?=number_format(intval($comments))?>)
                                  </a>
                                </span>
                            </p>

                            <?=$this->Form->create($comment,['id' => 'comment-form', 'type' => 'file', 'class' => 'form-horizontal'])?>
                                <div class="input-group input-group-sm mb-0">
                                    <?=$this->Form->text('comment',[
                                        'id' => 'comment',
                                        'class' => 'form-control form-control-sm',
                                        'placeholder' => ucwords('Comment'),
                                        'required' => true,
                                        'title' => ucwords('enter comment here')
                                    ])?>
                                    <?=$this->Form->hidden('user_id',[
                                        'id' => 'user-id',
                                        'required' => true,
                                        'readonly' => true,
                                        'value' => @$auth['id']
                                    ])?>
                                    <?=$this->Form->hidden('project_id',[
                                        'id' => 'project-id',
                                        'required' => true,
                                        'readonly' => true,
                                        'value' => intval($project->id)
                                    ])?>
                                    <div class="input-group-append">
                                        <?=$this->Form->button('<i class="fa fa-paper-plane" aria-hidden="true"></i>',[
                                            'class' => 'btn btn-primary',
                                            'type' => 'submit',
                                            'escapeTitle' => false
                                        ])?>
                                    </div>
                                </div>
                            <?=$this->Form->end()?>
                        </div>
                    </div>
                </div>
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<?=$this->Html->script('client/projects/view')?>
