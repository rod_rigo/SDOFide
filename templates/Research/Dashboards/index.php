<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-user"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Researchers</span>
                <span class="info-box-number" id="total-clients">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-archive"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Researches</span>
                <span class="info-box-number" id="total-projects">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Today</span>
                <span class="info-box-number" id="total-requests-today">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Week</span>
                <span class="info-box-number" id="total-requests-week">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
            <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Month</span>
                <span class="info-box-number" id="total-requests-month">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
              <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Year</span>
                <span class="info-box-number" id="total-requests-year">0</span>
            </div>
        </div>
    </div>

</div>

<div class="row mb-3">
    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
        <div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Top Rated</h3>
            </div>

            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table m-0">
                        <thead>
                        <tr>
                            <th>Project</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($ratings as $rating):?>
                            <tr>
                                <td><?=strtoupper($rating['project'])?></td>
                                <td><?=doubleval($rating['total'])?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="card-footer clearfix">

            </div>

        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
        <div class="card">
            <div class="card-header border-transparent">
                <h3 class="card-title">Project Popularity</h3>
            </div>

            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table m-0">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Views</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($views as $view):?>
                            <tr>
                                <td><?=strtoupper($view['project'])?></td>
                                <td><?=number_format($view['total'])?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="card-footer clearfix">

            </div>

        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-sm-12 col-md-12 col-lg-12 mt-2">
        <div class="card p-3">
            <canvas id="requests-chart" width="20" height="20" style="height: 35em;"></canvas>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
        <div class="card p-3">
            <canvas id="project-chart" width="20" height="20" style="height: 35em;"></canvas>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
        <div class="card p-3">
            <canvas id="status-chart" width="20" height="20" style="height: 35em;"></canvas>
        </div>
    </div>
</div>

<?=$this->Html->script('research/dashboards/index')?>
