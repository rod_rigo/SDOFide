<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Memos Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\DocumentsTable&\Cake\ORM\Association\HasMany $Documents
 *
 * @method \App\Model\Entity\Memo newEmptyEntity()
 * @method \App\Model\Entity\Memo newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Memo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Memo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Memo findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Memo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Memo[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Memo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Memo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Memo[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Memo[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Memo[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Memo[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MemosTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('memos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Documents', [
            'foreignKey' => 'memo_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('memo')
            ->maxLength('memo', 255)
            ->requirePresence('memo', 'create')
            ->notEmptyString('memo');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->requirePresence('code', 'create')
            ->notEmptyString('code');

        $validator
            ->numeric('is_active')
            ->notEmptyString('is_active')
            ->add('is_active','is_active',[
                'rule'=> function($value){
                    $isActive = [0, 1];

                    if(!in_array(intval($value), $isActive)){
                        return ucwords('please select active value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('is_shown')
            ->notEmptyString('is_shown')
            ->add('is_shown','is_shown',[
                'rule'=> function($value){
                    $isShown = [0, 1];

                    if(!in_array(intval($value), $isShown)){
                        return ucwords('please select is shown value');
                    }

                    return true;

                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->isUnique(['memo'], ucwords('this memo is already exists')), ['errorField' => 'memo']);

        return $rules;
    }
}
