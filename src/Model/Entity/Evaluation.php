<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Evaluation Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $head_id
 * @property int $department_id
 * @property int $monitoring_type_id
 * @property int $progression_id
 * @property int $status_id
 * @property string|null $analysis
 * @property string|null $recommendations
 * @property \Cake\I18n\Date $date
 * @property string|null $dr_file
 * @property string|null $air_file
 * @property string|null $irp_file
 * @property string|null $ptr_file
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Head $head
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Classroom[] $classrooms
 * @property \App\Model\Entity\MonitoringDate[] $monitoring_dates
 * @property \App\Model\Entity\Subscription[] $subscriptions
 * @property \App\Model\Entity\MonitoringType $monitoring_type
 * @property \App\Model\Entity\Equipment[] $equipments
 * @property \App\Model\Entity\Progression $progression
 * @property \App\Model\Entity\Status $status
 */
class Evaluation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'head_id' => true,
        'department_id' => true,
        'monitoring_type_id' => true,
        'progression_id' => true,
        'status_id' => true,
        'analysis' => true,
        'recommendations' => true,
        'date' => true,
        'dr_file' => true,
        'air_file' => true,
        'irp_file' => true,
        'ptr_file' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'head' => true,
        'department' => true,
        'classrooms' => true,
        'monitoring_dates' => true,
        'subscriptions' => true,
        'monitoring_type' => true,
        'equipments' => true,
        'progression' => true,
        'status' => true,
    ];

    protected function _setAnalysis($value){
        return (intval(strlen($value)))? strtoupper($value): $value;
    }

    protected function _setRecommendations($value){
        return (intval(strlen($value)))? strtoupper($value): $value;
    }

}
