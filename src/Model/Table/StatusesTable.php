<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Statuses Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ProgressionsTable&\Cake\ORM\Association\BelongsTo $Progressions
 * @property \App\Model\Table\TrackingsTable&\Cake\ORM\Association\HasMany $Trackings
 *
 * @method \App\Model\Entity\Status newEmptyEntity()
 * @method \App\Model\Entity\Status newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Status[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Status get($primaryKey, $options = [])
 * @method \App\Model\Entity\Status findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Status patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Status[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Status|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Status saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Status[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Status[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Status[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Status[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StatusesTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('statuses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Progressions', [
            'foreignKey' => 'progression_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Trackings', [
            'foreignKey' => 'status_id',
        ]);
        $this->hasMany('Projects', [
            'foreignKey' => 'status_id',
        ]);
        $this->hasMany('Students', [
            'foreignKey' => 'status_id',
        ]);
        $this->hasMany('Evaluations', [
            'foreignKey' => 'status_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('progression_id')
            ->requirePresence('progression_id', true)
            ->notEmptyString('progression_id', ucwords('please fill out this field'), false);

        $validator
            ->scalar('status')
            ->maxLength('status', 255, ucwords('this field must not exceed at 255 characters'))
            ->requirePresence('status', true)
            ->notEmptyString('status', ucwords('please fill out this field'));

        $validator
            ->numeric('is_active')
            ->notEmptyString('is_active')
            ->add('is_active','is_active',[
                'rule'=> function($value){
                    $isActive = [0, 1];

                    if(!in_array(intval($value), $isActive)){
                        return ucwords('please select active value');
                    }

                    return true;

                }
            ]);

        $validator
            ->numeric('is_default')
            ->notEmptyString('is_default')
            ->add('is_default','is_default',[
                'rule'=> function($value){
                    $isDefault = [0, 1];

                    if(!in_array(intval($value), $isDefault)){
                        return ucwords('please select default value');
                    }

                    return true;

                }
            ]);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['progression_id'], 'Progressions'), ['errorField' => 'progression_id']);
        $rules->add($rules->isUnique(['progression_id', 'status'],ucwords('this status is already exists in this progression')), ['errorField' => 'status']);

        return $rules;
    }
}
