<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property int $is_admin
 * @property int $is_records
 * @property int $is_mapeh
 * @property int $is_research
 * @property int $is_ict_monitoring
 * @property int $is_client
 * @property int $is_active
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Department[] $departments
 * @property \App\Model\Entity\Establishment[] $establishments
 * @property \App\Model\Entity\Progression[] $progressions
 * @property \App\Model\Entity\Status[] $statuses
 * @property \App\Model\Entity\Tracking[] $trackings
 * @property \App\Model\Entity\Transaction[] $transactions
 * @property \App\Model\Entity\Memo[] $memos
 * @property \App\Model\Entity\About[] $abouts
 * @property \App\Model\Entity\Contact[] $contacts
 * @property \App\Model\Entity\Team[] $teams
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Position[] $positions
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\Rating[] $rating
 * @property \App\Model\Entity\View[] $views
 * @property \App\Model\Entity\Profile $profile
 * @property \App\Model\Entity\Gender[] $genders
 * @property \App\Model\Entity\Level[] $levels
 * @property \App\Model\Entity\Grade[] $grades
 * @property \App\Model\Entity\Student[] $students
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'username' => true,
        'email' => true,
        'password' => true,
        'is_admin' => true,
        'is_records' => true,
        'is_mapeh' => true,
        'is_research' => true,
        'is_ict_monitoring' => true,
        'is_client' => true,
        'is_active' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'departments' => true,
        'establishments' => true,
        'progressions' => true,
        'statuses' => true,
        'trackings' => true,
        'transactions' => true,
        'memos' => true,
        'abouts' => true,
        'contacts' => true,
        'teams' => true,
        'categories' => true,
        'positions' => true,
        'comments' => true,
        'projects' => true,
        'ratings' => true,
        'views' => true,
        'profile' => true,
        'genders' => true,
        'levels' => true,
        'grades' => true,
        'students' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token',
    ];

    protected function _setPassword($value){
        return (new DefaultPasswordHasher())->hash($value);
    }

}
