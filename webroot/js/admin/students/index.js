'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'students/';
    var url = '';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getStudents',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {

        },
        buttons: [
            {
                text: 'XLSX',
                action: function (e) {
                    Swal.fire({
                        title:'Export To XLSX',
                        text: null,
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            window.open(baseurl+'xlsx?department_id='+($('#department').val()));
                        }
                    })
                },
                attr:  {
                    id: 'pdf',
                    class:'btn btn-success rounded-0',
                },
                footer:true
            },
            {
                text: 'PDF',
                action: function (e) {
                    Swal.fire({
                        title:'Export To PDF',
                        text: null,
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            window.open(baseurl+'pdf?department_id='+($('#department').val()));
                        }
                    })
                },
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [10],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.modified).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [11],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'name'},
            { data: 'department.department'},
            { data: 'level.level'},
            { data: 'grade.grade'},
            { data: 'gender.gender'},
            { data: 'age'},
            { data: 'status.status'},
            { data: 'scale'},
            { data: 'user.name'},
            { data: 'modified'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'view/'+(dataId);
        Swal.fire({
            title: 'Open In New Window',
            text: 'Are You Sure?',
            icon: 'question',
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            denyButtonColor: '#808080',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            denyButtonText: 'Cancel',
            allowOutsideClick:false,
        }).then(function (result) {
            if (result.isConfirmed) {
                window.open(href);
            }else if(result.isDenied){
                Swal.close();
            }else{
                Turbolinks.visit(href, {action: 'advance'});
            }
        });
    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        student_chart.destroy();
        getBmiChart();
        table.ajax.url(baseurl+'getStudents?department_id='+($('#department').val())).load(null, false);
    });

    var student_canvas = document.querySelector('#student-chart').getContext('2d');
    var student_chart;

    function getBmi() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getBmi?department_id='+($('#department').val()),
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.scale).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getBmiChart() {
        student_chart =  new Chart(student_canvas, {
            type: 'bar',
            data: {
                labels: getBmi().label,
                datasets: [
                    {
                        label:'Total',
                        data: getBmi().data
                    }
                ]
            },
            options: {
                indexAxis:'x',
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Student Scales',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

    getBmiChart();

});