'use strict';
$(document).ready(function () {
    const baseurl = mainurl+'requests/';
    var url = '';
    var isAllowed = [
        '<span class="text-danger"><i class="fas fa-times"></i></span>',
        '<span class="text-success"><i class="fas fa-check"></i></span>',
    ];
    var title = 'sample';

    var datatable = $('#datatable');
    var table = datatable.DataTable({
        destroy:true,
        dom:'lBfrtip',
        processing:true,
        responsive: true,
        serchDelay:3500,
        deferRender: true,
        pagingType: 'full_numbers',
        order:[[0, 'asc']],
        lengthMenu:[100, 200, 500, 1000],
        ajax:{
            url:baseurl+'getRequests',
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function (e) {

            },
            error:function (data, status, xhr) {
                window.location.reload();
            }
        },
        createdRow: function( row, data, key ) {
            $(row).find('td:nth-child(6)').addClass('text-center');
        },
        buttons: [
            {
                extend: 'print',
                title: title,
                text: 'Print',
                attr:  {
                    id: 'print',
                    class:'btn btn-secondary rounded-0',
                },
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10px' )
                        .prepend('');
                    $(win.document.body).find( 'table tbody' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ).css({'background':'transparent'});
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'excelHtml5',
                attr:  {
                    id: 'excel',
                    class:'btn btn-success rounded-0',
                },
                title: title,
                text: 'Excel',
                tag: 'button',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'Excel',
                        text:'Export To Excel?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.excelHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
            {
                extend: 'pdfHtml5',
                attr:  {
                    id: 'pdf',
                    class:'btn btn-danger rounded-0',
                },
                text: 'PDF',
                title: title,
                tag: 'button',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [0,1,2,3,4,5,6,7,8,]
                },
                action: function(e, dt, node, config) {
                    Swal.fire({
                        title:'PDF',
                        text:'Export To PDF?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes'
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            setTimeout(function(){
                                $.fn.dataTable.ext.buttons.pdfHtml5.action.call(dt.button(this), e, dt, node, config);
                            }, 1000);
                        }
                    });
                },
                customize: function(doc) {
                    doc.pageMargins = [2, 2, 2, 2 ];
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                    doc.styles.tableHeader.fontSize = 7;
                    doc.styles.tableBodyEven.fontSize = 7;
                    doc.styles.tableBodyOdd.fontSize = 7;
                    doc.styles.tableFooter.fontSize = 7;
                },
                download: 'open',
                messageTop: function () {
                    return null;
                },
                messageBottom: function () {
                    return 'Printed At '+(moment().format('Y-MM-DD h:m A'));
                },
                footer:true
            },
        ],
        columnDefs: [
            {
                targets: [0],
                data: null,
                render: function ( data, type, full, meta ) {
                    const row = meta.row;
                    return  row+1;
                }
            },
            {
                targets: [5],
                data: null,
                render: function(data,type,row,meta){
                    return isAllowed[parseInt(row.is_allowed)];
                }
            },
            {
                targets: [8],
                data: null,
                render: function(data,type,row,meta){
                    return moment(row.created).format('Y-MM-DD hh:mm A');
                }
            },
            {
                targets: [9],
                data: null,
                orderable:false,
                searchable:false,
                render: function(data,type,row,meta){
                    return '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-info rounded-0 text-white view" title="View">View</a> | '+
                        '<a data-id="'+(parseInt(row.id))+'" class="btn btn-sm btn-danger rounded-0 text-white delete" title="Delete">Delete</a>';
                }
            }
        ],
        columns: [
            { data: 'id'},
            { data: 'user.name'},
            { data: 'project.title'},
            { data: 'project.author'},
            { data: 'purpose'},
            { data: 'is_allowed'},
            { data: 'approved_at'},
            { data: 'remarks'},
            { data: 'created'},
            { data: 'id'},
        ]
    });

    datatable.on('click','.view',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'edit/'+(parseInt(dataId));
        $.ajax({
            url:href,
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                url = 'edit/'+(parseInt(dataId));
                $('button[type="reset"]').fadeOut(100);
            },
        }).done(function (data, status, xhr) {
            $('#purpose').val(data.purpose);
            $('#remarks').val(data.remarks);
            $('#allowed').prop('checked', data.is_allowed);
            $('#is-allowed').val(data.is_allowed);
            $('#modal').modal('toggle');
            Swal.close();
        }).fail(function (data, status, xhr) {
            swal('error', 'Error', data.responseJSON.message);
        });

    });

    datatable.on('click','.delete',function (e) {
        e.preventDefault();
        var dataId = $(this).attr('data-id');
        var href = baseurl+'delete/'+(parseInt(dataId));
        Swal.fire({
            title: 'Delete Data',
            text: 'Are You Sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(function (result) {
            if (result.isConfirmed) {
                $.ajax({
                    url:href,
                    type: 'DELETE',
                    method: 'DELETE',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'JSON',
                    beforeSend: function (e) {
                        Swal.fire({
                            icon: 'info',
                            title: null,
                            text: 'Please Wait!...',
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            timerProgressBar: false,
                            didOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                }).done(function (data, status, xhr) {
                    swal('success', null, data.message);
                    table.ajax.reload(null, false);
                }).fail(function (data, status, xhr) {
                    swal('error', 'Error', data.responseJSON.message);
                });
            }
        });
    });

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl + url,
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('small').empty();
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            $('#modal').modal('toggle');
            table.ajax.reload(null, false);
            swal('success', null, data.message);
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);


        });
    });

    $('#toggle-modal').click(function (e) {
        url = 'add';
        $('#modal').modal('toggle');
    });

    $('#modal').on('hidden.bs.modal', function (e) {
        $('#form')[0].reset();
        $('small').empty();
        $('.form-control').removeClass('is-invalid');
        $('button[type="reset"]').fadeIn(100);
        $('button[type="reset"], button[type="submit"]').prop('disabled', false);
    });

    $('#allowed').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-allowed').val(Number(checked));
    });

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: null,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        })
    }

});