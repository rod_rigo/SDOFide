<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Banner $banner
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Banners Form</h3>
            </div>
            <?= $this->Form->create($banner,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('officer', ucwords('officer'))?>
                        <?=$this->Form->text('officer',[
                            'class' => 'form-control',
                            'id' => 'officer',
                            'required' => true,
                            'placeholder' => ucwords('officer'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <?=$this->Form->label('position', ucwords('position'))?>
                        <?=$this->Form->text('position',[
                            'class' => 'form-control',
                            'id' => 'position',
                            'required' => true,
                            'placeholder' => ucwords('position'),
                            'pattern' => '(.){1,}',
                            'title' => ucwords('Please Fill Out This Field')
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                                <img src="<?=$this->Url->assetUrl('/img/'.($banner->header))?>" class="img-thumbnail" id="header-preview" alt="Logo Image" height="400" width="400" style="object-fit: contain;" loading="lazy">
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 mt-4">

                                <div class="form-group">
                                    <?=$this->Form->label('header_file', ucwords('Header file'))?>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <?=$this->Form->file('header_file',[
                                                'class' => 'custom-file-input rounded-0',
                                                'id' => 'logo-file',
                                                'data-target' => '#header-preview',
                                                'accept' => 'image/*',
                                            ])?>
                                            <?=$this->Form->label('header_file', ucwords('Choose file'),[
                                                'class' => 'custom-file-label rounded-0'
                                            ])?>
                                        </div>
                                    </div>
                                    <small></small>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-6 mt-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-center align-items-center">
                                <img src="<?=$this->Url->assetUrl('/img/'.($banner->footer))?>" class="img-thumbnail" id="footer-preview" alt="Banners Image" height="400" width="400" style="object-fit: contain;" loading="lazy">
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-12 mt-4">

                                <div class="form-group">
                                    <?=$this->Form->label('footer_file', ucwords('footer file'))?>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <?=$this->Form->file('footer_file',[
                                                'class' => 'custom-file-input rounded-0',
                                                'id' => 'logo-file',
                                                'data-target' => '#footer-preview',
                                                'accept' => 'image/*',
                                            ])?>
                                            <?=$this->Form->label('footer_file', ucwords('Choose file'),[
                                                'class' => 'custom-file-label rounded-0'
                                            ])?>
                                        </div>
                                    </div>
                                    <small></small>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                        <div class="icheck-primary d-inline">
                            <?=$this->Form->checkbox('active',[
                                'id' => 'active',
                                'label' => false,
                                'hiddenField' => false,
                                'checked' => boolval($banner->is_active)
                            ])?>
                            <?=$this->Form->label('active', ucwords('Active'))?>
                        </div>
                        <small></small>
                    </div>

                </div>

            </div>

            <div class="card-footer d-flex justify-content-end align-items-center">
                <?= $this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'value' => intval(@$auth['id'])
                ]);?>
                <?= $this->Form->hidden('is_active',[
                    'id' => 'is-active',
                    'value' => intval($banner->is_active),
                    'required' => true
                ]);?>
                <?= $this->Form->hidden('header',[
                    'id' => 'header',
                    'value' => strval($banner->header),
                    'required' => true
                ]);?>
                <?= $this->Form->hidden('footer',[
                    'id' => 'footer',
                    'value' => strval($banner->footer),
                    'required' => true
                ]);?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Banners', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/banners/edit')?>

