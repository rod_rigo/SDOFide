<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="container-fluid text-white" style="background: #061429;">
    <div class="container text-center">
        <div class="row justify-content-end">
            <div class="col-lg-8 col-md-6">
                <div class="d-flex align-items-center justify-content-center" style="height: 75px;">
                    <p class="mb-0">&copy; <a class="text-white border-bottom" href="javascript:void (0);">
                            Records Unit Office
                        </a>. All Rights Reserved 2024.
                </div>
            </div>
        </div>
    </div>
</div>
