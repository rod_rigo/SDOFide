<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$path = WWW_ROOT. 'excel'. DS. 'PFT-Form.xlsx';

$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);

$start = intval(12);
$no = intval(1);

$worksheet = $spreadsheet->getActiveSheet();

foreach ($students as $key => $student){

    $worksheet
        ->setCellValue(strval('A'.$start), strval($no));

    $worksheet
        ->setCellValue(strval('B'.$start), strtoupper(@$student['name']));

    $worksheet
        ->setCellValue(strval('C'.$start), strval(@$student['age']));

    $worksheet
        ->setCellValue(strval('D'.$start), strval(@$student['height']));

    $worksheet
        ->setCellValue(strval('E'.$start), strval(@$student['weight']));

    $worksheet
        ->setCellValue(strval('F'.$start), strval(@$student['bmi']));

    $worksheet
        ->setCellValue(strval('G'.$start), strval(@$student['resting_heart_rest']));

    $worksheet
        ->setCellValue(strval('H'.$start), strval(@$student['pulse_rate']));

    $worksheet
        ->setCellValue(strval('I'.$start), strval(@$student['partial_curl_up']));

    $worksheet
        ->setCellValue(strval('J'.$start), strval(@$student['push_up']));

    $worksheet
        ->setCellValue(strval('K'.$start), strval(@$student['stork_stand_left']));

    $worksheet
        ->setCellValue(strval('L'.$start), strval(@$student['stork_stand_right']));

    $worksheet
        ->setCellValue(strval('M'.$start), strval(@$student['meter_run']));

    $worksheet
        ->setCellValue(strval('N'.$start), strval(@$student['shuttle_run']));

    $worksheet
        ->setCellValue(strval('O'.$start), strval(@$student['tive_hand_wall']));

    $worksheet
        ->setCellValue(strval('P'.$start), strval(@$student['ruler_drop_test']));

    $worksheet
        ->setCellValue(strval('Q'.$start), strval(@$student['sit_and_reach_result']));

    $worksheet
        ->setCellValue(strval('R'.$start), strval(@$student['standing_long_jump_total']));

    $start++;
    $no++;
}


// Save the spreadsheet
$writer = new Xlsx($spreadsheet);
header('Content-Disposition: attachment; filename="'. urlencode('PFT.xlsx').'"');
$writer->save('php://output');
exit(0);