<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Head Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $establishment_id
 * @property int $department_id
 * @property string $head
 * @property string $ict_coordinator
 * @property string $property_custodian
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Establishment $establishment
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Evaluation[] $evaluations
 */
class Head extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'establishment_id' => true,
        'department_id' => true,
        'head' => true,
        'ict_coordinator' => true,
        'property_custodian' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'establishment' => true,
        'department' => true,
        'evaluations' => true,
    ];

    protected function _setHead($value){
        return strtoupper($value);
    }

    protected function _setIctCoordinator($value){
        return strtoupper($value);
    }

    protected function _setPropertyCustodian($value){
        return strtoupper($value);
    }

}
