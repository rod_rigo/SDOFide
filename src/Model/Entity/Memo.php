<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Memo Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $memo
 * @property string $code
 * @property int $is_active
 * @property int $is_shown
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Document[] $documents
 */
class Memo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'memo' => true,
        'code' => true,
        'is_active' => true,
        'is_shown' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'documents' => true,
    ];

    protected function _setMemo($value){
        return ucwords($value);
    }

    protected function _setCode($value){
        return strtoupper($value);
    }

}
