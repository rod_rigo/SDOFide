<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Students Model
 *
 * @property \App\Model\Table\DepartmentsTable&\Cake\ORM\Association\BelongsTo $Departments
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\EstablishmentsTable&\Cake\ORM\Association\BelongsTo $Establishments
 * @property \App\Model\Table\LevelsTable&\Cake\ORM\Association\BelongsTo $Levels
 * @property \App\Model\Table\GradesTable&\Cake\ORM\Association\BelongsTo $Grades
 * @property \App\Model\Table\GendersTable&\Cake\ORM\Association\BelongsTo $Genders
 * @property \App\Model\Table\StatusesTable&\Cake\ORM\Association\BelongsTo $Statuses
 *
 * @method \App\Model\Entity\Student newEmptyEntity()
 * @method \App\Model\Entity\Student newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Student[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Student get($primaryKey, $options = [])
 * @method \App\Model\Entity\Student findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Student patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Student[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Student|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Student saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Student[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Student[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Student[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Student[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StudentsTable extends Table
{

    use SoftDeleteTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('students');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Departments', [
            'foreignKey' => 'department_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Establishments', [
            'foreignKey' => 'establishment_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Levels', [
            'foreignKey' => 'level_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Grades', [
            'foreignKey' => 'grade_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Genders', [
            'foreignKey' => 'gender_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Statuses', [
            'foreignKey' => 'status_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, true);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', true)
            ->notEmptyString('name', ucwords('please fill out this field'), false);

        $validator
            ->numeric('age')
            ->requirePresence('age', true)
            ->notEmptyString('age', ucwords('please fill out this field'), false);

        $validator
            ->numeric('height')
            ->requirePresence('height', true)
            ->notEmptyString('height', ucwords('please fill out this field'), false);

        $validator
            ->numeric('weight')
            ->requirePresence('weight', true)
            ->notEmptyString('weight', ucwords('please fill out this field'), false);

        $validator
            ->numeric('bmi')
            ->requirePresence('bmi', true)
            ->notEmptyString('bmi', ucwords('please fill out this field'), false);

        $validator
            ->numeric('resting_heart_rest')
            ->requirePresence('resting_heart_rest', true)
            ->notEmptyString('resting_heart_rest', ucwords('please fill out this field'), false);

        $validator
            ->numeric('pulse_rate')
            ->requirePresence('pulse_rate', true)
            ->notEmptyString('pulse_rate', ucwords('please fill out this field'), false);

        $validator
            ->numeric('partial_curl_up')
            ->requirePresence('partial_curl_up', true)
            ->notEmptyString('partial_curl_up', ucwords('please fill out this field'), false);

        $validator
            ->numeric('push_up')
            ->requirePresence('push_up', true)
            ->notEmptyString('push_up', ucwords('please fill out this field'), false);

        $validator
            ->numeric('stork_stand_left')
            ->requirePresence('stork_stand_left', true)
            ->notEmptyString('stork_stand_left', ucwords('please fill out this field'), false);

        $validator
            ->numeric('stork_stand_right')
            ->requirePresence('stork_stand_right', true)
            ->notEmptyString('stork_stand_right', ucwords('please fill out this field'), false);

        $validator
            ->numeric('meter_run')
            ->requirePresence('meter_run', true)
            ->notEmptyString('meter_run', ucwords('please fill out this field'), false);

        $validator
            ->numeric('shuttle_run')
            ->requirePresence('shuttle_run', true)
            ->notEmptyString('shuttle_run', ucwords('please fill out this field'), false);

        $validator
            ->numeric('tive_hand_wall')
            ->requirePresence('tive_hand_wall', true)
            ->notEmptyString('tive_hand_wall', ucwords('please fill out this field'), false);

        $validator
            ->numeric('ruler_drop_test')
            ->requirePresence('ruler_drop_test', true)
            ->notEmptyString('ruler_drop_test', ucwords('please fill out this field'), false);

        $validator
            ->numeric('vertical_jump')
            ->requirePresence('vertical_jump', true)
            ->notEmptyString('vertical_jump', ucwords('please fill out this field'), false);

        $validator
            ->numeric('standing_long_jump_one')
            ->requirePresence('standing_long_jump_one', true)
            ->notEmptyString('standing_long_jump_one', ucwords('please fill out this field'), false);

        $validator
            ->numeric('standing_long_jump_two')
            ->requirePresence('standing_long_jump_two', true)
            ->notEmptyString('standing_long_jump_two', ucwords('please fill out this field'), false);

        $validator
            ->numeric('standing_long_jump_total')
            ->requirePresence('standing_long_jump_total', true)
            ->notEmptyString('standing_long_jump_total', ucwords('please fill out this field'), false);

        $validator
            ->numeric('hexagon_agility_clockwise')
            ->requirePresence('hexagon_agility_clockwise', true)
            ->notEmptyString('hexagon_agility_clockwise', ucwords('please fill out this field'), false);

        $validator
            ->numeric('hexagon_agility_counter_clockwise')
            ->requirePresence('hexagon_agility_counter_clockwise', true)
            ->notEmptyString('hexagon_agility_counter_clockwise', ucwords('please fill out this field'), false);

        $validator
            ->numeric('hexagon_agility_result')
            ->requirePresence('hexagon_agility_result', true)
            ->notEmptyString('hexagon_agility_result', ucwords('please fill out this field'), false);

        $validator
            ->numeric('kick_test')
            ->requirePresence('kick_test', true)
            ->notEmptyString('kick_test', ucwords('please fill out this field'), false);

        $validator
            ->numeric('sit_and_reach_one')
            ->requirePresence('sit_and_reach_one', true)
            ->notEmptyString('sit_and_reach_one', ucwords('please fill out this field'), false);

        $validator
            ->numeric('sit_and_reach_two')
            ->requirePresence('sit_and_reach_two', true)
            ->notEmptyString('sit_and_reach_two', ucwords('please fill out this field'), false);

        $validator
            ->numeric('sit_and_reach_result')
            ->requirePresence('sit_and_reach_result', true)
            ->notEmptyString('sit_and_reach_result', ucwords('please fill out this field'), false);

        $validator
            ->numeric('stick_drop_test_one')
            ->requirePresence('stick_drop_test_one', true)
            ->notEmptyString('stick_drop_test_one', ucwords('please fill out this field'), false);

        $validator
            ->numeric('stick_drop_test_two')
            ->requirePresence('stick_drop_test_two', true)
            ->notEmptyString('stick_drop_test_two', ucwords('please fill out this field'), false);

        $validator
            ->numeric('stick_drop_test_three')
            ->requirePresence('stick_drop_test_three', true)
            ->notEmptyString('stick_drop_test_three', ucwords('please fill out this field'), false);

        $validator
            ->numeric('stick_drop_test_four')
            ->requirePresence('stick_drop_test_four', true)
            ->notEmptyString('stick_drop_test_four', ucwords('please fill out this field'), false);

        $validator
            ->numeric('stick_drop_test_result')
            ->requirePresence('stick_drop_test_result', true)
            ->notEmptyString('stick_drop_test_result', ucwords('please fill out this field'), false);

        $validator
            ->numeric('zipper_test_one')
            ->requirePresence('zipper_test_one', true)
            ->notEmptyString('zipper_test_one', ucwords('please fill out this field'), false);

        $validator
            ->numeric('zipper_test_two')
            ->requirePresence('zipper_test_two', true)
            ->notEmptyString('zipper_test_two', ucwords('please fill out this field'), false);

        $validator
            ->numeric('zipper_test_result')
            ->requirePresence('zipper_test_result', true)
            ->notEmptyString('zipper_test_result', ucwords('please fill out this field'), false);

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 4294967295)
            ->requirePresence('remarks', true)
            ->notEmptyString('remarks', ucwords('please fill out this field'), false);

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['department_id'], 'Departments'), ['errorField' => 'department_id']);
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);
        $rules->add($rules->existsIn(['establishment_id'], 'Establishments'), ['errorField' => 'establishment_id']);
        $rules->add($rules->existsIn(['level_id'], 'Levels'), ['errorField' => 'level_id']);
        $rules->add($rules->existsIn(['grade_id'], 'Grades'), ['errorField' => 'grade_id']);
        $rules->add($rules->existsIn(['gender_id'], 'Genders'), ['errorField' => 'gender_id']);
        $rules->add($rules->existsIn(['status_id'], 'Statuses'), ['errorField' => 'status_id']);

        return $rules;
    }
}
