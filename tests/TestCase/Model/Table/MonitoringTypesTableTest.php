<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MonitoringTypesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MonitoringTypesTable Test Case
 */
class MonitoringTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MonitoringTypesTable
     */
    protected $MonitoringTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MonitoringTypes',
        'app.Users',
        'app.MonitoringDates',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('MonitoringTypes') ? [] : ['className' => MonitoringTypesTable::class];
        $this->MonitoringTypes = $this->getTableLocator()->get('MonitoringTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MonitoringTypes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\MonitoringTypesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\MonitoringTypesTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
