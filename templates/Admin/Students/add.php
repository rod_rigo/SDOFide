<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 * @var \Cake\Collection\CollectionInterface|string[] $departments
 * @var \Cake\Collection\CollectionInterface|string[] $users
 * @var \Cake\Collection\CollectionInterface|string[] $establishments
 * @var \Cake\Collection\CollectionInterface|string[] $levels
 * @var \Cake\Collection\CollectionInterface|string[] $grades
 * @var \Cake\Collection\CollectionInterface|string[] $genders
 * @var \Cake\Collection\CollectionInterface|string[] $statuses
 */
?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Student Form</h3>
            </div>
            <?= $this->Form->create($student,['type' => 'file', 'id' => 'form', 'class' => 'form-horizontal']) ?>
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-8 col-lg-9 mt-3">
                        <?=$this->Form->label('name', ucwords('name'))?>
                        <?=$this->Form->text('name',[
                            'class' => 'form-control',
                            'id' => 'name',
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('name'),
                            'pattern' => '(.){1,}',
                            'required' => true,
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-3 mt-3">
                        <?=$this->Form->label('gender_id', ucwords('gender'))?>
                        <?=$this->Form->select('gender_id', $genders,[
                            'class' => 'form-control',
                            'id' => 'gender-id',
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Select gender'),
                            'pattern' => '(.){1,}',
                            'required' => true,
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-5 mt-3">
                        <?=$this->Form->label('department_id', ucwords('Department'))?>
                        <?=$this->Form->select('department_id', $departments,[
                            'class' => 'form-control',
                            'id' => 'department-id',
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Select department'),
                            'pattern' => '(.){1,}',
                            'required' => true,
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('level_id', ucwords('Level'))?>
                        <?=$this->Form->select('level_id', $levels,[
                            'class' => 'form-control',
                            'id' => 'level-id',
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Select level'),
                            'pattern' => '(.){1,}',
                            'required' => true,
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                        <?=$this->Form->label('grade_id', ucwords('Grade'))?>
                        <?=$this->Form->select('grade_id', [],[
                            'class' => 'form-control',
                            'id' => 'grade-id',
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Choose Level'),
                            'pattern' => '(.){1,}',
                            'required' => true,
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-2 mt-3">
                        <?=$this->Form->label('age', ucwords('age'))?>
                        <?=$this->Form->number('age',[
                            'class' => 'form-control',
                            'id' => 'age',
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('age'),
                            'pattern' => '([0-9]){1,}',
                            'required' => true,
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                        <?=$this->Form->label('height', ucwords('height (CM)'))?>
                        <?=$this->Form->number('height',[
                            'class' => 'form-control',
                            'id' => 'height',
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('height'),
                            'pattern' => '([0-9\.]){1,}',
                            'required' => true,
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-3 mt-3">
                        <?=$this->Form->label('weight', ucwords('weight (KG)'))?>
                        <?=$this->Form->number('weight',[
                            'class' => 'form-control',
                            'id' => 'weight',
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('weight'),
                            'pattern' => '([0-9\.]){1,}',
                            'required' => true,
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-3 col-lg-4 mt-3">
                        <?=$this->Form->label('bmi', ucwords('BMI'))?>
                        <?=$this->Form->number('bmi',[
                            'class' => 'form-control',
                            'id' => 'bmi',
                            'title' => ucwords('please fill out this field'),
                            'placeholder' => ucwords('BMI'),
                            'pattern' => '([0-9\.]){1,}',
                            'required' => true,
                            'min' => 0
                        ])?>
                        <small></small>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-3 mt-3">
                        <?=$this->Form->label('resting_heart_rest', ucwords('resting heart rest'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('resting_heart_rest',[
                                'class' => 'form-control',
                                'id' => 'resting-heart-rest',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('resting heart rest'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-heart"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-3 mt-3">
                        <?=$this->Form->label('pulse_rate', ucwords('pulse rate'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('pulse_rate',[
                                'class' => 'form-control',
                                'id' => 'pulse-rate',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('pulse rate'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-hand-paper"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-3 mt-3">
                        <?=$this->Form->label('partial_curl_up', ucwords('partial curl up (Freq.)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('partial_curl_up',[
                                'class' => 'form-control',
                                'id' => 'partial-curl-up',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('partial curl up (Freq.)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-hand-peace"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-3 mt-3">
                        <?=$this->Form->label('push_up', ucwords('push up (Freq.)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('push_up',[
                                'class' => 'form-control',
                                'id' => 'push-up',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('push-up (Freq.)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-hand-rock"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('stork_stand_left', ucwords('stork stand (Left)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('stork_stand_left',[
                                'class' => 'form-control',
                                'id' => 'stork-stand-left',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('stork stand (Left)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-male"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('stork_stand_right', ucwords('stork stand (Right)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('stork_stand_right',[
                                'class' => 'form-control',
                                'id' => 'stork-stand-right',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('stork stand (Right)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-male"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('meter_run', ucwords('meter run (Seconds)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('meter_run',[
                                'class' => 'form-control',
                                'id' => 'meter-run',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('meter run (Seconds)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-road"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('shuttle_run', ucwords('shuttle run (Seconds)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('shuttle_run',[
                                'class' => 'form-control',
                                'id' => 'shuttle-run',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('shuttle run (Seconds)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-bullhorn"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('tive_hand_wall', ucwords('tive hand wall'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('tive_hand_wall',[
                                'class' => 'form-control',
                                'id' => 'tive-hand-wall',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('tive hand wall'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-hand-paper"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('ruler_drop_test', ucwords('ruler drop test'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('ruler_drop_test',[
                                'class' => 'form-control',
                                'id' => 'ruler-drop-test',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('ruler drop test'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-ruler"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('vertical_jump', ucwords('vertical jump (CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('vertical_jump',[
                                'class' => 'form-control',
                                'id' => 'vertical-jump',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('vertical jump (CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-child"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="w-100"></div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('standing_long_jump_one', ucwords('standing long jump (1)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('standing_long_jump_one',[
                                'class' => 'form-control',
                                'id' => 'standing-long-jump-one',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('standing long jump (1)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-running"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('standing_long_jump_two', ucwords('standing long jump (2)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('standing_long_jump_two',[
                                'class' => 'form-control',
                                'id' => 'standing-long-jump-two',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('standing long jump (2)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-running"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('standing_long_jump_total', ucwords('standing long jump (Total)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('standing_long_jump_total',[
                                'class' => 'form-control',
                                'id' => 'standing-long-jump-total',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('standing long jump (Total)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-running"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('hexagon_agility_clockwise', ucwords('hexagon agility (Clockwise)(Seconds)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('hexagon_agility_clockwise',[
                                'class' => 'form-control',
                                'id' => 'hexagon-agility-clockwise',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('hexagon agility (Clockwise)(Seconds)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-undo"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('hexagon_agility_counter_clockwise', ucwords('hexagon agility (Counter Clockwise)(Seconds)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('hexagon_agility_counter_clockwise',[
                                'class' => 'form-control',
                                'id' => 'hexagon-agility-counter-clockwise',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('hexagon agility (Counter Clockwise)(Seconds)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-undo"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('hexagon_agility_result', ucwords('hexagon agility (Result)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('hexagon_agility_result',[
                                'class' => 'form-control',
                                'id' => 'hexagon-agility-result',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('hexagon agility result'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-undo"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('kick_test', ucwords('Kick Test (Standard)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('kick_test',[
                                'class' => 'form-control',
                                'id' => 'kick-test',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('Kick Test (Standard)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-undo"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="w-100"></div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('sit_and_reach_one', ucwords('sit and reach (1)(Seconds)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('sit_and_reach_one',[
                                'class' => 'form-control',
                                'id' => 'sit-and-reach-one',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('sit and reach (1)(Seconds)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('sit_and_reach_two', ucwords('sit and reach (2)(Seconds)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('sit_and_reach_two',[
                                'class' => 'form-control',
                                'id' => 'sit-and-reach-two',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('sit and reach (2)(Seconds)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('sit_and_reach_result', ucwords('sit and reach (Result)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('sit_and_reach_result',[
                                'class' => 'form-control',
                                'id' => 'sit-and-reach-result',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('sit and reach (Result)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('stick_drop_test_one', ucwords('stick drop test (1)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('stick_drop_test_one',[
                                'class' => 'form-control',
                                'id' => 'stick-drop-test-one',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('stick drop test (1)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('stick_drop_test_two', ucwords('stick drop test (2)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('stick_drop_test_two',[
                                'class' => 'form-control',
                                'id' => 'stick-drop-test-two',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('stick drop test (2)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('stick_drop_test_three', ucwords('stick drop test (3)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('stick_drop_test_three',[
                                'class' => 'form-control',
                                'id' => 'stick-drop-test-three',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('stick drop test (3)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('stick_drop_test_four', ucwords('stick drop test (4)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('stick_drop_test_four',[
                                'class' => 'form-control',
                                'id' => 'stick-drop-test-four',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('stick drop test (4)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('stick_drop_test_result', ucwords('stick drop test (Result)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('stick_drop_test_result',[
                                'class' => 'form-control',
                                'id' => 'stick-drop-test-result',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('stick drop test (Result)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-blind"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="w-100"></div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('zipper_test_one', ucwords('zipper test (1)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('zipper_test_one',[
                                'class' => 'form-control',
                                'id' => 'zipper-test-one',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('zipper test (1)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-sign-language"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('zipper_test_two', ucwords('zipper test (2)(CM)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('zipper_test_two',[
                                'class' => 'form-control',
                                'id' => 'zipper-test-two',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('zipper test (2)(CM)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-sign-language"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('zipper_test_result', ucwords('zipper test (Result)'))?>
                        <div class="input-group mb-3">
                            <?=$this->Form->number('zipper_test_result',[
                                'class' => 'form-control',
                                'id' => 'zipper-test-result',
                                'title' => ucwords('please fill out this field'),
                                'placeholder' => ucwords('zipper test (Result)'),
                                'pattern' => '([0-9\.]){1,}',
                                'required' => true,
                                'min' => 0
                            ])?>
                            <small></small>
                            <div class="input-group-append">
                                <span class="input-group-text bg-info">
                                    <i class="fas fa-sign-language"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="w-100"></div>

                    <div class="col-sm-12 col-md-4 col-lg-4 mt-3">
                        <?=$this->Form->label('status_id', ucwords('status'))?>
                        <?=$this->Form->select('status_id', $statuses,[
                            'class' => 'form-control',
                            'id' => 'status-id',
                            'title' => ucwords('please fill out this field'),
                            'empty' => ucwords('Select status'),
                            'pattern' => '(.){1,}',
                            'required' => true,
                        ])?>
                        <small></small>
                    </div>


                    <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
                       <?=$this->Form->label('remarks', ucwords('remarks'))?>
                       <?=$this->Form->textarea('remarks',[
                           'class' => 'form-control',
                           'id' => 'remarks',
                           'required' => true,
                           'placeholder' => ucwords('remarks'),
                           'value' => '-',
                           'title' => ucwords('please fill out this field')
                       ]);?>
                        <small></small>
                    </div>

                </div>
            </div>
            <div class="card-footer d-flex justify-content-end align-items-center">
                <?=$this->Form->hidden('establishment_id',[
                    'id' => 'establishment-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval($establishment->id)
                ])?>
                <?=$this->Form->hidden('user_id',[
                    'id' => 'user-id',
                    'required' => true,
                    'readonly' => true,
                    'value' => intval(@$auth['id'])
                ])?>
                <a link href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Students', 'action' => 'index'])?>" title="Return" class="btn btn-primary rounded-0 mx-2">
                    Return
                </a>
                <?= $this->Form->button(__('Submit'),[
                    'class' => 'btn btn-success rounded-0',
                    'title' => ucwords('Submit')
                ])?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/students/add')?>