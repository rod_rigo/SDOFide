<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Indicator Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $condition_id
 * @property string $indicator
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Condition $condition
 * @property \App\Model\Entity\Classroom[] $classrooms
 */
class Indicator extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'condition_id' => true,
        'indicator' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'condition' => true,
        'classrooms' => true,
    ];

    protected function _setIndicator($value){
        return ucwords($value);
    }

}
