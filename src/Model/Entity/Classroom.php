<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Classroom Entity
 *
 * @property int $id
 * @property int $evaluation_id
 * @property int $condition_id
 * @property int $indicator_id
 * @property int|null $is_yes
 * @property string|null $remarks
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Evaluation $evaluation
 * @property \App\Model\Entity\Condition $condition
 * @property \App\Model\Entity\Indicator $indicator
 */
class Classroom extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'evaluation_id' => true,
        'condition_id' => true,
        'indicator_id' => true,
        'is_yes' => true,
        'remarks' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'evaluation' => true,
        'condition' => true,
        'indicator' => true,
    ];
}
