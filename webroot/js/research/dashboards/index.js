'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'dashboards/';

    const autocolors = window['chartjs-plugin-autocolors'];
    Chart.register(autocolors);

    var request_canvas = document.querySelector('#requests-chart').getContext('2d');
    var request_chart;

    function getRequests() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getRequests',
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.month).toUpperCase());
            });
            $('#year').prop('disabled', false);
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getRequestsChart() {
        request_chart =  new Chart(request_canvas, {
            type: 'bar',
            data: {
                labels: getRequests().label,
                datasets: [
                    {
                        label:'Total Requests',
                        data: getRequests().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Request',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var project_canvas = document.querySelector('#project-chart').getContext('2d');
    var project_chart;

    function getProjects() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getProjectsCategory',
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.category).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getProjectsChart() {
        project_chart =  new Chart(project_canvas, {
            type: 'polarArea',
            data: {
                labels: getProjects().label,
                datasets: [
                    {
                        label:'Project Category',
                        data: getProjects().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Project',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    var status_canvas = document.querySelector('#status-chart').getContext('2d');
    var status_chart;

    function getStatuses() {
        var array = {
            'data':[],
            'label':[]
        };
        $.ajax({
            url:baseurl+'getProjectsStatus',
            method: 'GET',
            type:'GET',
            dataType:'JSON',
            async: false,
            global: false,
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {
            $.map(data,function (data) {
                array.data.push(parseFloat(data.total).toFixed(2));
                array.label.push((data.status).toUpperCase());
            });
        }).fail(function (data, status, xhr) {
            Swal.close();
        });
        return array;
    }

    function getStatusesChart() {
        status_chart =  new Chart(status_canvas, {
            type: 'doughnut',
            data: {
                labels: getStatuses().label,
                datasets: [
                    {
                        label:'Project Status',
                        data: getStatuses().data
                    }
                ]
            },
            options: {
                indexAxis:'y',
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    y: {
                        beginAtZero: true,
                    },
                    x: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    autocolors: {
                        mode: 'data',
                        offset: 5
                    },
                    title: {
                        display: true,
                        text: 'Project Status',
                        font: {
                            size: 14,
                            family: 'Arial'
                        }
                    },
                    subtitle: {
                        display: false,
                        text: ''
                    },
                    legend: {
                        display:true,
                        onClick: function (evt, legendItem, legend) {
                            const index = legend.chart.data.labels.indexOf(legendItem.text);
                            legend.chart.toggleDataVisibility(index);
                            legend.chart.update();
                        },
                        labels:{
                            generateLabels: function (chart) {
                                return  $.map(chart.data.labels, function( label, index ) {
                                    return {
                                        text:label,
                                        strokeStyle: chart.data.datasets[0].borderColor[index],
                                        fillStyle: chart.data.datasets[0].backgroundColor[index],
                                        hidden: !chart.getDataVisibility(index)
                                    };
                                });
                            },
                            font: {
                                size: 14
                            }
                        }
                    },
                }
            },
            plugins:[
                {
                    id: null,
                    beforeDraw: function (chart) {
                        const ctx = chart.canvas.getContext('2d');
                        ctx.save();
                        ctx.globalCompositeOperation = 'destination-over';
                        ctx.fillStyle = 'white';
                        ctx.fillRect(0, 0, chart.width, chart.height);
                        ctx.restore();
                    },
                }
            ]
        });
    }

    function requests() {
        $.ajax({
            url:baseurl+'requests',
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                $('#total-transactions-today, #total-transactions-week, #total-transactions-month, #total-transactions-year').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-requests-today').text(parseFloat(data.today));
            $('#total-requests-week').text(parseFloat(data.week));
            $('#total-requests-month').text(parseFloat(data.month));
            $('#total-requests-year').text(parseFloat(data.year));
        }).fail(function (data, status, xhr) {

        });
    }

    function counts() {
        $.ajax({
            url:baseurl+'counts',
            type: 'GET',
            method: 'GET',
            dataType:'JSON',
            beforeSend: function (e) {
                $('#total-clients, #total-projects').text(0);
            },
        }).done(function (data, status, xhr) {
            $('#total-clients').text(parseFloat(data.users));
            $('#total-projects').text(parseFloat(data.projects));
        }).fail(function (data, status, xhr) {

        });
    }

    requests();
    counts();
    getRequestsChart();
    getProjectsChart();
    getStatusesChart();

});