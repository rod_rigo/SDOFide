<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="p-5 mt-3">

</div>

<div class="modal fade" id="modal" data-bs-backrop="static" tabindex="-1">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content" style="background: rgba(9, 30, 62, .7);">
            <div class="modal-header border-0">
                <button type="button" class="btn bg-white btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body d-flex align-items-center justify-content-center">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <embed src="#" id="embed" width="800" height="0" title="CSM">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Team Start -->
<?php if(!$teams->isEmpty()):?>
    <div class="container-fluid py-3 wow fadeInUp" data-wow-delay="0.1s" id="team">
        <div class="container py-3">
            <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text-primary text-uppercase">Team Members</h5>
                <h1 class="mb-0">Professional Stuffs Ready to Help You</h1>
            </div>
            <div class="row d-flex justify-content-around align-items-center g-5">
                <?php foreach ($teams as $team):?>
                    <div class="col-sm-12 col-md-5 col-lg-4 wow slideInUp m-2" data-wow-delay="0.3s">
                        <div class="team-item bg-light rounded overflow-hidden">
                            <div class="team-img position-relative overflow-hidden">
                                <img class="img-fluid w-100" src="<?=$this->Url->assetUrl('/img/'.($team->team_image))?>" style="object-fit: contain !important;" loading="lazy">
                            </div>
                            <div class="text-center py-4">
                                <h4 class="text-primary"><?=strtoupper($team->name)?></h4>
                                <p class="text-uppercase m-0"><?=strtoupper($team->position)?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
<?php endif;?>
    <!-- Team End -->

<!-- About Start -->
<?php if(!$abouts->isEmpty()):?>
    <div class="container-fluid py-3 wow fadeInUp" data-wow-delay="0.1s" id="about">
        <div class="container py-3">
            <?php foreach ($abouts as $about):?>
                <div class="row g-5">
                    <div class="col-sm-12 col-md-8 col-lg-7">
                        <div class="section-title position-relative pb-3 mb-5">
<!--                            <h5 class="fw-bold text-primary text-uppercase">strtoupper($about->header)</h5>-->
                            <h1 class="mb-0"><?=strtoupper($about->title)?></h1>
                        </div>
                        <p class="mb-4" style="text-align: justify !important; text-indent: 100px;">
                            <?=strtoupper($about->description)?>
                        </p>
                        <div class="row g-0 mb-3">
                            <?=($about->content)?>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-5" style="min-height: 500px;">
                        <div class="position-relative h-100">
                            <img class="position-absolute w-100 h-100 rounded wow zoomIn" data-wow-delay="0.9s" src="<?=$this->Url->assetUrl('/img/'.($about->thumbnail))?>" loading="lazy" style="object-fit: contain;">
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
<?php endif;?>
<!-- About End -->

<?php if(!empty($contact)):?>
<!-- Contact Start -->
    <div class="container-fluid py-3 wow fadeInUp" data-wow-delay="0.1s" id="contact">
        <div class="container py-3">
            <div class="section-title text-center position-relative pb-3 mb-5 mx-auto" style="max-width: 600px;">
                <h5 class="fw-bold text-primary text-uppercase"><?=strtoupper($contact->header)?></h5>
                <h1 class="mb-0"><?=strtoupper($contact->title)?></h1>
            </div>
            <div class="row g-5 mb-5">
                <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                    <div class="d-flex align-items-center wow fadeIn" data-wow-delay="0.1s">
                        <div class="bg-primary d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-phone-alt text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2 text-capitalize">Call to ask any question</h5>
                            <h4 class="text-primary mb-0"><?=($contact->contact_mobile)?></h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 mt-4">
                    <div class="d-flex align-items-center wow fadeIn" data-wow-delay="0.4s">
                        <div class="bg-primary d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-envelope-open text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2 text-capitalize">Email Us To Ask Any Question</h5>
                            <h4 class="text-primary mb-0"><?=($contact->email)?></h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 mt-3">
                    <div class="d-flex align-items-center wow fadeIn" data-wow-delay="0.8s">
                        <div class="bg-primary d-flex align-items-center justify-content-center rounded" style="width: 60px; height: 60px;">
                            <i class="fa fa-map-marker-alt text-white"></i>
                        </div>
                        <div class="ps-4">
                            <h5 class="mb-2 text-capitalize">Visit our office</h5>
                            <h4 class="text-primary mb-0"><?=ucwords($contact->address)?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row g-5 mt-3">
                <div class="col-sm-12 col-md-6 col-lg-6 mt-2 wow slideInUp" data-wow-delay="0.3s">
                    <p class="mb-4" style="text-align: justify !important; text-indent: 100px !important;">
                        <?=strtoupper($contact->description)?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 mt-2 wow slideInUp" data-wow-delay="0.6s">
                    <div class="maps w-100" id="map-<?=intval($contact->id)?>" style="height: 60vh !important;"></div>
                    <script>
                        var streets = L.tileLayer('http://www.google.cn/maps/vt?lyrs=m&x={x}&y={y}&z={z}',{
                            attribution: 'Google Streets',
                            detectRetina: true
                        });
                        var map = new L.Map('map-<?=intval($contact->id)?>',{
                            layers: [streets]
                        }).locate({
                            setView: false,
                            zoom: 13,
                            watch:true
                        }).setView([parseFloat(<?=doubleval($contact->latitude)?>), parseFloat(<?=doubleval($contact->longtitude)?>)], 15);
                        marker = L.marker([parseFloat(<?=doubleval($contact->latitude)?>), parseFloat(<?=doubleval($contact->longtitude)?>)], {
                            icon:  L.icon({
                                iconUrl: '/recordsunitoffice/leaflet/images/marker-icon.png',
                                iconSize: [20, 38],
                                iconAnchor: [10, 40],
                            }),
                            draggable:false
                        }).addTo(map);
                    </script>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12 mt-2 wow slideInUp" data-wow-delay="0.9s">
                    <?=$contact->content?>
                </div>
            </div>
        </div>
    </div>
<!-- Contact End -->
<?php endif;?>

<?=$this->Html->script('websites/index')?>
