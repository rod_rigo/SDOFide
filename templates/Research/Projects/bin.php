<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Project> $projects
 */
?>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 mt-3">
            <div class="card p-3">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Position</th>
                            <th>Department</th>
                            <th>Batch</th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Remarks</th>
<!--                            <th>Published</th>-->
                            <th>Modified By</th>
                            <th>Deleted</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?=$this->Html->script('research/projects/bin')?>