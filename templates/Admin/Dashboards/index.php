<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="modal fade" id="progression-modal" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <?=$this->Form->create(null,['id' => 'progression-form', 'type' => 'file'])?>
            <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('start_date', ucwords('Start Date'))?>
                        <?=$this->Form->date('start_date',[
                            'class' => 'form-control rounded-0',
                            'id' => 'progression-start-date',
                            'required' => true,
                            'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d')
                        ])?>
                    </div>
                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('end_date', ucwords('End Date'))?>
                        <?=$this->Form->date('end_date',[
                            'class' => 'form-control rounded-0',
                            'id' => 'progression-end-date',
                            'required' => true,
                            'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('month')->format('Y-m-d')
                        ])?>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-4 d-flex justify-content-start align-items-end">
                        <?=$this->Form->button('Search',[
                            'class' => 'btn btn-primary rounded-0',
                            'type' => 'submit'
                        ])?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <canvas id="progression-chart" height="600" width="800"></canvas>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<div class="modal fade" id="status-modal" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <?=$this->Form->create(null,['id' => 'status-form', 'type' => 'file'])?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('start_date', ucwords('Start Date'))?>
                        <?=$this->Form->date('start_date',[
                            'class' => 'form-control rounded-0',
                            'id' => 'status-start-date',
                            'required' => true,
                            'value' => (new \Moment\Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d')
                        ])?>
                    </div>
                    <div class="col-sm-12 col-md-5 col-lg-4">
                        <?=$this->Form->label('end_date', ucwords('End Date'))?>
                        <?=$this->Form->date('end_date',[
                            'class' => 'form-control rounded-0',
                            'id' => 'status-end-date',
                            'required' => true,
                            'value' => (new \Moment\Moment(null,'Asia/Manila'))->endOf('month')->format('Y-m-d')
                        ])?>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-4 d-flex justify-content-start align-items-end">
                        <?=$this->Form->button('Search',[
                            'class' => 'btn btn-primary rounded-0',
                            'type' => 'submit'
                        ])?>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <canvas id="status-chart" height="600" width="800"></canvas>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center align-items-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <?=$this->Form->end()?>
    </div>
</div>

<h5 class="mb-2">Documents</h5>
<div class="row">

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Today</span>
                <span class="info-box-number" id="total-documents-today">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Week</span>
                <span class="info-box-number" id="total-documents-week">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
            <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Month</span>
                <span class="info-box-number" id="total-documents-month">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
              <i class="fa fa-file-alt"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Year</span>
                <span class="info-box-number" id="total-documents-year">0</span>
            </div>
        </div>
    </div>

</div>

<h5 class="mb-2">Transactions</h5>
<div class="row">

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-calendar"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Today</span>
                <span class="info-box-number" id="total-transactions-today">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-calendar"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Week</span>
                <span class="info-box-number" id="total-transactions-week">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-calendar"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Month</span>
                <span class="info-box-number" id="total-transactions-month">0</span>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="info-box">
            <span class="info-box-icon bg-info">
                <i class="far fa-calendar"></i>
            </span>
            <div class="info-box-content">
                <span class="info-box-text">Year</span>
                <span class="info-box-number" id="total-transactions-year">0</span>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12 col-md-5 col-lg-4 mb-3">
        <?=$this->Form->year('year',[
            'class' => 'form-control rounded-0',
            'id' => 'year',
            'min' => '2000',
            'value' => date('Y'),
            'disabled' => true
        ])?>
    </div>

    <div class="col-sm-12 col-md-7 col-lg-8 mb-3 d-flex justify-content-end align-items-center">
        <?=$this->Form->button('Progression',[
            'id' => 'toggle-progression',
            'class' => 'btn btn-primary rounded-0 mx-2',
        ])?>
        <?=$this->Form->button('Status',[
            'id' => 'toggle-status',
            'class' => 'btn btn-primary rounded-0',
        ])?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-8">
        <div class="card p-3">
            <canvas id="transaction-chart-month" width="20" height="20" style="height: 35em;"></canvas>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-4" style="height: 35em;">
        <div class="card p-3">
            <canvas id="transaction-chart-week" width="20" height="20" style="height: 35em;"></canvas>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-9">
        <div class="card p-3">
            <canvas id="department-chart" width="20" height="20" style="height: 35em;"></canvas>
        </div>
    </div>
    <div class="col-sm-12 col-md-4 col-lg-3">
        <div class="card p-3">
            <canvas id="establishment-chart" width="20" height="20" style="height: 35em;"></canvas>
        </div>
    </div>
</div>

<?=$this->Html->script('admin/dashboards/index')?>
