<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Equipment Entity
 *
 * @property int $id
 * @property int $evaluation_id
 * @property string|null $dcp_batch
 * @property string|null $year_delivered
 * @property string|null $package_type
 * @property string|null $location
 * @property \Cake\I18n\FrozenDate|null $warranty_expiration_date
 * @property int|null $is_dr_available
 * @property string|null $dr_document
 * @property int|null $is_iar_available
 * @property string|null $iar_document
 * @property int|null $is_irp_available
 * @property string|null $irp_document
 * @property int|null $is_ptr_available
 * @property string|null $ptr_document
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Evaluation $evaluation
 */
class Equipment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'evaluation_id' => true,
        'dcp_batch' => true,
        'year_delivered' => true,
        'package_type' => true,
        'location' => true,
        'warranty_expiration_date' => true,
        'is_dr_available' => true,
        'dr_document' => true,
        'is_iar_available' => true,
        'iar_document' => true,
        'is_irp_available' => true,
        'irp_document' => true,
        'is_ptr_available' => true,
        'ptr_document' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'evaluation' => true,
    ];
}
