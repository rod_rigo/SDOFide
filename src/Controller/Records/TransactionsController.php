<?php
declare(strict_types=1);

namespace App\Controller\Records;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\EventInterface;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Moment\Moment;

/**
 * Transactions Controller
 *
 * @property \App\Model\Table\TransactionsTable $Transactions
 * @method \App\Model\Entity\Transaction[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TransactionsController extends AppController
{

    public function beforeFilter(EventInterface $event)
    {
        $this->viewBuilder()->setLayout('records');
        parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }

    public function isAuthorized($user)
    {
        if(empty($user) && !boolval($user['is_records'])){
            throw new ForbiddenException(__('Forbidden Action!'));
        }

        return parent::isAuthorized($user); // TODO: Change the autogenerated stub
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $departments = TableRegistry::getTableLocator()->get('Departments')
            ->find('list',[
                'keyField' => function($query){
                    return ucwords($query->department);
                },
                'valueField' => function($query){
                    return ucwords($query->department);
                }
            ])
            ->order(['department' => 'ASC'],true);
        $this->set(compact('departments'));
    }

    public function getTransactions(){
        $data = $this->Transactions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ]);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function today()
    {
        $departments = TableRegistry::getTableLocator()->get('Departments')
            ->find('list',[
                'keyField' => function($query){
                    return ucwords($query->department);
                },
                'valueField' => function($query){
                    return ucwords($query->department);
                }
            ])
            ->order(['department' => 'ASC'],true);
        $this->set(compact('departments'));
    }

    public function getTransactionsToday(){
        $startDate = (new Moment(null,'Asia/Manila'))->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->addDays(1)->format('Y-m-d');
        $data = $this->Transactions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Transactions.created >=' => $startDate,
                'Transactions.created <=' => $endDate
            ])
            ->order(['Transactions.modified' => 'DESC'],true)
            ->limit(1000);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function week()
    {
        $departments = TableRegistry::getTableLocator()->get('Departments')
            ->find('list',[
                'keyField' => function($query){
                    return ucwords($query->department);
                },
                'valueField' => function($query){
                    return ucwords($query->department);
                }
            ])
            ->order(['department' => 'ASC'],true);
        $this->set(compact('departments'));
    }

    public function getTransactionsWeek(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('week')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('week')->addDays(1)->format('Y-m-d');
        $data = $this->Transactions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Transactions.created >=' => $startDate,
                'Transactions.created <=' => $endDate
            ])
            ->order(['Transactions.modified' => 'DESC'],true)
            ->limit(1000);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function month()
    {
        $departments = TableRegistry::getTableLocator()->get('Departments')
            ->find('list',[
                'keyField' => function($query){
                    return ucwords($query->department);
                },
                'valueField' => function($query){
                    return ucwords($query->department);
                }
            ])
            ->order(['department' => 'ASC'],true);
        $this->set(compact('departments'));
    }

    public function getTransactionsMonth(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('month')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('month')->addDays(1)->format('Y-m-d');
        $data = $this->Transactions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Transactions.created >=' => $startDate,
                'Transactions.created <=' => $endDate
            ])
            ->order(['Transactions.modified' => 'DESC'],true)
            ->limit(1000);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function year()
    {
        $departments = TableRegistry::getTableLocator()->get('Departments')
            ->find('list',[
                'keyField' => function($query){
                    return ucwords($query->department);
                },
                'valueField' => function($query){
                    return ucwords($query->department);
                }
            ])
            ->order(['department' => 'ASC'],true);
        $this->set(compact('departments'));
    }

    public function getTransactionsYear(){
        $startDate = (new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = (new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = $this->Transactions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Transactions.created >=' => $startDate,
                'Transactions.created <=' => $endDate
            ])
            ->order(['Transactions.modified' => 'DESC'],true)
            ->limit(1000);
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function released(){

    }

    public function getReleased(){
        $records = $this->request->getQuery('records')? $this->request->getQuery('records'): 10000;
        $startDate = $this->request->getQuery('start_date')? (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d')
            :(new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = $this->request->getQuery('end_date')? (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->format('Y-m-d')
            :(new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = $this->Transactions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Transactions.date_release >=' => $startDate,
                'Transactions.date_release <=' => $endDate
            ])
            ->whereNotNull('Transactions.date_release')
            ->order(['Transactions.modified' => 'DESC'],true)
            ->limit(intval($records));
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function received(){

    }

    public function getReceived(){
        $records = $this->request->getQuery('records')? $this->request->getQuery('records'): 10000;
        $startDate = $this->request->getQuery('start_date')? (new Moment($this->request->getQuery('start_date'),'Asia/Manila'))->format('Y-m-d')
            :(new Moment(null,'Asia/Manila'))->startOf('year')->format('Y-m-d');
        $endDate = $this->request->getQuery('end_date')? (new Moment($this->request->getQuery('end_date'),'Asia/Manila'))->format('Y-m-d')
            :(new Moment(null,'Asia/Manila'))->endOf('year')->addDays(1)->format('Y-m-d');
        $data = $this->Transactions->find()
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->where([
                'Transactions.date_received >=' => $startDate,
                'Transactions.date_received <=' => $endDate
            ])
            ->whereNotNull('Transactions.date_received')
            ->order(['Transactions.modified' => 'DESC'],true)
            ->limit(intval($records));
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    public function bin()
    {

    }

    public function getTransactionsDeleted(){
        $data = $this->Transactions->find('all',['withDeleted'])
            ->contain([
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted'])
                            ->order(['Trackings.created' => 'DESC'], true);
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
            ])
            ->whereNotNull('Transactions.deleted');
        return $this->response->withType('application/json')
            ->withStringBody(json_encode(['data' => $data]));
    }

    /**
     * View method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => [
                'Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Establishments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Departments' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings' => [
                    'queryBuilder' => function($query){
                        return $query->find('all');
                    }
                ],
                'Trackings.Statuses' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Progressions' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ],
                'Trackings.Users' => [
                    'queryBuilder' => function($query){
                        return $query->find('all',['withDeleted']);
                    }
                ]
            ],
        ]);
        $entity = TableRegistry::getTableLocator()->get('Trackings')->newEmptyEntity();

        $establishments = $this->Transactions->Establishments->find('list', [
            'valueField' => function($query){
                return ucwords($query->establishment);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'is_active =' => intval(1)
        ])->order(['establishment' => 'ASC'],true);
        $departments = $this->Transactions->Departments->find('list', [
            'valueField' => function($query){
                return ucwords($query->department);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'is_active =' => intval(1),
            'establishment_id =' => intval($transaction->establishment_id)
        ])->order(['department' => 'ASC'],true);
        $progressions = TableRegistry::getTableLocator()->get('Progressions')->find('list',[
            'valueField' => function($query){
                return ucwords($query->progression);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'is_active =' => intval(1),
        ])->order(['progression' => 'ASC'],true);
        $statuses = TableRegistry::getTableLocator()->get('Statuses')->find('list',[
            'valueField' => function($query){
                return ucwords($query->status);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'is_active =' => intval(1),
        ])->order(['status' => 'ASC'],true);

        $this->set(compact('transaction','progressions', 'statuses', 'establishments', 'departments', 'entity'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transaction = $this->Transactions->newEmptyEntity();
        if ($this->request->is('post')) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            $transaction->user_id = $this->Auth->user('id');

            $validator = new Validator();

            $validator
                ->requirePresence('file', false)
                ->allowEmptyFile('file', ucwords('Please upload a file'),true)
                ->add('file','file', [
                    'rule' => function($value){
                        $mimes = [
                            strtolower('application/pdf'),
                            strtolower('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                        ];

                        if(!in_array(strtolower($value->getClientMediaType()),$mimes)){
                            return ucwords('Only PDF & DOCX Format Is Allowed');
                        }

                        return true;
                    }
                ]);

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            $other = ($this->request->getData('other'))? strlen($this->request->getData('other')): intval(0);
            if(intval($other) > intval(0)){
                $department = TableRegistry::getTableLocator()->get('Departments')->findOrCreate([
                    'department like' => '%'.ucwords($this->request->getData('other')).'%',
                    'establishment_id =' => intval($this->request->getData('establishment_id'))
                ], function ($entity) {
                        $entity->user_id = $this->Auth->user('id');
                        $entity->establishment_id = $this->request->getData('establishment_id');
                        $entity->department = ucwords($this->request->getData('other'));
                        $entity->is_active = intval(1);
                    }
                );

                $transaction->department_id = intval($department->id);
            }

            $transaction->has_file = intval(0);
            if($transaction->hasErrors() == false && strlen($this->request->getData('file')->getClientFileName()) > 0){

                $file = $this->request->getData('file');

                $filename = uniqid().$this->request->getData('file')->getClientFileName();

                try{
                    $path = WWW_ROOT. 'files';
                    $folder = new Folder();
                    if(!$folder->cd($path)){
                        $folder->create($path);
                    }

                    $path = WWW_ROOT. 'files'. DS. strval($this->request->getData('department_id'));
                    $folder = new Folder();
                    if(!$folder->cd($path)){
                        $folder->create($path);
                    }

                    $filepath = WWW_ROOT. 'files'. DS. strval($this->request->getData('department_id')). DS. $filename;

                    if($file){
                        $file->moveTo($filepath);
                    }

                    $transaction->has_file = intval(1);
                    $transaction->document = strval($this->request->getData('department_id')).'/'.$filename;
                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }

            }

            if ($this->Transactions->save($transaction,['associated' => ['Trackings']])) {
                $result = ['message' => ucwords('The transaction has been saved'), 'result' => ucwords('success'),
                    'redirect' => Router::url(['prefix' => 'Admin', 'controller' => 'Transactions', 'action' => 'index'])];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($transaction->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $transaction->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
        $establishments = $this->Transactions->Establishments->find('list', [
            'valueField' => function($query){
                return ucwords($query->establishment);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'is_active =' => intval(1)
        ])->order(['establishment' => 'ASC'],true);
        $departments = $this->Transactions->Departments->find('list', [
            'valueField' => function($query){
                return ucwords($query->department);
            },
            'keyField' => function($query){
                return intval($query->id);
            }
        ])->where([
            'is_active =' => intval(1)
        ])->order(['department' => 'ASC'],true);
        $progression = TableRegistry::getTableLocator()->get('Progressions')->find()->where([
            'is_active =' => intval(1),
            'is_default =' => intval(1)
        ])->first();
        $status = TableRegistry::getTableLocator()->get('Statuses')->find()->where([
            'is_active =' => intval(1),
            'is_default =' => intval(1)
        ])->first();
        $this->set(compact('transaction',  'establishments', 'departments', 'progression', 'status'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transaction = $this->Transactions->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transaction = $this->Transactions->patchEntity($transaction, $this->request->getData());
            $transaction->user_id = $this->Auth->user('id');

            $validator = new Validator();

            $validator
                ->requirePresence('file', false)
                ->allowEmptyFile('file', ucwords('Please upload a file'),true)
                ->add('file','file', [
                    'rule' => function($value){
                        $mimes = [
                            strtolower('application/pdf'),
                            strtolower('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                        ];

                        if(!in_array(strtolower($value->getClientMediaType()),$mimes)){
                            return ucwords('Only PDF & DOCX Format Is Allowed');
                        }

                        return true;
                    }
                ]);

            $errors = $validator->validate($this->request->getData());

            foreach ($errors as $key => $value){
                $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                    'errors' => $errors];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

            $transaction->has_file = intval(0);
            if($transaction->hasErrors() == false && strlen($this->request->getData('file')->getClientFileName()) > 0){

                $file = $this->request->getData('file');

                $filename = uniqid().$this->request->getData('file')->getClientFileName();

                try{

                    $path = WWW_ROOT. 'files'. DS. strval($this->request->getData('department_id'));
                    $folder = new Folder();
                    if(!$folder->cd($path)){
                        $folder->create($path);
                    }

                    $path = WWW_ROOT. 'files'. DS. strval($transaction->document);
                    $folder = new File($path);
                    if($folder->exists()){
                        $folder->delete();
                    }

                    $filepath = WWW_ROOT. 'files'. DS. strval($this->request->getData('department_id')). DS. $filename;

                    if($file){
                        $file->moveTo($filepath);
                    }

                    $transaction->has_file = intval(1);
                    $transaction->document = strval($this->request->getData('department_id')).'/'.$filename;
                }catch (\Exception $exception){
                    $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }

            }

            if ($this->Transactions->save($transaction)) {
                $result = ['message' => ucwords('The transaction has been saved'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }else{
                foreach ($transaction->getErrors() as $key => $value){
                    $result = ['message' => ucwords(reset($value)), 'result' => ucwords('error'),
                        'errors' => $transaction->getErrors()];
                    return $this->response->withStatus(422)->withType('application/json')
                        ->withStringBody(json_encode($result));
                }
            }
        }
    }

    public function download($id = null)
    {
        $transaction = $this->Transactions->get($id);

        $path = WWW_ROOT. 'files'.DS .strval($transaction->document);

        $mime = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);

        $folder = new File($path);
        if($folder->exists()){
            return $this->response->withFile($path, [
                'download' => true,
                'name' => $transaction->docsid.'.'.(explode('/',$mime)[1])
            ]);
        }else{
            throw new NotFoundException(ucwords('file not found'));
        }

    }

    /**
     * Delete method
     *
     * @param string|null $id Transaction id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id);

        if ($this->Transactions->delete($transaction)) {
            $result = ['message' => ucwords('The transaction has been deleted'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The transaction has not been deleted'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function restore($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transaction = $this->Transactions->get($id,[
            'withDeleted'
        ]);

        $trackings = TableRegistry::getTableLocator()->get('Trackings')->updateAll([
            'deleted' => null
        ],[
            'transaction_id =' => intval($transaction->id)
        ]);

        if ($this->Transactions->restore($transaction) && $trackings) {
            $result = ['message' => ucwords('The transaction has been restored'), 'result' => ucwords('success')];
            return $this->response->withStatus(200)->withType('application/json')
                ->withStringBody(json_encode($result));
        } else {
            $result = ['message' => ucwords('The transaction has not been restored'), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }
    }

    public function hardDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $connection = ConnectionManager::get('default');

        $transaction = $this->Transactions->get($id,[
            'withDeleted'
        ]);

        $path = WWW_ROOT. 'files'. DS. strval($transaction->document);
        $folder = new File($path);

        $connection->begin();

        try{

            $connection->execute('SET FOREIGN_KEY_CHECKS=0;');

            $trackings = TableRegistry::getTableLocator()->get('Trackings')
                ->query()
                ->where([
                    'transaction_id =' => intval($transaction->id)
                ])
                ->delete();

            if($folder->exists()){
                $folder->delete();
            }

            if ($this->Transactions->hardDelete($transaction) && $trackings->execute()) {
                $connection->commit();
                $result = ['message' => ucwords('The transaction has been deleted'), 'result' => ucwords('success')];
                return $this->response->withStatus(200)->withType('application/json')
                    ->withStringBody(json_encode($result));
            } else {
                $result = ['message' => ucwords('The transaction has not been deleted'), 'result' => ucwords('error')];
                return $this->response->withStatus(422)->withType('application/json')
                    ->withStringBody(json_encode($result));
            }

        }catch (\Exception $exception){
            $connection->rollback();
            $result = ['message' => ucwords($exception->getMessage()), 'result' => ucwords('error')];
            return $this->response->withStatus(422)->withType('application/json')
                ->withStringBody(json_encode($result));
        }

    }

}
