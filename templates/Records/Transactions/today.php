<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Transaction[]|\Cake\Collection\CollectionInterface $transactions
 */
?>

<?=$this->Html->css('records/transactions/today')?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 d-flex justify-content-end align-items-center mb-3">
        <a href="<?=$this->Url->build(['prefix' => 'Records', 'controller' => 'Transactions', 'action' => 'add'])?>" id="toggle-modal" class="btn btn-primary rounded-0" title="New Transaction">
            New Transaction
        </a>
    </div>

    <div class="col-sm-12 col-md-5 col-lg-5 mb-3">
        <?=$this->Form->label('departments', ucwords('departments'))?>
        <?=$this->Form->select('departments', $departments,[
            'class' => 'form-control rounded-0',
            'id' => 'departments',
            'empty' => ucwords('Departments')
        ])?>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="card p-3">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Docs ID</th>
                        <th>Transaction Name</th>
                        <th>Establishment</th>
                        <th>Department</th>
                        <th>Date Release</th>
                        <th>Received By</th>
                        <th>Date Received</th>
                        <th>Progression</th>
                        <th>Status</th>
                        <th>Remarks</th>
                        <th>Modified By</th>
                        <th>Modified</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<?=$this->Html->script('records/transactions/today')?>
